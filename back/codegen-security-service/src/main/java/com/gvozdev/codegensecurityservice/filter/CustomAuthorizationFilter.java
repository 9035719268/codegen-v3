package com.gvozdev.codegensecurityservice.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gvozdev.codegensecurityservice.util.auth.TokenUtil;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.context.support.WebApplicationContextUtils.getWebApplicationContext;

public class CustomAuthorizationFilter extends OncePerRequestFilter {
    private final TokenUtil tokenUtil;
    private Algorithm algorithm;

    public CustomAuthorizationFilter(TokenUtil tokenUtil, Algorithm algorithm) {
        this.tokenUtil = tokenUtil;
        this.algorithm = algorithm;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String servletPath = request.getServletPath();

        if (algorithm == null) {
            ServletContext servletContext = request.getServletContext();
            WebApplicationContext webApplicationContext = getWebApplicationContext(servletContext);
            algorithm = requireNonNull(webApplicationContext).getBean(Algorithm.class);
        }

        if (tokenUtil.isTokenNeeded(servletPath) && tokenUtil.isTokenValid(request)) {
            try {
                String authorizationHeader = request.getHeader(AUTHORIZATION);
                String token = authorizationHeader.substring("Bearer ".length());

                JWTVerifier jwtVerifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = jwtVerifier.verify(token);

                String userName = decodedJWT.getSubject();
                List<String> tokenBearerRoles = decodedJWT.getClaim("roles").asList(String.class);

                List<SimpleGrantedAuthority> authorities = tokenBearerRoles.stream()
                    .map(SimpleGrantedAuthority::new)
                    .collect(toList());

                Authentication authentication = new UsernamePasswordAuthenticationToken(userName, null, authorities);

                SecurityContextHolder.getContext().setAuthentication(authentication);

                filterChain.doFilter(request, response);
            } catch (Exception exception) {
                response.setStatus(FORBIDDEN.value());

                Map<String, String> errorInfo = new HashMap<>();
                errorInfo.put("error_message", exception.getMessage());

                response.setContentType(APPLICATION_JSON_VALUE);

                new ObjectMapper().writeValue(response.getOutputStream(), errorInfo);
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }
}
