package com.gvozdev.codegensecurityservice.util.config

class PortConfig {
    public static final String GATEWAY_PORT = '8888'

    public static final String TEST_SERVER_PORT = '9998'
}
