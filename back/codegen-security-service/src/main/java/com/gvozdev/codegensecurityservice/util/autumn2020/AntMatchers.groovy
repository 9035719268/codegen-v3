package com.gvozdev.codegensecurityservice.util.autumn2020

class AntMatchers {
    public static final String AUTUMN_2020_VAR1_ANT_MATCHER = '/api/tasks/year/2020/autumn/var1/**'

    public static final String AUTUMN_2020_VAR2_ANT_MATCHER = '/api/tasks/year/2020/autumn/var2/**'

    public static final String AUTUMN_2020_VAR3_ANT_MATCHER = '/api/tasks/year/2020/autumn/var3/**'

    public static final String AUTUMN_2020_RESOURCES_ANT_MATCHER = '/api/tasks/year/2020/autumn/download/resources'
}
