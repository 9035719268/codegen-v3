package com.gvozdev.codegensecurityservice.util.swagger

import static com.gvozdev.codegensecurityservice.util.config.PortConfig.GATEWAY_PORT
import static com.gvozdev.codegensecurityservice.util.config.PortConfig.TEST_SERVER_PORT

class Paths {
	public static final String AUTUMN_2020_SWAGGER_PATH = "http://localhost:$GATEWAY_PORT/api/year/2020/autumn/swagger-ui"

	public static final String AUTUMN_2020_SWAGGER_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/year/2020/autumn/swagger-ui"

	public static final String SPRING_2021_SWAGGER_PATH = "http://localhost:$GATEWAY_PORT/api/year/2021/spring/swagger-ui"

	public static final String SPRING_2021_SWAGGER_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/year/2021/spring/swagger-ui"
}
