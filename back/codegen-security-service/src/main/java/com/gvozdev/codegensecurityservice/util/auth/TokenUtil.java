package com.gvozdev.codegensecurityservice.util.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

import static com.gvozdev.codegensecurityservice.util.auth.AntMatchers.LOGIN_ANT_MATCHER;
import static com.gvozdev.codegensecurityservice.util.auth.AntMatchers.REFRESH_TOKEN_ANT_MATCHER;
import static java.lang.System.currentTimeMillis;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
public class TokenUtil {

    @Value("${access-token.expiration-time-in-ms}")
    private int accessTokenExpirationTime;

    @Value("${refresh-token.expiration-time-in-ms}")
    private int refreshTokenExpirationTime;

    public String getAccessToken(HttpServletRequest request, UserDetails userDetails, Algorithm algorithm) {
        Date expirationTime = new Date(currentTimeMillis() + accessTokenExpirationTime);

        return JWT.create()
            .withSubject(userDetails.getUsername())
            .withExpiresAt(expirationTime)
            .withIssuer(request.getRequestURL().toString())
            .withClaim("roles", userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(toList()))
            .sign(algorithm);
    }

    public String getRefreshToken(HttpServletRequest request, UserDetails userDetails, Algorithm algorithm) {
        Date expirationTime = new Date(currentTimeMillis() + refreshTokenExpirationTime);

        return JWT.create()
            .withSubject(userDetails.getUsername())
            .withExpiresAt(expirationTime)
            .withIssuer(request.getRequestURL().toString())
            .sign(algorithm);
    }

    public boolean isTokenNeeded(String servletPath) {
        return !isLoginPath(servletPath) && !isTokenRefreshPath(servletPath);
    }

    public boolean isTokenValid(HttpServletRequest request) {
        String authorizationHeader = request.getHeader(AUTHORIZATION);

        return authorizationHeader != null && authorizationHeader.startsWith("Bearer ");
    }

    private static boolean isLoginPath(String servletPath) {
        return servletPath.equals(LOGIN_ANT_MATCHER);
    }

    private static boolean isTokenRefreshPath(String servletPath) {
        return servletPath.equals(REFRESH_TOKEN_ANT_MATCHER);
    }
}
