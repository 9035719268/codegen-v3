package com.gvozdev.codegensecurityservice.restadapter.task.spring2021;

import com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestOperations;

import static com.gvozdev.codegensecurityservice.util.spring2021.Paths.*;

@RestController("Spring2021Controller")
@RequestMapping("/api/tasks/year/2021/spring")
public class PageController {

    @Autowired
    private RestOperations restOperations;

    @GetMapping("/exercise-info")
    @LoggableSecureEndpoint
    public ResponseEntity<String> getExerciseInfo(@RequestParam("inputfilename") String inputFileName) {
        return restOperations.getForEntity(
            SPRING_2021_EXERCISE_INFO_PATH,
            String.class,
            inputFileName
        );
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("inputFileName") String inputFileName,
        @PathVariable("outputFileName") String outputFileName
    ) {
        return restOperations.getForEntity(
            SPRING_2021_DOWNLOAD_LISTING_PATH,
            byte[].class,
            lang, file, oop, inputFileName, outputFileName
        );
    }

    @GetMapping("/download/charts/{inputFileName}")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") String inputFileName) {
        return restOperations.getForEntity(
            SPRING_2021_DOWNLOAD_CHARTS_PATH,
            byte[].class,
            inputFileName
        );
    }

    @GetMapping("/download/resources")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadResources() {
        return restOperations.getForEntity(
            SPRING_2021_DOWNLOAD_RESOURCES_PATH,
            byte[].class
        );
    }
}
