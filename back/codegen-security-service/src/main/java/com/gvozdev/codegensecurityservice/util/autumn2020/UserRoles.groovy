package com.gvozdev.codegensecurityservice.util.autumn2020

class UserRoles {
    public static final String[] AUTUMN_2020_ALL_USER_ROLES = ['ADMIN', 'A20201', 'A20202', 'A20203']

    public static final String[] AUTUMN_2020_VAR1_USER_ROLES = ['ADMIN', 'A20201']

    public static final String[] AUTUMN_2020_VAR2_USER_ROLES = ['ADMIN', 'A20202']

    public static final String[] AUTUMN_2020_VAR3_USER_ROLES = ['ADMIN', 'A20203']
}
