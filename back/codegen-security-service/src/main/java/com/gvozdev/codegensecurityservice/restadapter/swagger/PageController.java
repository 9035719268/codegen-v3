package com.gvozdev.codegensecurityservice.restadapter.swagger;

import com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestOperations;

import static com.gvozdev.codegensecurityservice.util.swagger.Paths.AUTUMN_2020_SWAGGER_PATH;
import static com.gvozdev.codegensecurityservice.util.swagger.Paths.SPRING_2021_SWAGGER_PATH;

@RestController("SwaggerController")
@RequestMapping("/api/year")
public class PageController {

    @Autowired
    private RestOperations restOperations;

    @GetMapping("/2020/autumn/swagger-ui")
    @LoggableSecureEndpoint
    public ResponseEntity<String> getAutumn2020Swagger() {
        return restOperations.getForEntity(
            AUTUMN_2020_SWAGGER_PATH,
            String.class
        );
    }

    @GetMapping("/2021/spring/swagger-ui")
    @LoggableSecureEndpoint
    public ResponseEntity<String> getSpring2021Swagger() {
        return restOperations.getForEntity(
            SPRING_2021_SWAGGER_PATH,
            String.class
        );
    }
}
