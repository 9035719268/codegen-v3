package com.gvozdev.codegensecurityservice.restadapter.task.autumn2020.var3;

import com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestOperations;

import static com.gvozdev.codegensecurityservice.util.autumn2020.Paths.*;

@RestController("Autumn2020Var3Controller")
@RequestMapping("/api/tasks/year/2020/autumn/var3")
@CrossOrigin("http://localhost:3000")
public class PageController {

    @Autowired
    private RestOperations restOperations;

    @GetMapping("/exercise-info")
    @LoggableSecureEndpoint
    public ResponseEntity<String> getComponents(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        return restOperations.getForEntity(
            AUTUMN_2020_VAR3_EXERCISE_INFO_PATH,
            String.class,
            latitude, longitude, satelliteNumber
        );
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{outputFileName}")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("outputFileName") String outputFileName,
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        return restOperations.getForEntity(
            AUTUMN_2020_VAR3_DOWNLOAD_LISTING_PATH,
            byte[].class,
            lang, file, oop, outputFileName, latitude, longitude, satelliteNumber
        );
    }

    @GetMapping("/download/charts")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadCharts(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        return restOperations.getForEntity(
            AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_PATH,
            byte[].class,
            latitude, longitude, satelliteNumber
        );
    }
}