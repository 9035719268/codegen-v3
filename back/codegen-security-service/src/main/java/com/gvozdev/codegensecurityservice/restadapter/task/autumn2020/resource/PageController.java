package com.gvozdev.codegensecurityservice.restadapter.task.autumn2020.resource;

import com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestOperations;

import static com.gvozdev.codegensecurityservice.util.autumn2020.Paths.AUTUMN_2020_DOWNLOAD_RESOURCES_PATH;

@RestController("ResourcesController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/tasks/year/2020/autumn")
public class PageController {

    @Autowired
    private RestOperations restOperations;

    @GetMapping("/download/resources")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadResources() {
        return restOperations.getForEntity(
            AUTUMN_2020_DOWNLOAD_RESOURCES_PATH,
            byte[].class
        );
    }
}
