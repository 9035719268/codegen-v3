package com.gvozdev.codegensecurityservice.dao;

import com.gvozdev.codegensecurityservice.domain.Student;

import java.util.Optional;

public interface UserDao {
    Optional<Student> findUserByUserName(String userName);
}
