package com.gvozdev.codegensecurityservice;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableEurekaClient
public class CodegenSecurityServiceApplication {
    public static void main(String[] args) {
        run(CodegenSecurityServiceApplication.class, args);
    }
}
