package com.gvozdev.codegensecurityservice.util.swagger

class AntMatchers {
    public static final String AUTUMN_2020_SWAGGER_ANT_MATCHER = '/api/year/2020/autumn/swagger-ui'

    public static final String SPRING_2021_SWAGGER_ANT_MATCHER = '/api/year/2021/spring/swagger-ui'
}
