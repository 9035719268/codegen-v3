package com.gvozdev.codegensecurityservice.util.autumn2020


import org.springframework.stereotype.Component

import static com.gvozdev.codegensecurityservice.util.config.PortConfig.GATEWAY_PORT
import static com.gvozdev.codegensecurityservice.util.config.PortConfig.TEST_SERVER_PORT

class Paths {
    public static final String AUTUMN_2020_VAR1_EXERCISE_INFO_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var1/exercise-info?inputfilename={inputFileName}"

    public static final String AUTUMN_2020_VAR1_EXERCISE_INFO_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var1/exercise-info?inputfilename=POTS_6hours.dat"

    public static final String AUTUMN_2020_VAR1_DOWNLOAD_LISTING_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var1/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}"

    public static final String AUTUMN_2020_VAR1_DOWNLOAD_LISTING_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var1/download/listing/cpp/file/withoop/POTS_6hours.dat/main.cpp"

    public static final String AUTUMN_2020_VAR1_DOWNLOAD_CHARTS_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var1/download/charts/{inputFileName}"

    public static final String AUTUMN_2020_VAR1_DOWNLOAD_CHARTS_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var1/download/charts/POTS_6hours.dat"

    public static final String AUTUMN_2020_VAR2_EXERCISE_INFO_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var2/exercise-info?inputfilename={inputFileName}"

    public static final String AUTUMN_2020_VAR2_EXERCISE_INFO_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var2/exercise-info?inputfilename=POTS_6hours.dat"

    public static final String AUTUMN_2020_VAR2_DOWNLOAD_LISTING_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var2/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}"

    public static final String AUTUMN_2020_VAR2_DOWNLOAD_LISTING_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var2/download/listing/cpp/file/withoop/POTS_6hours.dat/main.cpp"

    public static final String AUTUMN_2020_VAR2_DOWNLOAD_CHARTS_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var2/download/charts/{inputFileName}"

    public static final String AUTUMN_2020_VAR2_DOWNLOAD_CHARTS_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var2/download/charts/POTS_6hours.dat"

    public static final String AUTUMN_2020_VAR3_EXERCISE_INFO_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var3/exercise-info?lat={latitude}&lon={longitude}&satnum={satelliteNumber}"

    public static final String AUTUMN_2020_VAR3_EXERCISE_INFO_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var3/exercise-info?lat=55.0&lon=162&satnum=7"

    public static final String AUTUMN_2020_VAR3_DOWNLOAD_LISTING_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var3/download/listing/{lang}/{file}/{oop}/{outputFileName}?lat={latitude}&lon={longitude}&satnum={satelliteNumber}"

    public static final String AUTUMN_2020_VAR3_DOWNLOAD_LISTING_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var3/download/listing/cpp/file/withoop/main.cpp?lat=55.0&lon=162&satnum=7"

    public static final String AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/var3/download/charts?lat={latitude}&lon={longitude}&satnum={satelliteNumber}"

    public static final String AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var3/download/charts?lat=55.0&lon=162&satnum=7"

    public static final String AUTUMN_2020_DOWNLOAD_RESOURCES_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2020/autumn/download/resources"

    public static final String AUTUMN_2020_DOWNLOAD_RESOURCES_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/download/resources"
}
