package com.gvozdev.codegensecurityservice.restadapter.task.autumn2020.var1;

import com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestOperations;

import static com.gvozdev.codegensecurityservice.util.autumn2020.Paths.*;

@RestController("Autumn2020Var1Controller")
@RequestMapping("/api/tasks/year/2020/autumn/var1")
@CrossOrigin("http://localhost:3000")
public class PageController {

    @Autowired
    private RestOperations restOperations;

    @GetMapping("/exercise-info")
    @LoggableSecureEndpoint
    public ResponseEntity<String> getSatellites(@RequestParam("inputfilename") String inputFileName) {
        return restOperations.getForEntity(
            AUTUMN_2020_VAR1_EXERCISE_INFO_PATH,
            String.class,
            inputFileName
        );
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") String lang,
        @PathVariable("file") String file,
        @PathVariable("oop") String oop,
        @PathVariable("inputFileName") String inputFileName,
        @PathVariable("outputFileName") String outputFileName
    ) {
        return restOperations.getForEntity(
            AUTUMN_2020_VAR1_DOWNLOAD_LISTING_PATH,
            byte[].class,
            lang, file, oop, inputFileName, outputFileName
        );
    }

    @GetMapping("/download/charts/{inputFileName}")
    @LoggableSecureEndpoint
    public ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") String inputFileName) {
        return restOperations.getForEntity(
            AUTUMN_2020_VAR1_DOWNLOAD_CHARTS_PATH,
            byte[].class,
            inputFileName
        );
    }
}