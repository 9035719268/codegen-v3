package com.gvozdev.codegensecurityservice.util.auth


import org.springframework.stereotype.Component

import static com.gvozdev.codegensecurityservice.util.config.PortConfig.GATEWAY_PORT
import static com.gvozdev.codegensecurityservice.util.config.PortConfig.TEST_SERVER_PORT

class Paths {
    public static final String LOGIN_PATH = "http://localhost:$TEST_SERVER_PORT/api/login"

    public static final String REFRESH_TOKEN_PATH = "http://localhost:$TEST_SERVER_PORT/api/token/refresh"

    public static final String GATEWAY_SERVER_PATH = "http://localhost:$GATEWAY_PORT/"
}
