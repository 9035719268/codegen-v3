package com.gvozdev.codegensecurityservice.util.auth

class AntMatchers {
    public static final String LOGIN_ANT_MATCHER = '/api/login'

    public static final String REFRESH_TOKEN_ANT_MATCHER = '/api/token/refresh'

    public static final String[] PUBLIC_AUTH_ANT_MATCHERS = [LOGIN_ANT_MATCHER, REFRESH_TOKEN_ANT_MATCHER]
}
