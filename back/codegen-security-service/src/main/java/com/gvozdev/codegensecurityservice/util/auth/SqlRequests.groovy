package com.gvozdev.codegensecurityservice.util.auth

class SqlRequests {
    public static final String FIND_STUDENT_BY_USER_NAME =
            '''
                SELECT * FROM Student WHERE user_name = :userName
            '''

    public static final String GET_STUDENT_AUTHORITIES =
            '''
                SELECT authority FROM Role 
                WHERE id = (SELECT role_id FROM student_role WHERE student_id = :studentId)
            '''
}

