package com.gvozdev.codegensecurityservice.service;

import com.gvozdev.codegensecurityservice.domain.Student;

public interface UserService {
    Student getUserByUserName(String userName);
}
