package com.gvozdev.codegensecurityservice.util.spring2021


import org.springframework.stereotype.Component

import static com.gvozdev.codegensecurityservice.util.config.PortConfig.GATEWAY_PORT
import static com.gvozdev.codegensecurityservice.util.config.PortConfig.TEST_SERVER_PORT

class Paths {
    public static final String SPRING_2021_EXERCISE_INFO_PATH ="http://localhost:$GATEWAY_PORT/api/tasks/year/2021/spring/exercise-info?inputfilename={inputFileName}"

    public static final String SPRING_2021_EXERCISE_INFO_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/exercise-info?inputfilename=arti_6hours.dat"

    public static final String SPRING_2021_DOWNLOAD_LISTING_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2021/spring/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}"

    public static final String SPRING_2021_DOWNLOAD_LISTING_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/download/listing/cpp/file/withoop/arti_6hours.dat/main.cpp"

    public static final String SPRING_2021_DOWNLOAD_CHARTS_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2021/spring/download/charts/{inputFileName}"

    public static final String SPRING_2021_DOWNLOAD_CHARTS_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/download/charts/arti_6hours.dat"

    public static final String SPRING_2021_DOWNLOAD_RESOURCES_PATH = "http://localhost:$GATEWAY_PORT/api/tasks/year/2021/spring/download/resources"

    public static final String SPRING_2021_DOWNLOAD_RESOURCES_SECURE_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/download/resources"
}
