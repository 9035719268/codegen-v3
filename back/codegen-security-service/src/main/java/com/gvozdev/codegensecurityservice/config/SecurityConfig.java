package com.gvozdev.codegensecurityservice.config;

import com.auth0.jwt.algorithms.Algorithm;
import com.gvozdev.codegensecurityservice.filter.CustomAuthenticationFilter;
import com.gvozdev.codegensecurityservice.filter.CustomAuthorizationFilter;
import com.gvozdev.codegensecurityservice.util.auth.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.gvozdev.codegensecurityservice.util.auth.AntMatchers.LOGIN_ANT_MATCHER;
import static com.gvozdev.codegensecurityservice.util.auth.AntMatchers.PUBLIC_AUTH_ANT_MATCHERS;
import static com.gvozdev.codegensecurityservice.util.autumn2020.AntMatchers.*;
import static com.gvozdev.codegensecurityservice.util.autumn2020.UserRoles.*;
import static com.gvozdev.codegensecurityservice.util.spring2021.AntMatchers.SPRING_2021_ANT_MATCHER;
import static com.gvozdev.codegensecurityservice.util.spring2021.UserRoles.SPRING_2021_ALL_USER_ROLES;
import static com.gvozdev.codegensecurityservice.util.swagger.AntMatchers.AUTUMN_2020_SWAGGER_ANT_MATCHER;
import static com.gvozdev.codegensecurityservice.util.swagger.AntMatchers.SPRING_2021_SWAGGER_ANT_MATCHER;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private Algorithm algorithm;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean(), tokenUtil, algorithm);
        customAuthenticationFilter.setFilterProcessesUrl(LOGIN_ANT_MATCHER);

        http
            .cors().and().csrf().disable()
            .sessionManagement().sessionCreationPolicy(STATELESS)
            .and()
            .authorizeRequests().antMatchers(PUBLIC_AUTH_ANT_MATCHERS).permitAll()
            .and()
            .authorizeRequests().antMatchers(AUTUMN_2020_SWAGGER_ANT_MATCHER).hasAnyRole(AUTUMN_2020_ALL_USER_ROLES)
            .and()
            .authorizeRequests().antMatchers(SPRING_2021_SWAGGER_ANT_MATCHER).hasAnyRole(SPRING_2021_ALL_USER_ROLES)
            .and()
            .authorizeRequests().antMatchers(AUTUMN_2020_VAR1_ANT_MATCHER).hasAnyRole(AUTUMN_2020_VAR1_USER_ROLES)
            .and()
            .authorizeRequests().antMatchers(AUTUMN_2020_VAR2_ANT_MATCHER).hasAnyRole(AUTUMN_2020_VAR2_USER_ROLES)
            .and()
            .authorizeRequests().antMatchers(AUTUMN_2020_VAR3_ANT_MATCHER).hasAnyRole(AUTUMN_2020_VAR3_USER_ROLES)
            .and()
            .authorizeRequests().antMatchers(AUTUMN_2020_RESOURCES_ANT_MATCHER).hasAnyRole(AUTUMN_2020_ALL_USER_ROLES)
            .and()
            .authorizeRequests().antMatchers(SPRING_2021_ANT_MATCHER).hasAnyRole(SPRING_2021_ALL_USER_ROLES)
            .and()
            .authorizeRequests().anyRequest().authenticated()
            .and()
            .addFilter(customAuthenticationFilter)
            .addFilterBefore(new CustomAuthorizationFilter(tokenUtil, algorithm), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
