package com.gvozdev.codegensecurityservice.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.lang.reflect.Method;

import static java.util.Arrays.stream;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.security.core.context.SecurityContextHolder.getContext;

@Aspect
@Component
public class LoggingAspect {
    private static final Logger LOGGER = getLogger(LoggingAspect.class);

    @Pointcut("@annotation(com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint)")
    public void loggableSecureEndpointsPointcut() {
    }

    @After("loggableSecureEndpointsPointcut()")
    public void logSecureEndpointUser(JoinPoint joinPoint) {
        Authentication authentication = getContext().getAuthentication();

        String principalName = authentication.getName();

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        String endpoint = stream(method.getAnnotation(GetMapping.class).value())
            .findFirst()
            .orElseThrow();

        LOGGER.info("Пользователь {} обратился к конечной точке {}", principalName, endpoint);
    }
}
