package com.gvozdev.codegensecurityservice.service;

import com.gvozdev.codegensecurityservice.dao.UserDaoImpl;
import com.gvozdev.codegensecurityservice.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserDaoImpl userDaoImpl;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        return userDaoImpl.findUserByUserName(userName)
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Override
    public Student getUserByUserName(String userName) throws UsernameNotFoundException {
        return userDaoImpl.findUserByUserName(userName)
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }
}
