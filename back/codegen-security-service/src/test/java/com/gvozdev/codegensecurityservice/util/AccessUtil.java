package com.gvozdev.codegensecurityservice.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;

import static com.gvozdev.codegensecurityservice.util.auth.Paths.*;
import static org.springframework.http.HttpEntity.EMPTY;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;

@Component
public class AccessUtil {

    @Autowired
    private TestRestTemplate testRestTemplate;

    public boolean isGatewayServiceOnline() {
        try {
            testRestTemplate.exchange(GATEWAY_SERVER_PATH, GET, EMPTY, String.class);

            return true;
        } catch (ResourceAccessException exception) {
            return false;
        }
    }

    public String getAccessToken(String userName, String password) throws JSONException {
        MultiValueMap<String, String> loginBody = new LinkedMultiValueMap<>();
        loginBody.add("username", userName);
        loginBody.add("password", password);

        HttpHeaders loginHeaders = new HttpHeaders();
        loginHeaders.setContentType(APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(loginBody, loginHeaders);
        ResponseEntity<String> response = testRestTemplate.exchange(LOGIN_PATH, POST, httpEntity, String.class);
        JSONObject responseBody = new JSONObject(response.getBody());

        return "Bearer " + responseBody.getString("access_token");
    }

    public String getRefreshToken(String userName, String password) throws JSONException {
        MultiValueMap<String, String> loginBody = new LinkedMultiValueMap<>();
        loginBody.add("username", userName);
        loginBody.add("password", password);

        HttpHeaders loginHeaders = new HttpHeaders();
        loginHeaders.setContentType(APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(loginBody, loginHeaders);
        ResponseEntity<String> response = testRestTemplate.exchange(LOGIN_PATH, POST, httpEntity, String.class);
        JSONObject responseBody = new JSONObject(response.getBody());

        return "Bearer " + responseBody.getString("refresh_token");
    }

    public String getNewAccessToken(String refreshToken) throws JSONException {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTHORIZATION, refreshToken);

        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<String> response = testRestTemplate.exchange(REFRESH_TOKEN_PATH, GET, httpEntity, String.class);
        JSONObject responseBody = new JSONObject(response.getBody());

        return "Bearer " + responseBody.getString("access_token");
    }

    public ResponseEntity<String> getResponse(String accessToken, String path) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(AUTHORIZATION, accessToken);

        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);

        return testRestTemplate.exchange(path, GET, httpEntity, String.class);
    }
}
