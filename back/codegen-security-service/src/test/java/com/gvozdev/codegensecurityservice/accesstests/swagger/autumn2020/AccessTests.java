package com.gvozdev.codegensecurityservice.accesstests.swagger.autumn2020;

import com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.codegensecurityservice.restadapter.swagger.PageController;
import com.gvozdev.codegensecurityservice.util.AccessUtil;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.lang.reflect.Method;

import static com.gvozdev.codegensecurityservice.util.swagger.Paths.AUTUMN_2020_SWAGGER_SECURE_PATH;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class AccessTests {

    @Autowired
    private AccessUtil accessUtil;

    @Value("${admin.username}")
    private String adminUserName;

    @Value("${admin.password}")
    private String adminPassword;

    @Value("${a20201.username}")
    private String a20201UserName;

    @Value("${a20201.password}")
    private String a20201Password;

    @Value("${a20202.username}")
    private String a20202UserName;

    @Value("${a20202.password}")
    private String a20202Password;

    @Value("${a20203.username}")
    private String a20203UserName;

    @Value("${a20203.password}")
    private String a20203Password;

    @Value("${s2021.username}")
    private String s2021UserName;

    @Value("${s2021.password}")
    private String s2021Password;

    @BeforeEach
    void checkIfGatewayServiceOnline() {
        assumeTrue(accessUtil.isGatewayServiceOnline(), "Шлюзовый сервис недоступен");
    }

    @Test
    void shouldCheckAdminAccessGetSwaggerPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(adminUserName, adminPassword);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_SWAGGER_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(3318, responseBodyLength);
    }

    @Test
    void shouldCheckA20201AccessGetSwaggerPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20201UserName, a20201Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_SWAGGER_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(3318, responseBodyLength);
    }

    @Test
    void shouldCheckA20202AccessGetSwaggerPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20202UserName, a20202Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_SWAGGER_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(3318, responseBodyLength);
    }

    @Test
    void shouldCheckA20203AccessGetSwaggerPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20203UserName, a20203Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_SWAGGER_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(3318, responseBodyLength);
    }

    @Test
    void shouldCheckS2021AccessGetSwaggerPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(s2021UserName, s2021Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_SWAGGER_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckLoggableEndpoints() throws NoSuchMethodException {
        Method getAutumn2020Swagger = PageController.class.getMethod("getAutumn2020Swagger");

        assertTrue(getAutumn2020Swagger.isAnnotationPresent(LoggableSecureEndpoint.class));
    }
}
