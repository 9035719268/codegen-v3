package com.gvozdev.codegensecurityservice.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static com.gvozdev.codegensecurityservice.domain.Authority.*;
import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.TimeUnit.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class AccessUtilTests {

    @Autowired
    private AccessUtil accessUtil;

    @Value("${admin.username}")
    private String adminUserName;

    @Value("${admin.password}")
    private String adminPassword;

    @Value("${a20201.username}")
    private String a20201UserName;

    @Value("${a20201.password}")
    private String a20201Password;

    @Value("${a20202.username}")
    private String a20202UserName;

    @Value("${a20202.password}")
    private String a20202Password;

    @Value("${a20203.username}")
    private String a20203UserName;

    @Value("${a20203.password}")
    private String a20203Password;

    @Value("${s2021.username}")
    private String s2021UserName;

    @Value("${s2021.password}")
    private String s2021Password;

    @Test
    void shouldCheckGetAccessTokenAdmin() throws JSONException {
        String accessTokenWithPrefix = accessUtil.getAccessToken(adminUserName, adminPassword);
        String accessToken = accessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(accessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(adminUserName, subject);
        assertThat(roles).contains(ROLE_ADMIN.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetRefreshTokenAdmin() throws JSONException {
        String refreshTokenWithPrefix = accessUtil.getRefreshToken(adminUserName, adminPassword);
        String refreshToken = refreshTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(refreshToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(adminUserName, subject);
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(1439L, 1440L);
    }

    @Test
    void shouldCheckGetAccessTokenFromRefreshTokenAdmin() throws JSONException {
        String refreshToken = accessUtil.getRefreshToken(adminUserName, adminPassword);
        String newAccessTokenWithPrefix = accessUtil.getNewAccessToken(refreshToken);
        String newAccessToken = newAccessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(newAccessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(adminUserName, subject);
        assertThat(roles).contains(ROLE_ADMIN.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetAccessTokenA20201() throws JSONException {
        String accessTokenWithPrefix = accessUtil.getAccessToken(a20201UserName, a20201Password);
        String accessToken = accessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(accessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20201UserName, subject);
        assertThat(roles).contains(ROLE_A20201.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetRefreshTokenA20201() throws JSONException {
        String refreshTokenWithPrefix = accessUtil.getRefreshToken(a20201UserName, a20201Password);
        String refreshToken = refreshTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(refreshToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20201UserName, subject);
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(1439L, 1440L);
    }

    @Test
    void shouldCheckGetAccessTokenFromRefreshTokenA20201() throws JSONException {
        String refreshToken = accessUtil.getRefreshToken(a20201UserName, a20201Password);
        String newAccessTokenWithPrefix = accessUtil.getNewAccessToken(refreshToken);
        String newAccessToken = newAccessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(newAccessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20201UserName, subject);
        assertThat(roles).contains(ROLE_A20201.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetAccessTokenA20202() throws JSONException {
        String accessTokenWithPrefix = accessUtil.getAccessToken(a20202UserName, a20202Password);
        String accessToken = accessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(accessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20202UserName, subject);
        assertThat(roles).contains(ROLE_A20202.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetRefreshTokenA20202() throws JSONException {
        String refreshTokenWithPrefix = accessUtil.getRefreshToken(a20202UserName, a20202Password);
        String refreshToken = refreshTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(refreshToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20202UserName, subject);
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(1439L, 1440L);
    }

    @Test
    void shouldCheckGetAccessTokenFromRefreshTokenA20202() throws JSONException {
        String refreshToken = accessUtil.getRefreshToken(a20202UserName, a20202Password);
        String newAccessTokenWithPrefix = accessUtil.getNewAccessToken(refreshToken);
        String newAccessToken = newAccessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(newAccessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20202UserName, subject);
        assertThat(roles).contains(ROLE_A20202.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetAccessTokenA20203() throws JSONException {
        String accessTokenWithPrefix = accessUtil.getAccessToken(a20203UserName, a20203Password);
        String accessToken = accessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(accessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20203UserName, subject);
        assertThat(roles).contains(ROLE_A20203.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetRefreshTokenA20203() throws JSONException {
        String refreshTokenWithPrefix = accessUtil.getRefreshToken(a20203UserName, a20203Password);
        String refreshToken = refreshTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(refreshToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20203UserName, subject);
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(1439L, 1440L);
    }

    @Test
    void shouldCheckGetAccessTokenFromRefreshTokenA20203() throws JSONException {
        String refreshToken = accessUtil.getRefreshToken(a20203UserName, a20203Password);
        String newAccessTokenWithPrefix = accessUtil.getNewAccessToken(refreshToken);
        String newAccessToken = newAccessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(newAccessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(a20203UserName, subject);
        assertThat(roles).contains(ROLE_A20203.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetAccessTokenS2021() throws JSONException {
        String accessTokenWithPrefix = accessUtil.getAccessToken(s2021UserName, s2021Password);
        String accessToken = accessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(accessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(s2021UserName, subject);
        assertThat(roles).contains(ROLE_S2021.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }

    @Test
    void shouldCheckGetRefreshTokenS2021() throws JSONException {
        String refreshTokenWithPrefix = accessUtil.getRefreshToken(s2021UserName, s2021Password);
        String refreshToken = refreshTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(refreshToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(s2021UserName, subject);
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(1439L, 1440L);
    }

    @Test
    void shouldCheckGetAccessTokenFromRefreshTokenS2021() throws JSONException {
        String refreshToken = accessUtil.getRefreshToken(s2021UserName, s2021Password);
        String newAccessTokenWithPrefix = accessUtil.getNewAccessToken(refreshToken);
        String newAccessToken = newAccessTokenWithPrefix.substring("Bearer ".length());
        DecodedJWT decodedJWT = JWT.decode(newAccessToken);

        String header = decodedJWT.getHeader();
        String payload = decodedJWT.getPayload();
        String signature = decodedJWT.getSignature();
        String subject = decodedJWT.getSubject();
        List<String> roles = decodedJWT.getClaim("roles").asList(String.class);
        long expirationTimeInMillis = decodedJWT.getExpiresAt().getTime() - currentTimeMillis();

        assertNotNull(header);
        assertNotNull(payload);
        assertNotNull(signature);
        assertEquals(s2021UserName, subject);
        assertThat(roles).contains(ROLE_S2021.toString());
        assertThat(MILLISECONDS.toMinutes(expirationTimeInMillis)).isBetween(29L, 30L);
    }
}
