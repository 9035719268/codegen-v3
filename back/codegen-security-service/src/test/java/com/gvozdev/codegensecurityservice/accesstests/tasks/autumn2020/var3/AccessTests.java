package com.gvozdev.codegensecurityservice.accesstests.tasks.autumn2020.var3;

import com.gvozdev.codegensecurityservice.annotation.LoggableSecureEndpoint;
import com.gvozdev.codegensecurityservice.restadapter.task.autumn2020.var3.PageController;
import com.gvozdev.codegensecurityservice.util.AccessUtil;
import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import java.lang.reflect.Method;

import static com.gvozdev.codegensecurityservice.util.autumn2020.Paths.*;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class AccessTests {

    @Autowired
    private AccessUtil accessUtil;

    @Value("${admin.username}")
    private String adminUserName;

    @Value("${admin.password}")
    private String adminPassword;

    @Value("${a20201.username}")
    private String a20201UserName;

    @Value("${a20201.password}")
    private String a20201Password;

    @Value("${a20202.username}")
    private String a20202UserName;

    @Value("${a20202.password}")
    private String a20202Password;

    @Value("${a20203.username}")
    private String a20203UserName;

    @Value("${a20203.password}")
    private String a20203Password;

    @Value("${s2021.username}")
    private String s2021UserName;

    @Value("${s2021.password}")
    private String s2021Password;

    @BeforeEach
    void checkIfGatewayServiceOnline() {
        assumeTrue(accessUtil.isGatewayServiceOnline(), "Шлюзовый сервис недоступен");
    }

    @Test
    void shouldCheckAdminAccessExerciseInfoPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(adminUserName, adminPassword);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_EXERCISE_INFO_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(790, responseBodyLength);
    }

    @Test
    void shouldCheckAdminAccessDownloadListingPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(adminUserName, adminPassword);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_LISTING_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(26823, responseBodyLength);
    }

    @Test
    void shouldCheckAdminAccessDownloadChartsPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(adminUserName, adminPassword);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(71612, responseBodyLength);
    }

    @Test
    void shouldCheckA20201AccessExerciseInfoPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20201UserName, a20201Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_EXERCISE_INFO_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckA20201AccessDownloadListingPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20201UserName, a20201Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_LISTING_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckA20201AccessDownloadChartsPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20201UserName, a20201Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckA20202AccessExerciseInfoPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20202UserName, a20202Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_EXERCISE_INFO_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckA20202AccessDownloadListingPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20202UserName, a20202Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_LISTING_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckA20202AccessDownloadChartsPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20202UserName, a20202Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckA20203AccessExerciseInfoPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20203UserName, a20203Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_EXERCISE_INFO_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(790, responseBodyLength);
    }

    @Test
    void shouldCheckA20203AccessDownloadListingPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20203UserName, a20203Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_LISTING_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(26823, responseBodyLength);
    }

    @Test
    void shouldCheckA20203AccessDownloadChartsPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(a20203UserName, a20203Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(71612, responseBodyLength);
    }

    @Test
    void shouldCheckS2021AccessExerciseInfoPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(s2021UserName, s2021Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_EXERCISE_INFO_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckS2021AccessDownloadListingPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(s2021UserName, s2021Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_LISTING_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckS2021AccessDownloadChartsPath() throws JSONException {
        String accessToken = accessUtil.getAccessToken(s2021UserName, s2021Password);
        ResponseEntity<String> response = accessUtil.getResponse(accessToken, AUTUMN_2020_VAR3_DOWNLOAD_CHARTS_SECURE_PATH);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(403, responseStatusCode);
    }

    @Test
    void shouldCheckLoggableEndpoints() throws NoSuchMethodException {
        Method getSatellites = PageController.class.getMethod(
            "getComponents",
            String.class, String.class, String.class
        );
        Method downloadListing = PageController.class.getMethod(
            "downloadListing",
            String.class, String.class, String.class, String.class, String.class, String.class, String.class
        );
        Method downloadCharts = PageController.class.getMethod(
            "downloadCharts",
            String.class, String.class, String.class
        );

        assertTrue(getSatellites.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(downloadListing.isAnnotationPresent(LoggableSecureEndpoint.class));
        assertTrue(downloadCharts.isAnnotationPresent(LoggableSecureEndpoint.class));
    }
}
