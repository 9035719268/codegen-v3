package com.gvozdev.codegengatewayservice;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableEurekaClient
public class CodegenGatewayServiceApplication {
    public static void main(String[] args) {
        run(CodegenGatewayServiceApplication.class, args);
    }
}
