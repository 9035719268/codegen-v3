package com.gvozdev.codegenserviceregistry;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableEurekaServer
public class CodegenServiceRegistryApplication {
    public static void main(String[] args) {
        run(CodegenServiceRegistryApplication.class, args);
    }
}
