package com.gvozdev.codegen.spring2021;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.repo.OutputFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;

@Component
public class OutputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/output/";

    @Autowired
    private OutputFileRepository repository;

    @PostConstruct
    @Transactional
    public void init() {
        saveOutputFile(1L, "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(2L, "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(3L, "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(4L, "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(5L, "java", "file", "withoop", "Main.java");
        saveOutputFile(6L, "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(7L, "java", "manual", "withoop", "Main.java");
        saveOutputFile(8L, "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(9L, "python", "file", "nooop", "main.py");
        saveOutputFile(10L, "python", "file", "withoop", "main.py");
        saveOutputFile(11L, "python", "manual", "nooop", "main.py");
        saveOutputFile(12L, "python", "manual", "withoop", "main.py");

        LOGGER.info("Все выходные файлы для УИРС весны 2021 загружены");
    }

    private void saveOutputFile(Long id, String lang, String file, String oop, String name) {
        try {
            String filePath = PATH + "/" + lang + "/" + file + "/" + oop + "/" + name;
            File codeListing = new File(filePath);
            byte[] fileBytes = Files.readAllBytes(codeListing.toPath());
            OutputFile inputFile = new OutputFile(id, lang, file, oop, name, fileBytes, new Date());
            repository.save(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
