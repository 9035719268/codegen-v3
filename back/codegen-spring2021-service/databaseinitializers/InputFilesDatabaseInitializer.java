package com.gvozdev.codegen.spring2021.init;

import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.repo.InputFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class InputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/input/";

    @Autowired
    private InputFileRepository repository;

    @PostConstruct
    @Transactional
    public void init() {
        saveInputFile(1L, "arti_6hours.dat");
        saveInputFile(2L, "bshm_1-10.dat");
        saveInputFile(3L, "gele_6hours.dat");
        saveInputFile(4L, "hueg_1-10.dat");
        saveInputFile(5L, "irkm_6hours.dat");
        saveInputFile(6L, "kslv_6hours.dat");
        saveInputFile(7L, "leij_1-10.dat");
        saveInputFile(8L, "maga_6hours.dat");
        saveInputFile(9L, "menp_6hours.dat");
        saveInputFile(10L, "novs_6hours.dat");
        saveInputFile(11L, "noyb_6hours.dat");
        saveInputFile(12L, "onsa_1-10.dat");
        saveInputFile(13L, "spt0_1-10.dat");
        saveInputFile(14L, "svet_6hours.dat");
        saveInputFile(15L, "svtl_1-10.dat");
        saveInputFile(16L, "tit2_1-10.dat");
        saveInputFile(17L, "vis0_1-10.dat");
        saveInputFile(18L, "vlad_6hours.dat");
        saveInputFile(19L, "warn_1-10.dat");
        saveInputFile(20L, "ykts_6hours.dat");
        saveInputFile(21L, "yusa_6hours.dat");
        saveInputFile(22L, "zeck_1-10.dat");
        saveInputFile(23L, "resources.rar");
        LOGGER.info("Все входные файлы для УИРС весны 2021 загружены");
    }

    private void saveInputFile(Long id, String fileName) {
        try {
            File file = new File(PATH + fileName);
            byte[] fileBytes = Files.readAllBytes(file.toPath());
            InputFile inputFile = new InputFile(id, fileName, fileBytes);
            repository.save(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
