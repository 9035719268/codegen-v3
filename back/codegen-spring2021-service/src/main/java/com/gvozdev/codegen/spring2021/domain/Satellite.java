package com.gvozdev.codegen.spring2021.domain;

import java.util.List;

public class Satellite {
    private final int number;
    private final List<Double> md;
    private final List<Double> td;
    private final List<Double> mw;
    private final List<Double> tw;
    private final List<Double> elevation;

    public Satellite(int number, List<Double> md, List<Double> td, List<Double> mw, List<Double> tw, List<Double> elevation) {
        this.number = number;
        this.md = md;
        this.td = td;
        this.mw = mw;
        this.tw = tw;
        this.elevation = elevation;
    }

    public int getNumber() {
        return number;
    }

    public List<Double> getMd() {
        return md;
    }

    public List<Double> getTd() {
        return td;
    }

    public List<Double> getMw() {
        return mw;
    }

    public List<Double> getTw() {
        return tw;
    }

    public List<Double> getElevation() {
        return elevation;
    }
}
