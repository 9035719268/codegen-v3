package com.gvozdev.codegen.spring2021.service;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.repo.OutputFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OutputFileServiceImpl implements OutputFileService {

    @Autowired
    private OutputFileRepository outputFileRepository;

    @Override
    public OutputFile findByParameters(String lang, String file, String oop, String name) {
        return outputFileRepository.findFileByParameters(lang, file, oop, name);
    }
}
