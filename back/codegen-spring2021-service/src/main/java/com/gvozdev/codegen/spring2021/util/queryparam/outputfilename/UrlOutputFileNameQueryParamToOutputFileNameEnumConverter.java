package com.gvozdev.codegen.spring2021.util.queryparam.outputfilename;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.codegen.spring2021.util.queryparam.outputfilename.OutputFileName.getFileNameFromString;

@Component
public class UrlOutputFileNameQueryParamToOutputFileNameEnumConverter implements Converter<String, OutputFileName> {

    @Override
    public OutputFileName convert(String source) {
        return getFileNameFromString(source);
    }
}
