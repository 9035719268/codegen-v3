package com.gvozdev.codegen.spring2021.util.queryparam.oop;

public enum Oop {
    WITHOOP, NOOOP;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
