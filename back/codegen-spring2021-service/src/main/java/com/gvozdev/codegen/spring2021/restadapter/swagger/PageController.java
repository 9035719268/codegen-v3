package com.gvozdev.codegen.spring2021.restadapter.swagger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("SwaggerController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/year/2021/spring")
public class PageController {

    @GetMapping("/swagger-ui")
    public String getSwagger() {
        return "redirect:/swagger-ui.html";
    }
}
