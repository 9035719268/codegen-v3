package com.gvozdev.codegen.spring2021.util.chart;

import com.gvozdev.codegen.spring2021.domain.ElevationAngles;
import com.gvozdev.codegen.spring2021.domain.Satellite;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTitleAnnotation;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.List;

import static java.awt.Color.WHITE;
import static java.util.stream.IntStream.range;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import static org.jfree.chart.ui.RectangleAnchor.RIGHT;

public class ElevationAnglesChartDrawer extends TemplateChartDrawer {
    public ElevationAnglesChartDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @Override
    public XYSeriesCollection getMeasurementsSeriesCollection() {
        ElevationAngles elevationAnglesForSatellite1 = new ElevationAngles(satellite1);
        List<Double> satellite1Angles = elevationAnglesForSatellite1.getAnglesInHalfCircles();

        ElevationAngles elevationAnglesForSatellite2 = new ElevationAngles(satellite2);
        List<Double> satellite2Angles = elevationAnglesForSatellite2.getAnglesInHalfCircles();

        ElevationAngles elevationAnglesForSatellite3 = new ElevationAngles(satellite3);
        List<Double> satellite3Angles = elevationAnglesForSatellite3.getAnglesInHalfCircles();

        XYSeries satellite1ElevationSeries = new XYSeries("Спутник #" + satellite1.getNumber());
        XYSeries satellite2ElevationSeries = new XYSeries("Спутник #" + satellite2.getNumber());
        XYSeries satellite3ElevationSeries = new XYSeries("Спутник #" + satellite3.getNumber());

        range(0, amountOfObservations).forEach(observation -> {
            double troposphericDelayForSatellite1 = satellite1Angles.get(observation);
            satellite1ElevationSeries.add(observation, troposphericDelayForSatellite1);

            double troposphericDelayForSatellite2 = satellite2Angles.get(observation);
            satellite2ElevationSeries.add(observation, troposphericDelayForSatellite2);

            double troposphericDelayForSatellite3 = satellite3Angles.get(observation);
            satellite3ElevationSeries.add(observation, troposphericDelayForSatellite3);
        });

        XYSeriesCollection elevationSeriesCollection = new XYSeriesCollection();
        elevationSeriesCollection.addSeries(satellite1ElevationSeries);
        elevationSeriesCollection.addSeries(satellite2ElevationSeries);
        elevationSeriesCollection.addSeries(satellite3ElevationSeries);

        return elevationSeriesCollection;
    }

    @Override
    public JFreeChart createChart(XYSeriesCollection observationsSeriesCollection) {
        return createXYLineChart(
            "",
            "Время",
            "Угол возвышения спутника, полуциклы",
            observationsSeriesCollection,
            VERTICAL,
            true,
            true,
            false
        );
    }

    @Override
    public void configureLegend(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        LegendTitle legend = measurementsChart.getLegend();
        legend.setItemFont(legendFont);
        legend.setBackgroundPaint(WHITE);
        legend.setFrame(new BlockBorder(WHITE));
        XYTitleAnnotation titleAnnotation = new XYTitleAnnotation(0.98, 0.05, legend, RIGHT);

        titleAnnotation.setMaxWidth(0.48);
        plot.addAnnotation(titleAnnotation);

        measurementsChart.removeLegend();
    }
}
