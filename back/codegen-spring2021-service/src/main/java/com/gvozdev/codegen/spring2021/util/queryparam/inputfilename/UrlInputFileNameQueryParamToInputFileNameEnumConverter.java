package com.gvozdev.codegen.spring2021.util.queryparam.inputfilename;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.codegen.spring2021.util.queryparam.inputfilename.InputFileName.getFileNameFromString;

@Component
public class UrlInputFileNameQueryParamToInputFileNameEnumConverter implements Converter<String, InputFileName> {

    @Override
    public InputFileName convert(String source) {
        return getFileNameFromString(source);
    }
}
