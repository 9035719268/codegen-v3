package com.gvozdev.codegen.spring2021.util.queryparam.oop;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.codegen.spring2021.util.queryparam.oop.Oop.valueOf;

@Component
public class UrlOopQueryParamToOopEnumConverter implements Converter<String, Oop> {

    @Override
    public Oop convert(String source) {
        return valueOf(source.toUpperCase());
    }
}