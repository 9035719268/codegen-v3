package com.gvozdev.codegen.spring2021.config;

import com.gvozdev.codegen.spring2021.util.queryparam.file.UrlFileQueryParamToFileEnumConverter;
import com.gvozdev.codegen.spring2021.util.queryparam.inputfilename.UrlInputFileNameQueryParamToInputFileNameEnumConverter;
import com.gvozdev.codegen.spring2021.util.queryparam.lang.UrlLangQueryParamToLangEnumConverter;
import com.gvozdev.codegen.spring2021.util.queryparam.oop.UrlOopQueryParamToOopEnumConverter;
import com.gvozdev.codegen.spring2021.util.queryparam.outputfilename.UrlOutputFileNameQueryParamToOutputFileNameEnumConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new UrlLangQueryParamToLangEnumConverter());
        registry.addConverter(new UrlFileQueryParamToFileEnumConverter());
        registry.addConverter(new UrlOopQueryParamToOopEnumConverter());
        registry.addConverter(new UrlInputFileNameQueryParamToInputFileNameEnumConverter());
        registry.addConverter(new UrlOutputFileNameQueryParamToOutputFileNameEnumConverter());
    }
}