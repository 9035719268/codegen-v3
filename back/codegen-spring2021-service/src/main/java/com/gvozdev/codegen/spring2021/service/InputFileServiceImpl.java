package com.gvozdev.codegen.spring2021.service;

import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.repo.InputFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InputFileServiceImpl implements InputFileService {

    @Autowired
    private InputFileRepository inputFileRepository;

    @Override
    public InputFile findByName(String name) {
        return inputFileRepository.findByName(name);
    }
}
