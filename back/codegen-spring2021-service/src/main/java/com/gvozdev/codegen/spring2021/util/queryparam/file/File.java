package com.gvozdev.codegen.spring2021.util.queryparam.file;

public enum File {
    FILE, MANUAL;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
