package com.gvozdev.codegen.spring2021.util.common;

import org.springframework.stereotype.Component;

import static java.lang.Integer.parseInt;
import static java.lang.Math.abs;

@Component
public class CommonUtil {
    public char toChar(int number) {
        return (char) (number + 48);
    }

    public boolean isDoubleDigit(int number) {
        return Integer.toString(abs(number)).trim().length() == 2;
    }

    public int getFirstDigit(int number) {
        return parseInt(Integer.toString(number).substring(0, 1));
    }

    public int getSecondDigit(int number) {
        return parseInt(Integer.toString(number).substring(1, 2));
    }
}
