package com.gvozdev.codegen.spring2021.domain;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class TroposphericDelays {
    private final Satellite satellite;
    private final int amountOfObservations;

    public TroposphericDelays(Satellite satellite, int amountOfObservations) {
        this.satellite = satellite;
        this.amountOfObservations = amountOfObservations;
    }

    public List<Double> getTroposphericDelays() {
        List<Double> mdArray = satellite.getMd();
        List<Double> tdArray = satellite.getTd();
        List<Double> mwArray = satellite.getMw();
        List<Double> twArray = satellite.getTw();

        return range(0, amountOfObservations)
            .mapToObj(observation -> {
                double md = mdArray.get(observation);
                double td = tdArray.get(observation);
                double mw = mwArray.get(observation);
                double tw = twArray.get(observation);

                return md * td + mw * tw;
            })
            .collect(toList());
    }
}
