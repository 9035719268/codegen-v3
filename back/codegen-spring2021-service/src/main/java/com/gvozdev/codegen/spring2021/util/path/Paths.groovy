package com.gvozdev.codegen.spring2021.util.path

import org.springframework.stereotype.Component

import static com.gvozdev.codegen.spring2021.util.config.PortConfig.TEST_SERVER_PORT

class Paths {
	public static final String EXERCISE_INFO_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/exercise-info?inputfilename={inputFileName}"

	public static final String DOWNLOAD_LISTING_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}"

	public static final String DOWNLOAD_CHARTS_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/download/charts/{inputFileName}"

	public static final String DOWNLOAD_RESOURCES_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2021/spring/download/resources"

	public static final String SWAGGER_UI_PATH = "http://localhost:$TEST_SERVER_PORT/api/year/2021/spring/swagger-ui"

	public static final String ACTUATOR_PATH = "http://localhost:$TEST_SERVER_PORT/api/year/2021/spring/actuator"
}