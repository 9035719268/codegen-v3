package com.gvozdev.codegen.spring2021.util.queryparam.lang;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.codegen.spring2021.util.queryparam.lang.Lang.valueOf;

@Component
public class UrlLangQueryParamToLangEnumConverter implements Converter<String, Lang> {

    @Override
    public Lang convert(String source) {
        return valueOf(source.toUpperCase());
    }
}