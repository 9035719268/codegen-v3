package com.gvozdev.codegen.spring2021.domain;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class ElevationAngles {
    private final Satellite satellite;

    public ElevationAngles(Satellite satellite) {
        this.satellite = satellite;
    }

    public List<Double> getAnglesInHalfCircles() {
        int halfCircleInDegrees = 180;
        List<Double> elevationAnglesInDegrees = satellite.getElevation();

        return elevationAnglesInDegrees.stream()
            .map(angleInDegrees -> angleInDegrees / halfCircleInDegrees)
            .collect(toList());
    }
}