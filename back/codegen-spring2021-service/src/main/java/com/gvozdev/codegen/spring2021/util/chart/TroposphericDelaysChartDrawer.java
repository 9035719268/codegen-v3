package com.gvozdev.codegen.spring2021.util.chart;

import com.gvozdev.codegen.spring2021.domain.Satellite;
import com.gvozdev.codegen.spring2021.domain.TroposphericDelays;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTitleAnnotation;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.List;

import static java.awt.Color.WHITE;
import static java.util.stream.IntStream.range;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import static org.jfree.chart.ui.RectangleAnchor.RIGHT;

public class TroposphericDelaysChartDrawer extends TemplateChartDrawer {
    public TroposphericDelaysChartDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @Override
    public XYSeriesCollection getMeasurementsSeriesCollection() {
        TroposphericDelays troposphericDelaysForSatellite1 = new TroposphericDelays(satellite1, amountOfObservations);
        List<Double> satellite1Delays = troposphericDelaysForSatellite1.getTroposphericDelays();

        TroposphericDelays troposphericDelaysForSatellite2 = new TroposphericDelays(satellite2, amountOfObservations);
        List<Double> satellite2Delays = troposphericDelaysForSatellite2.getTroposphericDelays();

        TroposphericDelays troposphericDelaysForSatellite3 = new TroposphericDelays(satellite3, amountOfObservations);
        List<Double> satellite3Delays = troposphericDelaysForSatellite3.getTroposphericDelays();

        XYSeries satellite1DelaysSeries = new XYSeries("Спутник #" + satellite1.getNumber());
        XYSeries satellite2DelaysSeries = new XYSeries("Спутник #" + satellite2.getNumber());
        XYSeries satellite3DelaysSeries = new XYSeries("Спутник #" + satellite3.getNumber());

        range(0, amountOfObservations).forEach(observation -> {
            double troposphericDelayForSatellite1 = satellite1Delays.get(observation);
            satellite1DelaysSeries.add(observation, troposphericDelayForSatellite1);

            double troposphericDelayForSatellite2 = satellite2Delays.get(observation);
            satellite2DelaysSeries.add(observation, troposphericDelayForSatellite2);

            double troposphericDelayForSatellite3 = satellite3Delays.get(observation);
            satellite3DelaysSeries.add(observation, troposphericDelayForSatellite3);
        });

        XYSeriesCollection troposphericDelaysSeriesCollection = new XYSeriesCollection();
        troposphericDelaysSeriesCollection.addSeries(satellite1DelaysSeries);
        troposphericDelaysSeriesCollection.addSeries(satellite2DelaysSeries);
        troposphericDelaysSeriesCollection.addSeries(satellite3DelaysSeries);

        return troposphericDelaysSeriesCollection;
    }

    @Override
    public JFreeChart createChart(XYSeriesCollection observationsSeriesCollection) {
        return createXYLineChart(
            "",
            "Время",
            "Тропосферная задержка сигнала, метры",
            observationsSeriesCollection,
            VERTICAL,
            true,
            true,
            false
        );
    }

    @Override
    public void configureLegend(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        LegendTitle legend = measurementsChart.getLegend();
        legend.setItemFont(legendFont);
        legend.setBackgroundPaint(WHITE);
        legend.setFrame(new BlockBorder(WHITE));
        XYTitleAnnotation titleAnnotation = new XYTitleAnnotation(0.98, 0.95, legend, RIGHT);

        titleAnnotation.setMaxWidth(0.48);
        plot.addAnnotation(titleAnnotation);

        measurementsChart.removeLegend();
    }
}