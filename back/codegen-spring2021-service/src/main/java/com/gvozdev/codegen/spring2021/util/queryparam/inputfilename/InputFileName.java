package com.gvozdev.codegen.spring2021.util.queryparam.inputfilename;

import java.util.stream.Stream;

public enum InputFileName {
    ARTI_6("arti_6hours.dat"),
    BSHM_10("bshm_1-10.dat"),
    GELE_6("gele_6hours.dat"),
    HUEG_10("hueg_1-10.dat"),
    IRKM_6("irkm_6hours.dat"),
    KSLV_6("kslv_6hours.dat"),
    LEIJ_10("leij_1-10.dat"),
    MAGA_6("maga_6hours.dat"),
    MENP_6("menp_6hours.dat"),
    NOVS_6("novs_6hours.dat"),
    NOYB_6("noyb_6hours.dat"),
    ONSA_10("onsa_1-10.dat"),
    SPT_10("spt0_1-10.dat"),
    SVET_6("svet_6hours.dat"),
    SVTL_10("svtl_1-10.dat"),
    TIT_10("tit2_1-10.dat"),
    VIS_10("vis0_1-10.dat"),
    VLAD_6("vlad_6hours.dat"),
    WARN_10("warn_1-10.dat"),
    YKTS_6("ykts_6hours.dat"),
    YUSA_6("yusa_6hours.dat"),
    ZECK_10("zeck_1-10.dat"),
    RESOURCES("resources.rar");

    private final String fileName;

    InputFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public static InputFileName getFileNameFromString(String otherName) {
        return Stream.of(values())
            .filter(inputFileName -> inputFileName.getFileName().equalsIgnoreCase(otherName))
            .findFirst()
            .orElseThrow();
    }
}
