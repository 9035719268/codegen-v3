package com.gvozdev.codegen.spring2021.util.number;

import static java.util.Objects.hash;

public class TwoDigitNumber {
    private static final int SIZE = 2;

    private final char firstDigit;
    private final char secondDigit;

    public TwoDigitNumber(char firstDigit, char secondDigit) {
        this.firstDigit = firstDigit;
        this.secondDigit = secondDigit;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        TwoDigitNumber otherNumber = (TwoDigitNumber) other;

        return firstDigit == otherNumber.getFirstDigit() && secondDigit == otherNumber.getSecondDigit();
    }

    @Override
    public int hashCode() {
        return hash(firstDigit, secondDigit, SIZE);
    }

    private char getFirstDigit() {
        return firstDigit;
    }

    private char getSecondDigit() {
        return secondDigit;
    }
}
