package com.gvozdev.codegen.spring2021.repo;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OutputFileRepository extends JpaRepository<OutputFile, Long> {

    @Query(
        value = "SELECT * FROM output_file f " +
                "WHERE f.lang = ?1 " +
                "AND f.file = ?2 " +
                "AND f.oop = ?3 " +
                "AND f.name = ?4 ",
        nativeQuery = true
    )
    OutputFile findFileByParameters(String lang, String file, String oop, String name);
}
