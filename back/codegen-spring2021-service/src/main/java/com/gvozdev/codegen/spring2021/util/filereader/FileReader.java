package com.gvozdev.codegen.spring2021.util.filereader;

import com.gvozdev.codegen.spring2021.util.componentindex.ComponentIndexes;
import com.gvozdev.codegen.spring2021.util.number.OneDigitNumber;
import com.gvozdev.codegen.spring2021.util.number.TwoDigitNumber;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static java.util.stream.Stream.concat;

public class FileReader {
    private final List<List<List<Byte>>> allLines;
    private final ComponentIndexes componentIndexes;
    private final FileReaderUtil fileReaderUtil;
    private final int amountOfObservations;

    public FileReader(byte[] fileBytes, int amountOfObservations, ComponentIndexes componentIndexes, FileReaderUtil fileReaderUtil) {
        allLines = fileReaderUtil.extractLinesOfData(fileBytes);
        this.amountOfObservations = amountOfObservations;
        this.componentIndexes = componentIndexes;
        this.fileReaderUtil = fileReaderUtil;
    }

    public List<Integer> getSatelliteNumbers() {
        List<Integer> oneDigitSatelliteNumbers = fileReaderUtil.getOneDigitSatelliteNumbers(allLines);
        List<Integer> twoDigitSatelliteNumbers = fileReaderUtil.getTwoDigitSatelliteNumbers(allLines);

        return concat(oneDigitSatelliteNumbers.stream(), twoDigitSatelliteNumbers.stream())
            .collect(toList());
    }

    public List<List<Double>> getTroposphericDelayComponents(OneDigitNumber requiredSatelliteNumber) {
        int mdIndex = componentIndexes.getMdIndex();
        int tdIndex = componentIndexes.getTdIndex();
        int mwIndex = componentIndexes.getMwIndex();
        int twIndex = componentIndexes.getTwIndex();
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        List<Double> mdList = getTroposphericDelayComponentList(measurements, mdIndex);
        List<Double> tdList = getTroposphericDelayComponentList(measurements, tdIndex);
        List<Double> mwList = getTroposphericDelayComponentList(measurements, mwIndex);
        List<Double> twList = getTroposphericDelayComponentList(measurements, twIndex);

        return asList(mdList, tdList, mwList, twList);
    }

    public List<List<Double>> getTroposphericDelayComponents(TwoDigitNumber requiredSatelliteNumber) {
        int mdIndex = componentIndexes.getMdIndex();
        int tdIndex = componentIndexes.getTdIndex();
        int mwIndex = componentIndexes.getMwIndex();
        int twIndex = componentIndexes.getTwIndex();
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        List<Double> mdList = getTroposphericDelayComponentList(measurements, mdIndex);
        List<Double> tdList = getTroposphericDelayComponentList(measurements, tdIndex);
        List<Double> mwList = getTroposphericDelayComponentList(measurements, mwIndex);
        List<Double> twList = getTroposphericDelayComponentList(measurements, twIndex);

        return asList(mdList, tdList, mwList, twList);
    }

    public List<Double> getElevationAngles(OneDigitNumber requiredSatelliteNumber) {
        int elevationIndex = componentIndexes.getElevationIndex();
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        return getTroposphericDelayComponentList(measurements, elevationIndex);
    }

    public List<Double> getElevationAngles(TwoDigitNumber requiredSatelliteNumber) {
        int elevationIndex = componentIndexes.getElevationIndex();
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        return getTroposphericDelayComponentList(measurements, elevationIndex);
    }

    private List<Double> getTroposphericDelayComponentList(List<List<Double>> measurements, int componentIndex) {
        return range(0, amountOfObservations)
            .mapToObj(observation -> measurements.get(observation).get(componentIndex))
            .collect(toList());
    }
}
