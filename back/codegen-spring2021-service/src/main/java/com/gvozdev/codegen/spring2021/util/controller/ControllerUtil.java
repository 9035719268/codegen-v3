package com.gvozdev.codegen.spring2021.util.controller;

import com.gvozdev.codegen.spring2021.domain.Satellite;
import com.gvozdev.codegen.spring2021.util.common.CommonUtil;
import com.gvozdev.codegen.spring2021.util.componentindex.ComponentIndexes;
import com.gvozdev.codegen.spring2021.util.filereader.FileReader;
import com.gvozdev.codegen.spring2021.util.filereader.FileReaderUtil;
import com.gvozdev.codegen.spring2021.util.fileservice.FileService;
import com.gvozdev.codegen.spring2021.util.queryparam.lang.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.gvozdev.codegen.spring2021.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.codegen.spring2021.util.queryparam.lang.Lang.JAVA;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;

@Component
public class ControllerUtil {

    @Autowired
    private ComponentIndexes componentIndexes;

    @Autowired
    private FileReaderUtil fileReaderUtil;

    @Autowired
    private CommonUtil commonUtil;

    public String getFilledListingForFile(byte[] inputFileBytes, byte[] outputFileBytes) {
        FileReader fileReader = new FileReader(inputFileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);
        FileService fileService = new FileService(fileReader, commonUtil);
        List<Satellite> satellites = fileService.getSatellites();

        String draftListing = new String(outputFileBytes);

        int satellite1Number = satellites.get(0).getNumber();
        int satellite2Number = satellites.get(1).getNumber();
        int satellite3Number = satellites.get(2).getNumber();

        return format(
            draftListing,
            satellite1Number, satellite2Number, satellite3Number
        );
    }

    public String getFilledListingForManual(byte[] inputFileBytes, byte[] outputFileBytes, Lang lang) {
        FileReader fileReader = new FileReader(inputFileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);
        FileService fileService = new FileService(fileReader, commonUtil);
        List<Satellite> satellites = fileService.getSatellites();

        String draftListing = new String(outputFileBytes);

        int satellite1Number = satellites.get(0).getNumber();
        int satellite2Number = satellites.get(1).getNumber();
        int satellite3Number = satellites.get(2).getNumber();

        List<Double> satellite1Md = satellites.get(0).getMd();
        List<Double> satellite1Td = satellites.get(0).getTd();
        List<Double> satellite1Mw = satellites.get(0).getMw();
        List<Double> satellite1Tw = satellites.get(0).getTw();

        List<Double> satellite2Md = satellites.get(1).getMd();
        List<Double> satellite2Td = satellites.get(1).getTd();
        List<Double> satellite2Mw = satellites.get(1).getMw();
        List<Double> satellite2Tw = satellites.get(1).getTw();

        List<Double> satellite3Md = satellites.get(2).getMd();
        List<Double> satellite3Td = satellites.get(2).getTd();
        List<Double> satellite3Mw = satellites.get(2).getMw();
        List<Double> satellite3Tw = satellites.get(2).getTw();

        List<Double> satellite1Elevation = satellites.get(0).getElevation();
        List<Double> satellite2Elevation = satellites.get(1).getElevation();
        List<Double> satellite3Elevation = satellites.get(2).getElevation();

        String satellite1MdWithLineBreaks = getMeasurementsWithLineBreaks(satellite1Md, lang);
        String satellite1TdWithLineBreaks = getMeasurementsWithLineBreaks(satellite1Td, lang);
        String satellite1MwWithLineBreaks = getMeasurementsWithLineBreaks(satellite1Mw, lang);
        String satellite1TwWithLineBreaks = getMeasurementsWithLineBreaks(satellite1Tw, lang);

        String satellite2MdWithLineBreaks = getMeasurementsWithLineBreaks(satellite2Md, lang);
        String satellite2TdWithLineBreaks = getMeasurementsWithLineBreaks(satellite2Td, lang);
        String satellite2MwWithLineBreaks = getMeasurementsWithLineBreaks(satellite2Mw, lang);
        String satellite2TwWithLineBreaks = getMeasurementsWithLineBreaks(satellite2Tw, lang);

        String satellite3MdWithLineBreaks = getMeasurementsWithLineBreaks(satellite3Md, lang);
        String satellite3TdWithLineBreaks = getMeasurementsWithLineBreaks(satellite3Td, lang);
        String satellite3MwWithLineBreaks = getMeasurementsWithLineBreaks(satellite3Mw, lang);
        String satellite3TwWithLineBreaks = getMeasurementsWithLineBreaks(satellite3Tw, lang);

        String satellite1ElevationWithLineBreaks = getMeasurementsWithLineBreaks(satellite1Elevation, lang);
        String satellite2ElevationWithLineBreaks = getMeasurementsWithLineBreaks(satellite2Elevation, lang);
        String satellite3ElevationWithLineBreaks = getMeasurementsWithLineBreaks(satellite3Elevation, lang);

        return format(
            draftListing,
            satellite1Number, satellite2Number, satellite3Number,
            satellite1MdWithLineBreaks, satellite1TdWithLineBreaks, satellite1MwWithLineBreaks, satellite1TwWithLineBreaks,
            satellite2MdWithLineBreaks, satellite2TdWithLineBreaks, satellite2MwWithLineBreaks, satellite2TwWithLineBreaks,
            satellite3MdWithLineBreaks, satellite3TdWithLineBreaks, satellite3MwWithLineBreaks, satellite3TwWithLineBreaks,
            satellite1ElevationWithLineBreaks, satellite2ElevationWithLineBreaks, satellite3ElevationWithLineBreaks
        );
    }

    public ByteArrayOutputStream getChartsArchiveInBytes(byte[] troposphericDelaysChartInBytes, byte[] elevationAnglesChartInBytes) {
        String troposphericDelaysChartName = "troposphericDelaysChart.dat";
        String elevationAnglesChartName = "elevationAnglesChart.dat";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);

        ZipEntry troposphericDelaysEntry = new ZipEntry(troposphericDelaysChartName);
        troposphericDelaysEntry.setSize(troposphericDelaysChartInBytes.length);

        ZipEntry elevationAnglesEntry = new ZipEntry(elevationAnglesChartName);
        elevationAnglesEntry.setSize(elevationAnglesChartInBytes.length);

        try {
            zipOutputStream.putNextEntry(troposphericDelaysEntry);
            zipOutputStream.write(troposphericDelaysChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.putNextEntry(elevationAnglesEntry);
            zipOutputStream.write(elevationAnglesChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return byteArrayOutputStream;
    }

    private static String getMeasurementsWithLineBreaks(List<Double> measurements, Lang lang) {
        return range(0, AMOUNT_OF_OBSERVATIONS)
            .mapToObj(observation -> handleLineBreaks(observation, lang, measurements))
            .collect(joining());
    }

    private static String handleLineBreaks(int observation, Lang lang, List<Double> measurements) {
        if (isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsJava(observation, lang)) {
            return measurements.get(observation) + "";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsJava(observation, lang)) {
            return measurements.get(observation) + "\n\t\t\t";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsNotJava(observation, lang)) {
            return measurements.get(observation) + "";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsNotJava(observation, lang)) {
            return measurements.get(observation) + "\n\t\t";
        } else {
            return measurements.get(observation) + " ";
        }
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation == (AMOUNT_OF_OBSERVATIONS - 1));
        return divisibleBy10 && notLast && lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation != (AMOUNT_OF_OBSERVATIONS - 1));
        return divisibleBy10 && notLast && lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsNotJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation == (AMOUNT_OF_OBSERVATIONS - 1));
        return divisibleBy10 && notLast && !lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsNotJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation != (AMOUNT_OF_OBSERVATIONS - 1));
        return divisibleBy10 && notLast && !lang.equals(JAVA);
    }
}
