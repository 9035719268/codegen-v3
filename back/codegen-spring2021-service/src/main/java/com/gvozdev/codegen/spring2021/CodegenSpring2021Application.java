package com.gvozdev.codegen.spring2021;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableEurekaClient
public class CodegenSpring2021Application {
    public static void main(String[] args) {
        run(CodegenSpring2021Application.class, args);
    }
}
