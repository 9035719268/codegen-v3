package com.gvozdev.codegen.spring2021.service;

import com.gvozdev.codegen.spring2021.entity.OutputFile;

public interface OutputFileService {
    OutputFile findByParameters(String lang, String file, String oop, String name);
}
