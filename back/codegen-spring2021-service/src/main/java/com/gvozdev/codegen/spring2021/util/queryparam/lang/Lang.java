package com.gvozdev.codegen.spring2021.util.queryparam.lang;

public enum Lang {
    CPP, JAVA, PYTHON;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
