package com.gvozdev.codegen.spring2021.repo;

import com.gvozdev.codegen.spring2021.entity.InputFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InputFileRepository extends JpaRepository<InputFile, Long> {
    InputFile findByName(String name);
}
