package com.gvozdev.codegen.spring2021.restadapter.task;

import com.gvozdev.codegen.spring2021.domain.Satellite;
import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.service.InputFileService;
import com.gvozdev.codegen.spring2021.service.OutputFileService;
import com.gvozdev.codegen.spring2021.util.chart.ElevationAnglesChartDrawer;
import com.gvozdev.codegen.spring2021.util.chart.TemplateChartDrawer;
import com.gvozdev.codegen.spring2021.util.chart.TroposphericDelaysChartDrawer;
import com.gvozdev.codegen.spring2021.util.common.CommonUtil;
import com.gvozdev.codegen.spring2021.util.componentindex.ComponentIndexes;
import com.gvozdev.codegen.spring2021.util.controller.ControllerUtil;
import com.gvozdev.codegen.spring2021.util.filereader.FileReader;
import com.gvozdev.codegen.spring2021.util.filereader.FileReaderUtil;
import com.gvozdev.codegen.spring2021.util.fileservice.FileService;
import com.gvozdev.codegen.spring2021.util.queryparam.file.File;
import com.gvozdev.codegen.spring2021.util.queryparam.inputfilename.InputFileName;
import com.gvozdev.codegen.spring2021.util.queryparam.lang.Lang;
import com.gvozdev.codegen.spring2021.util.queryparam.oop.Oop;
import com.gvozdev.codegen.spring2021.util.queryparam.outputfilename.OutputFileName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.gvozdev.codegen.spring2021.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.codegen.spring2021.util.queryparam.file.File.FILE;
import static com.gvozdev.codegen.spring2021.util.queryparam.file.File.MANUAL;
import static com.gvozdev.codegen.spring2021.util.queryparam.inputfilename.InputFileName.RESOURCES;
import static com.gvozdev.codegen.spring2021.util.queryparam.outputfilename.OutputFileName.CHARTS;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.ContentDisposition.attachment;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@Controller("Spring2021Controller")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/tasks/year/2021/spring/")
public class PageController {

    @Autowired
    private InputFileService inputFileService;

    @Autowired
    private OutputFileService outputFileService;

    @Autowired
    private ComponentIndexes componentIndexes;

    @Autowired
    private ControllerUtil controllerUtil;

    @Autowired
    private FileReaderUtil fileReaderUtil;

    @Autowired
    private CommonUtil commonUtil;

    @GetMapping("/exercise-info")
    public ResponseEntity<List<Satellite>> getSatellites(@RequestParam("inputfilename") InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        return new ResponseEntity<>(satellites, OK);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") Lang lang,
        @PathVariable("file") File file,
        @PathVariable("oop") Oop oop,
        @PathVariable("inputFileName") InputFileName inputFileName,
        @PathVariable("outputFileName") OutputFileName outputFileName
    ) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());
        OutputFile outputFile = outputFileService.findByParameters(lang.toString(), file.toString(), oop.toString(), outputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();
        byte[] outputFileBytes = outputFile.getFileBytes();

        String listing;

        if (file.equals(FILE)) {
            listing = controllerUtil.getFilledListingForFile(inputFileBytes, outputFileBytes);
        } else if (file.equals(MANUAL)) {
            listing = controllerUtil.getFilledListingForManual(inputFileBytes, outputFileBytes, lang);
        } else {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        byte[] listingBytes = listing.getBytes(UTF_8);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_PDF);
        responseHeaders.setContentDisposition(attachment().filename(outputFileName.getFileName()).build());

        return new ResponseEntity<>(listingBytes, responseHeaders, OK);
    }

    @GetMapping("/download/charts/{inputFileName}")
    public ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        Satellite satellite1 = satellites.get(0);
        Satellite satellite2 = satellites.get(1);
        Satellite satellite3 = satellites.get(2);

        TemplateChartDrawer troposphericDelaysChartDrawer = new TroposphericDelaysChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] troposphericDelaysChartInBytes = troposphericDelaysChartDrawer.getChartInBytes();

        TemplateChartDrawer elevationAnglesDrawer = new ElevationAnglesChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] elevationAnglesChartInBytes = elevationAnglesDrawer.getChartInBytes();

        ByteArrayOutputStream byteArrayOutputStream = controllerUtil.getChartsArchiveInBytes(troposphericDelaysChartInBytes, elevationAnglesChartInBytes);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(CHARTS.getFileName()).build());

        return new ResponseEntity<>(byteArrayOutputStream.toByteArray(), responseHeaders, OK);
    }

    @GetMapping("/download/resources")
    public ResponseEntity<byte[]> downloadResources() {
        String inputFileName = RESOURCES.getFileName();

        InputFile inputFile = inputFileService.findByName(inputFileName);

        byte[] fileBytes = inputFile.getFileBytes();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(inputFileName).build());

        return new ResponseEntity<>(fileBytes, responseHeaders, OK);
    }
}
