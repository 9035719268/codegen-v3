package com.gvozdev.codegen.spring2021.controllertests.tasks;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static com.gvozdev.codegen.spring2021.util.path.Paths.*;
import static com.gvozdev.codegen.spring2021.util.queryparam.file.File.FILE;
import static com.gvozdev.codegen.spring2021.util.queryparam.file.File.MANUAL;
import static com.gvozdev.codegen.spring2021.util.queryparam.inputfilename.InputFileName.ARTI_6;
import static com.gvozdev.codegen.spring2021.util.queryparam.lang.Lang.*;
import static com.gvozdev.codegen.spring2021.util.queryparam.oop.Oop.NOOOP;
import static com.gvozdev.codegen.spring2021.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.codegen.spring2021.util.queryparam.outputfilename.OutputFileName.*;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.countMatches;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class PageControllerTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void shouldCheckExerciseInfo() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            EXERCISE_INFO_PATH,
            String.class,
            ARTI_6.getFileName()
        );
        String responseBody = response.getBody();

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(responseBody).length();
        int satelliteNumbersAmount = countMatches(responseBody, "number");
        int elevationListsAmount = countMatches(responseBody, "elevation");
        int mdListsAmount = countMatches(responseBody, "md");
        int tdListsAmount = countMatches(responseBody, "td");
        int mwListsAmount = countMatches(responseBody, "mw");
        int twListsAmount = countMatches(responseBody, "tw");

        assertEquals(200, responseStatusCode);
        assertEquals(49475, responseBodyLength);
        assertEquals(3, satelliteNumbersAmount);
        assertEquals(3, elevationListsAmount);
        assertEquals(3, mdListsAmount);
        assertEquals(3, tdListsAmount);
        assertEquals(3, mwListsAmount);
        assertEquals(3, twListsAmount);
    }

    @Test
    void shouldCheckExerciseInfoWrongInputFileName() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            EXERCISE_INFO_PATH,
            String.class,
            "wrong_input_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingCppFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(14236, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingCppFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(7883, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingCppManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(60009, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingCppManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(54950, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", MANUAL, NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, "wrong_file_value", NOOOP, ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppFile() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, "wrong_oop_value", ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppManual() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, WITHOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, NOOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, WITHOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameCppManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, NOOOP, "wrong_input_file_name", CPP_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(14574, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(5807, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(60931, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(57906, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaFileWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaFileWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaManualWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaManualWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, "wrong_oop_value", ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, "wrong_oop_value", ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaFileWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, "wrong_input_file_name", JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaFileWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, "wrong_input_file_name", JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaManualWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, "wrong_input_file_name", JAVA_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNameJavaManualWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, "wrong_input_file_name", JAVA_GRAPH_DRAWER.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingPythonFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(16096, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingPythonFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(10267, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingPythonManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(62865, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingPythonManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(57316, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", MANUAL, WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", MANUAL, NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, "wrong_file_value", WITHOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, "wrong_file_value", NOOOP, ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonFile() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, "wrong_oop_value", ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonManual() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, "wrong_oop_value", ARTI_6.getFileName(), PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, "wrong_input_file_name", PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, NOOOP, "wrong_input_file_name", PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, WITHOOP, "wrong_input_file_name", PYTHON_MAIN.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongInputFileNamePythonManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, NOOOP, "wrong_input_file_name", PYTHON_MAIN
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, WITHOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, NOOOP, ARTI_6.getFileName(), "wrong_output_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadCharts() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_CHARTS_PATH,
            String.class,
            ARTI_6.getFileName()
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(185774, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadChartsWrongInputFileName() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            DOWNLOAD_CHARTS_PATH,
            String.class,
            "wrong_input_file_name"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadResources() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(DOWNLOAD_RESOURCES_PATH, String.class);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(19683669, responseBodyLength);
    }
}