package com.gvozdev.codegen.spring2021.filestests.inputfiles;

import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.service.InputFileService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.codegen.spring2021.util.queryparam.inputfilename.InputFileName.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class FilesLengthTests {

    @Autowired
    private InputFileService inputFileService;

    @Test
    void shouldCheckArti6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(ARTI_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2183057, bytesInFile);
    }

    @Test
    void shouldCheckBshm10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(BSHM_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2727464, bytesInFile);
    }

    @Test
    void shouldCheckGele6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(GELE_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2086664, bytesInFile);
    }

    @Test
    void shouldCheckHueg10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(HUEG_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2746623, bytesInFile);
    }

    @Test
    void shouldCheckIrkm6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(IRKM_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2027486, bytesInFile);
    }

    @Test
    void shouldCheckKslv6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(KSLV_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2052737, bytesInFile);
    }

    @Test
    void shouldCheckLeij10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(LEIJ_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2819423, bytesInFile);
    }

    @Test
    void shouldCheckMaga6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(MAGA_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2179093, bytesInFile);
    }

    @Test
    void shouldCheckMenp6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(MENP_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2198799, bytesInFile);
    }

    @Test
    void shouldCheckNovs6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(NOVS_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2180633, bytesInFile);
    }

    @Test
    void shouldCheckNoyb6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(NOYB_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2268863, bytesInFile);
    }

    @Test
    void shouldCheckOnsa10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(ONSA_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2958038, bytesInFile);
    }

    @Test
    void shouldCheckSpt10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(SPT_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(3253827, bytesInFile);
    }

    @Test
    void shouldCheckSvet6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(SVET_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2212598, bytesInFile);
    }

    @Test
    void shouldCheckSvtl10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(SVTL_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(3026325, bytesInFile);
    }

    @Test
    void shouldCheckTit10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(TIT_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2807553, bytesInFile);
    }

    @Test
    void shouldCheckVis10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(VIS_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2869674, bytesInFile);
    }

    @Test
    void shouldCheckVlad6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(VLAD_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(1966226, bytesInFile);
    }

    @Test
    void shouldCheckWarn10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(WARN_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2868401, bytesInFile);
    }

    @Test
    void shouldCheckYkts6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(YKTS_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2189717, bytesInFile);
    }

    @Test
    void shouldCheckYusa6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(YUSA_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(1965381, bytesInFile);
    }

    @Test
    void shouldCheckZeck10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(ZECK_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2802665, bytesInFile);
    }
}
