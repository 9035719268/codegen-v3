package com.gvozdev.codegen.spring2021.unittests;

import com.gvozdev.codegen.spring2021.entity.InputFile;
import com.gvozdev.codegen.spring2021.service.InputFileService;
import com.gvozdev.codegen.spring2021.util.filereader.FileReader;
import com.gvozdev.codegen.spring2021.util.filereader.FileReaderUtil;
import com.gvozdev.codegen.spring2021.util.componentindex.ComponentIndexes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.gvozdev.codegen.spring2021.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.codegen.spring2021.util.queryparam.inputfilename.InputFileName.ARTI_6;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SatelliteNumbersTests {

    @Autowired
    private InputFileService inputFileService;

    @Autowired
    private ComponentIndexes componentIndexes;

    @Autowired
    private FileReaderUtil fileReaderUtil;

    @Test
    void shouldCheckSatelliteNumbers() {
        InputFile inputFile = inputFileService.findByName(ARTI_6.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes, AMOUNT_OF_OBSERVATIONS, componentIndexes, fileReaderUtil);

        List<Integer> expectedSatelliteNumbers = asList(3, 6, 9, 16, 22, 23, 26, 31);
        List<Integer> actualSatelliteNumbers = fileReader.getSatelliteNumbers();

        assertEquals(expectedSatelliteNumbers, actualSatelliteNumbers);
    }
}
