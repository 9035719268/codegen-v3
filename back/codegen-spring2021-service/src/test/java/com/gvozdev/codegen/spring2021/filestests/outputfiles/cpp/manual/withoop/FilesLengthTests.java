package com.gvozdev.codegen.spring2021.filestests.outputfiles.cpp.manual.withoop;

import com.gvozdev.codegen.spring2021.entity.OutputFile;
import com.gvozdev.codegen.spring2021.service.OutputFileService;
import com.gvozdev.codegen.spring2021.util.queryparam.file.File;
import com.gvozdev.codegen.spring2021.util.queryparam.lang.Lang;
import com.gvozdev.codegen.spring2021.util.queryparam.oop.Oop;
import com.gvozdev.codegen.spring2021.util.queryparam.outputfilename.OutputFileName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.codegen.spring2021.util.queryparam.file.File.MANUAL;
import static com.gvozdev.codegen.spring2021.util.queryparam.lang.Lang.CPP;
import static com.gvozdev.codegen.spring2021.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.codegen.spring2021.util.queryparam.outputfilename.OutputFileName.CPP_MAIN;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class FilesLengthTests {
    private static final Lang LANG = CPP;
    private static final File FILE = MANUAL;
    private static final Oop OOP = WITHOOP;
    private static final OutputFileName OUTPUT_FILE_NAME = CPP_MAIN;

    @Autowired
    private OutputFileService outputFileService;

    @Test
    void shouldCheckMainLengthInBytes() {
        OutputFile outputFile = outputFileService.findByParameters(LANG.toString(), FILE.toString(), OOP.toString(), OUTPUT_FILE_NAME.getFileName());

        byte[] fileBytes = outputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(9695, bytesInFile);
    }
}

