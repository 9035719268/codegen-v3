package com.gvozdev.codegen.spring2021.controllertests.swagger;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static com.gvozdev.codegen.spring2021.util.path.Paths.SWAGGER_UI_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class PageControllerTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void shouldCheckSwaggerUi() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(SWAGGER_UI_PATH, String.class);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(302, responseStatusCode);
    }
}