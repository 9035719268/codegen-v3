package com.gvozdev.codegen.spring2021.utilstests;

import com.gvozdev.codegen.spring2021.util.common.CommonUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CommonUtilTests {

    @Autowired
    private CommonUtil commonUtil;

    @Test
    void shouldCheckToCharMethod() {
        char result = commonUtil.toChar(58);

        assertEquals('j', result);
    }

    @Test
    void shouldCheckIsDoubleDigitMethod() {
        boolean is4DoubleDigit = commonUtil.isDoubleDigit(4);
        boolean is56DoubleDigit = commonUtil.isDoubleDigit(56);
        boolean is789DoubleDigit = commonUtil.isDoubleDigit(789);

        assertFalse(is4DoubleDigit);
        assertTrue(is56DoubleDigit);
        assertFalse(is789DoubleDigit);
    }

    @Test
    void shouldCheckGetFirstDigitMethod() {
        int firstDigitOf1 = commonUtil.getFirstDigit(1);
        int firstDigitOf23 = commonUtil.getFirstDigit(23);
        int firstDigitOf456 = commonUtil.getFirstDigit(456);

        assertEquals(1, firstDigitOf1);
        assertEquals(2, firstDigitOf23);
        assertEquals(4, firstDigitOf456);
    }

    @Test
    void shouldCheckGetSecondDigitMethod() {
        int secondDigitOf34 = commonUtil.getSecondDigit(34);
        int secondDigitOf567 = commonUtil.getSecondDigit(567);

        assertEquals(4, secondDigitOf34);
        assertEquals(6, secondDigitOf567);
        assertThrows(StringIndexOutOfBoundsException.class, () -> commonUtil.getSecondDigit(2));
    }
}
