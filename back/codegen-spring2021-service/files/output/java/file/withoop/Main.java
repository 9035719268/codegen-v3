import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Main {
    public static void main(String[] args) throws IOException {
        int amountOfObservations = 360;

        int satellite1Number = %s;
        int satellite2Number = %s;
        int satellite3Number = %s;

        String fileName = "./src/file.dat";
        FileReader fileReader = new FileReader(fileName);

        SatelliteFactory satelliteFactory = new SatelliteFactory(fileReader, amountOfObservations);

        Satellite satellite1 = satelliteFactory.createSatellite(satellite1Number);
        Satellite satellite2 = satelliteFactory.createSatellite(satellite2Number);
        Satellite satellite3 = satelliteFactory.createSatellite(satellite3Number);

        ConsoleOutput consoleOutput = new ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations);
        consoleOutput.printInfo();

        GraphDrawer.draw();
    }
}

class ConsoleOutput {
    private Satellite satellite1;
    private Satellite satellite2;
    private Satellite satellite3;
    private int amountOfObservations;

    public ConsoleOutput(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
    }

    public void printInfo() {
        int satellite1Number = satellite1.getNumber();
        int satellite2Number = satellite2.getNumber();
        int satellite3Number = satellite3.getNumber();
        List<Double> satellite1TroposphericDelays = satellite1.getTroposphericDelays();
        List<Double> satellite2TroposphericDelays = satellite2.getTroposphericDelays();
        List<Double> satellite3TroposphericDelays = satellite3.getTroposphericDelays();
        List<Double> satellite1ElevationAngles = satellite1.getElevationAngles();
        List<Double> satellite2ElevationAngles = satellite2.getElevationAngles();
        List<Double> satellite3ElevationAngles = satellite3.getElevationAngles();
        System.out.println("Спутник #" + satellite1Number +
                           "\t\t\t\t\t\t\tСпутник #" + satellite2Number +
                           "\t\t\t\t\t\t\tСпутник #" + satellite3Number);
        DecimalFormat df = new DecimalFormat("#.##########");
        for (int observation = 0; observation < amountOfObservations; observation++) {
            double satellite1TroposphericDelay = satellite1TroposphericDelays.get(observation);
            double satellite1ElevationAngle = satellite1ElevationAngles.get(observation);
            double satellite2TroposphericDelay = satellite2TroposphericDelays.get(observation);
            double satellite2ElevationAngle = satellite2ElevationAngles.get(observation);
            double satellite3TroposphericDelay = satellite3TroposphericDelays.get(observation);
            double satellite3ElevationAngle = satellite3ElevationAngles.get(observation);
            System.out.println(df.format(satellite1TroposphericDelay) + "\t" + df.format(satellite1ElevationAngle) + "\t\t" +
                               df.format(satellite2TroposphericDelay) + "\t" + df.format(satellite2ElevationAngle) + "\t\t" +
                               df.format(satellite3TroposphericDelay) + "\t" + df.format(satellite3ElevationAngle));
        }
    }
}

@SuppressWarnings("UnnecessaryLocalVariable")
class SatelliteFactory {
    private FileReader fileReader;
    private int amountOfObservations;

    public SatelliteFactory(FileReader fileReader, int amountOfObservations) {
        this.fileReader = fileReader;
        this.amountOfObservations = amountOfObservations;
    }

    public Satellite createSatellite(int requiredSatelliteNumber) {
        List<Double> mdArray = new ArrayList<>(amountOfObservations);
        List<Double> tdArray = new ArrayList<>(amountOfObservations);
        List<Double> mwArray = new ArrayList<>(amountOfObservations);
        List<Double> twArray = new ArrayList<>(amountOfObservations);
        List<Double> elevationArray = new ArrayList<>(amountOfObservations);

        List<List<Double>> measurements;
        if (requiredSatelliteNumber < 10) {
            char satelliteNumber = Integer.toString(requiredSatelliteNumber).charAt(0);
            measurements = fileReader.getMeasurements(satelliteNumber, 1);
        } else {
            char satelliteNumber1 = Integer.toString(requiredSatelliteNumber).charAt(0);
            char satelliteNumber2 = Integer.toString(requiredSatelliteNumber).charAt(1);
            measurements = fileReader.getMeasurements(satelliteNumber1, satelliteNumber2, 2);
        }

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double md = measurements.get(observation).get(5);
            mdArray.add(md);
            double td = measurements.get(observation).get(6);
            tdArray.add(td);
            double mw = measurements.get(observation).get(7);
            mwArray.add(mw);
            double tw = measurements.get(observation).get(8);
            twArray.add(tw);
            double elevation = measurements.get(observation).get(14);
            elevationArray.add(elevation);
        }

        DelayComponent dryComponentFunction = new DryComponentFunction(mdArray);
        DelayComponent verticalDryComponent = new VerticalDryComponent(tdArray);
        DelayComponent wetComponentFunction = new WetComponentFunction(mwArray);
        DelayComponent verticalWetComponent = new VerticalWetComponent(twArray);
        TroposphericDelays delay = new TroposphericDelays(dryComponentFunction, verticalDryComponent,
            wetComponentFunction, verticalWetComponent,
            amountOfObservations);
        ElevationAngles angles = new ElevationAngles(elevationArray);

        Satellite satellite = new Satellite(requiredSatelliteNumber, delay, angles);

        return satellite;
    }
}

@SuppressWarnings("UnnecessaryLocalVariable")
class Satellite {
    private int number;
    private TroposphericDelays troposphericDelays;
    private ElevationAngles elevationAngles;

    public Satellite(int number, TroposphericDelays troposphericDelays, ElevationAngles elevationAngles) {
        this.number = number;
        this.troposphericDelays = troposphericDelays;
        this.elevationAngles = elevationAngles;
    }

    public int getNumber() {
        return number;
    }

    public List<Double> getTroposphericDelays() {
        List<Double> delays = troposphericDelays.getDelays();
        return delays;
    }

    public List<Double> getElevationAngles() {
        List<Double> angles = elevationAngles.getAnglesInHalfCircles();
        return angles;
    }
}

class TroposphericDelays {
    private DelayComponent dryComponentFunction;
    private DelayComponent verticalDryComponent;
    private DelayComponent wetComponentFunction;
    private DelayComponent verticalWetComponent;
    private int amountOfObservations;

    public TroposphericDelays(DelayComponent dryComponentFunction, DelayComponent verticalDryComponent,
                              DelayComponent wetComponentFunction, DelayComponent verticalWetComponent,
                              int amountOfObservations) {
        this.dryComponentFunction = dryComponentFunction;
        this.verticalDryComponent = verticalDryComponent;
        this.wetComponentFunction = wetComponentFunction;
        this.verticalWetComponent = verticalWetComponent;
        this.amountOfObservations = amountOfObservations;
    }

    public List<Double> getDelays() {
        List<Double> dryComponentFunctionValues = dryComponentFunction.getValues();
        List<Double> verticalDryComponentValues = verticalDryComponent.getValues();
        List<Double> wetComponentFunctionValues = wetComponentFunction.getValues();
        List<Double> verticalWetComponentValues = verticalWetComponent.getValues();
		List<Double> delays = new ArrayList<>(amountOfObservations);

        for (int observation = 0; observation < amountOfObservations; observation++) {
            double md = dryComponentFunctionValues.get(observation);
            double td = verticalDryComponentValues.get(observation);
            double mw = wetComponentFunctionValues.get(observation);
            double tw = verticalWetComponentValues.get(observation);
            double delay = md * td + mw * tw;
            delays.add(delay);
        }
        return delays;
    }
}

interface DelayComponent {
    List<Double> getValues();
}

class DryComponentFunction implements DelayComponent {
    private List<Double> values;

    public DryComponentFunction(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class VerticalDryComponent implements DelayComponent {
    private List<Double> values;

    public VerticalDryComponent(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class WetComponentFunction implements DelayComponent {
    private List<Double> values;

    public WetComponentFunction(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class VerticalWetComponent implements DelayComponent {
    private List<Double> values;

    public VerticalWetComponent(List<Double> values) {
        this.values = values;
    }

    @Override
    public List<Double> getValues() {
        return values;
    }
}

class ElevationAngles {
    private List<Double> degrees;

    public ElevationAngles(List<Double> degrees) {
        this.degrees = degrees;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    public List<Double> getAnglesInHalfCircles() {
        double halfCircle = 180;
        List<Double> halfCircles = degrees.stream()
                                               .map(angleInDegrees -> angleInDegrees / halfCircle)
                                               .collect(toList());
        return halfCircles;
    }
}

class FileReader {
    private byte[] allBytes;
    private List<List<List<Byte>>> allLines;

    public FileReader(String fileName) throws IOException {
        try (FileInputStream fin = new FileInputStream(fileName)) {
            allBytes = new byte[fin.available()];
            int offset = 0;
            fin.read(allBytes, offset, allBytes.length);
        }
        allLines = analyzeSyntaxAndReturnLinesList();
    }

    public List<List<Double>> getMeasurements(char requiredSatelliteNumber1, char requiredSatelliteNumber2,
                                              int requiredSatelliteNumberSize) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;
        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber1 = (char) (byte) line.get(0).get(0);
                char satelliteNumber2 = (char) (byte) line.get(0).get(1);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber1 == requiredSatelliteNumber1) &&
                    (satelliteNumber2 == requiredSatelliteNumber2) &&
                    (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) {
            }
        }
        return measurements;
    }

    public List<List<Double>> getMeasurements(char requiredSatelliteNumber, int requiredSatelliteNumberSize) {
        List<List<Double>> measurements = new ArrayList<>();
        int numbersInLine = 21;

        for (List<List<Byte>> line : allLines) {
            try {
                char satelliteNumber = (char) (byte) line.get(0).get(0);
                int satelliteNumberSize = line.get(0).size();

                if ((satelliteNumber == requiredSatelliteNumber) && (satelliteNumberSize == requiredSatelliteNumberSize)) {
                    List<Double> lineOfNumbers = new ArrayList<>();
                    for (int number = 1; number <= numbersInLine; number++) {
                        double numeric = getNumeric(line, number);
                        lineOfNumbers.add(numeric);
                    }
                    measurements.add(lineOfNumbers);
                }
            } catch (Exception ignored) {
            }
        }
        return measurements;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    private double getNumeric(List<List<Byte>> line, int number) {
        StringBuilder numberBuilder = new StringBuilder();
        int numberLength = line.get(number).size();

        for (int digit = 0; digit < numberLength; digit++) {
            char symbol = (char) line.get(number).get(digit).byteValue();
            numberBuilder.append(symbol);
        }

        double numeric = Double.parseDouble(numberBuilder.toString());
        return numeric;
    }

    private List<List<List<Byte>>> analyzeSyntaxAndReturnLinesList() {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte tab = 9;
        byte newLine = 10;
        byte carriageReturn = 13;
        byte space = 32;

        for (byte symbol : allBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }
}