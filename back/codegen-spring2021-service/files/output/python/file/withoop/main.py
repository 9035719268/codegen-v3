from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
import numpy as np


class FileReader:
    def __init__(self, fileName: str) -> None:
        with open(fileName, "rb") as file:
            self.__allBytes: bytes = file.read()
        self.__allLines = self.__analyzeSyntaxAndReturnLinesList()

    def getMeasurements(self, requiredSatelliteNumber: int, requiredSatelliteNumberSize: int,
                        requiredSatelliteNumber2: int = None) -> list:
        measurements: list = []
        numbersInLine: int = 21

        for line in self.__allLines:
            try:
                satelliteNumber: int = line[0][0]
                satelliteNumberSize: int = len(line[0])
                if requiredSatelliteNumber2 is not None:
                    satelliteNumber2: int = line[0][1]
                    if (satelliteNumber is requiredSatelliteNumber and
                            satelliteNumber2 is requiredSatelliteNumber2 and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        lineOfNumbers: list = []
                        for number in range(1, numbersInLine + 1):
                            numeric: float = self.__getNumeric(line, number)
                            lineOfNumbers.append(numeric)
                        measurements.append(lineOfNumbers)
                else:
                    if (satelliteNumber is requiredSatelliteNumber and
                            satelliteNumberSize is requiredSatelliteNumberSize):
                        lineOfNumbers: list = []
                        for number in range(1, numbersInLine + 1):
                            numeric: float = self.__getNumeric(line, number)
                            lineOfNumbers.append(numeric)
                        measurements.append(lineOfNumbers)
            except Exception:
                pass
        return measurements

    @classmethod
    def __getNumeric(cls, line: list, number: int) -> float:
        numberBuilder: str = ""
        numberLength: int = len(line[number])
        for digit in range(numberLength):
            symbol: chr = chr(line[number][digit])
            numberBuilder += symbol
        numeric: float = float(numberBuilder)
        return numeric

    def __analyzeSyntaxAndReturnLinesList(self) -> list:
        allLines: list = []
        words: list = []
        symbols: list = []
        isWord: bool = False

        tab: int = 9
        newLine: int = 10
        carriageReturn: int = 13
        space: int = 32

        for symbol in self.__allBytes:
            if symbol is newLine:
                words.append(symbols)
                symbols = []
                allLines.append(words)
                words = []
                isWord = False
            elif isWord is True and symbol is tab:
                words.append(symbols)
                symbols = []
                isWord = False
            elif symbol is not space and symbol is not tab and symbol is not carriageReturn:
                isWord = True
                symbols.append(symbol)
        return allLines


class DelayComponent(ABC):
    @abstractmethod
    def getValues(self) -> list:
        pass


class DryComponentFunction(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class VerticalDryComponent(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class WetComponentFunction(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class VerticalWetComponent(DelayComponent):
    def __init__(self, values: list) -> None:
        self.__values: list = values

    def getValues(self) -> list:
        return self.__values


class TroposphericDelays:
    def __init__(self, dryComponentFunction: DelayComponent, verticalDryComponent: DelayComponent,
                 wetComponentFunction: DelayComponent, verticalWetComponent: DelayComponent,
                 amountOfObservations: int) -> None:
        self.__dryComponentFunction: DelayComponent = dryComponentFunction
        self.__verticalDryComponent: DelayComponent = verticalDryComponent
        self.__wetComponentFunction: DelayComponent = wetComponentFunction
        self.__verticalWetComponent: DelayComponent = verticalWetComponent
        self.__amountOfObservations: int = amountOfObservations

    def getDelays(self) -> list:
        dryComponentFunctionValues: list = self.__dryComponentFunction.getValues()
        verticalDryComponentValues: list = self.__verticalDryComponent.getValues()
        wetComponentFunctionValues: list = self.__wetComponentFunction.getValues()
        verticalWetComponentValues: list = self.__verticalWetComponent.getValues()
        delays: list = []
        for observation in range(self.__amountOfObservations):
            md: float = dryComponentFunctionValues[observation]
            td: float = verticalDryComponentValues[observation]
            mw: float = wetComponentFunctionValues[observation]
            tw: float = verticalWetComponentValues[observation]
            delay: float = md * td + mw * tw
            delays.append(delay)
        return delays


class ElevationAngles:
    def __init__(self, degrees: list) -> None:
        self.__degrees: list = degrees

    def getAnglesInHalfCircles(self) -> list:
        halfCircle: float = 180
        halfCircles: list = []
        for angleInDegrees in self.__degrees:
            angleInHalfCircles: float = angleInDegrees / halfCircle
            halfCircles.append(angleInHalfCircles)
        return halfCircles


class Satellite:
    def __init__(self, number: int, troposphericDelays: TroposphericDelays, elevationAngles: ElevationAngles) -> None:
        self.__number = number
        self.__troposphericDelays: TroposphericDelays = troposphericDelays
        self.__elevationAngles: ElevationAngles = elevationAngles

    @property
    def number(self) -> int:
        return self.__number

    @property
    def troposphericDelays(self) -> list:
        delays: list = self.__troposphericDelays.getDelays()
        return delays

    @property
    def elevationAngles(self) -> list:
        angles: list = self.__elevationAngles.getAnglesInHalfCircles()
        return angles


class SatelliteFactory:
    def __init__(self, fileReader: FileReader, amountOfObservations: int) -> None:
        self.__fileReader: FileReader = fileReader
        self.__amountOfObservations: int = amountOfObservations

    def createSatellite(self, requiredSatelliteNumber: int) -> Satellite:
        mdArray: list = []
        tdArray: list = []
        mwArray: list = []
        twArray: list = []
        elevationArray = []

        if requiredSatelliteNumber < 10:
            satelliteNumberInAscii: int = self.__toAsciiCode(requiredSatelliteNumber)
            measurements: list = self.__fileReader.getMeasurements(satelliteNumberInAscii, 1)
        else:
            satelliteNumberFirstDigit = int(str(requiredSatelliteNumber)[0])
            satelliteNumberSecondDigit = int(str(requiredSatelliteNumber)[1])

            satelliteNumberFirstDigitInAscii: int = self.__toAsciiCode(satelliteNumberFirstDigit)
            satelliteNumberSecondDigitInAscii: int = self.__toAsciiCode(satelliteNumberSecondDigit)

            measurements: list = self.__fileReader.getMeasurements(satelliteNumberFirstDigitInAscii, 2,
                                                                   satelliteNumberSecondDigitInAscii)

        for observation in range(self.__amountOfObservations):
            md: float = measurements[observation][5]
            mdArray.append(md)
            td: float = measurements[observation][6]
            tdArray.append(td)
            mw: float = measurements[observation][7]
            mwArray.append(mw)
            tw: float = measurements[observation][8]
            twArray.append(tw)
            elevation: float = measurements[observation][14]
            elevationArray.append(elevation)

        dryComponentFunction: DelayComponent = DryComponentFunction(mdArray)
        verticalDryComponent: DelayComponent = VerticalDryComponent(tdArray)
        wetComponentFunction: DelayComponent = WetComponentFunction(mwArray)
        verticalWetComponent: DelayComponent = VerticalWetComponent(twArray)
        delay: TroposphericDelays = TroposphericDelays(dryComponentFunction, verticalDryComponent,
                                                       wetComponentFunction, verticalWetComponent,
                                                       self.__amountOfObservations)
        angles: ElevationAngles = ElevationAngles(elevationArray)
        satellite: Satellite = Satellite(requiredSatelliteNumber, delay, angles)
        return satellite

    @classmethod
    def __toAsciiCode(cls, number: int) -> int:
        return number + 48


class ConsoleOutput:
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def printInfo(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        satellite1TroposphericDelays: list = self.__satellite1.troposphericDelays
        satellite2TroposphericDelays: list = self.__satellite2.troposphericDelays
        satellite3TroposphericDelays: list = self.__satellite3.troposphericDelays
        satellite1ElevationAngles: list = self.__satellite1.elevationAngles
        satellite2ElevationAngles: list = self.__satellite2.elevationAngles
        satellite3ElevationAngles: list = self.__satellite3.elevationAngles
        print("Спутник #" + str(satellite1Number) + "\t\t\t\t\t\t\t" +
              "Спутник #" + str(satellite2Number) + "\t\t\t\t\t\t\t" +
              "Спутник #" + str(satellite3Number))
        for observation in range(self.__amountOfObservations):
            satellite1TroposphericDelay: float = satellite1TroposphericDelays[observation]
            satellite1ElevationAngle: float = satellite1ElevationAngles[observation]
            satellite2TroposphericDelay: float = satellite2TroposphericDelays[observation]
            satellite2ElevationAngle: float = satellite2ElevationAngles[observation]
            satellite3TroposphericDelay: float = satellite3TroposphericDelays[observation]
            satellite3ElevationAngle: float = satellite3ElevationAngles[observation]
            print(str(round(satellite1TroposphericDelay, 10)) + "\t" + str(round(satellite1ElevationAngle, 10)) +
                  "\t\t" +
                  str(round(satellite2TroposphericDelay, 10)) + "\t" + str(round(satellite2ElevationAngle, 10)) +
                  "\t\t" +
                  str(round(satellite3TroposphericDelay, 10)) + "\t" + str(round(satellite3ElevationAngle, 10)))


class DrawerTemplate(ABC):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        self.__satellite1: Satellite = satellite1
        self.__satellite2: Satellite = satellite2
        self.__satellite3: Satellite = satellite3
        self.__amountOfObservations: int = amountOfObservations

    def draw(self) -> None:
        satellite1Number: int = self.__satellite1.number
        satellite2Number: int = self.__satellite2.number
        satellite3Number: int = self.__satellite3.number
        measurements1: list = self._getSatellite1Measurements()
        measurements2: list = self._getSatellite2Measurements()
        measurements3: list = self._getSatellite3Measurements()
        yLabel: str = self._getYLabel()
        observations = np.arange(0, self.__amountOfObservations)
        plt.plot(observations, measurements1, 'o-', label="Спутник #" + str(satellite1Number))
        plt.plot(observations, measurements2, 'o-', label="Спутник #" + str(satellite2Number))
        plt.plot(observations, measurements3, 'o-', label="Спутник #" + str(satellite3Number))
        plt.xlabel("Время")
        plt.ylabel(yLabel)
        plt.legend()
        plt.grid(linestyle='-', linewidth=0.5)
        plt.show()

    @abstractmethod
    def _getYLabel(self) -> str:
        pass

    @abstractmethod
    def _getSatellite1Measurements(self) -> list:
        pass

    @abstractmethod
    def _getSatellite2Measurements(self) -> list:
        pass

    @abstractmethod
    def _getSatellite3Measurements(self) -> list:
        pass


class TroposphericDelayDrawer(DrawerTemplate):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(satellite1, satellite2, satellite3, amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def _getYLabel(self) -> str:
        return "Тропосферная задержка сигнала, метры"

    def _getSatellite1Measurements(self) -> list:
        return self.__satellite1.troposphericDelays

    def _getSatellite2Measurements(self) -> list:
        return self.__satellite2.troposphericDelays

    def _getSatellite3Measurements(self) -> list:
        return self.__satellite3.troposphericDelays


class ElevationAnglesDrawer(DrawerTemplate):
    def __init__(self, satellite1: Satellite, satellite2: Satellite, satellite3: Satellite,
                 amountOfObservations: int) -> None:
        super().__init__(satellite1, satellite2, satellite3, amountOfObservations)
        self.__satellite1 = satellite1
        self.__satellite2 = satellite2
        self.__satellite3 = satellite3

    def _getYLabel(self) -> str:
        return "Угол возвышения спутника, полуциклы"

    def _getSatellite1Measurements(self) -> list:
        return self.__satellite1.elevationAngles

    def _getSatellite2Measurements(self) -> list:
        return self.__satellite2.elevationAngles

    def _getSatellite3Measurements(self) -> list:
        return self.__satellite3.elevationAngles


def main():
    amountOfObservations: int = 360

    satellite1Number: int = %s
    satellite2Number: int = %s
    satellite3Number: int = %s

    fileName: str = "../resources/file.dat"
    fileReader: FileReader = FileReader(fileName)

    satelliteFactory: SatelliteFactory = SatelliteFactory(fileReader, amountOfObservations)

    satellite1: Satellite = satelliteFactory.createSatellite(satellite1Number)
    satellite2: Satellite = satelliteFactory.createSatellite(satellite2Number)
    satellite3: Satellite = satelliteFactory.createSatellite(satellite3Number)

    consoleOutput: ConsoleOutput = ConsoleOutput(satellite1, satellite2, satellite3, amountOfObservations)
    consoleOutput.printInfo()

    troposphericDelayDrawer: DrawerTemplate = TroposphericDelayDrawer(satellite1, satellite2, satellite3,
                                                                      amountOfObservations)
    troposphericDelayDrawer.draw()

    elevationAnglesDrawer: DrawerTemplate = ElevationAnglesDrawer(satellite1, satellite2, satellite3,
                                                                  amountOfObservations)
    elevationAnglesDrawer.draw()


if __name__ == '__main__':
    main()
