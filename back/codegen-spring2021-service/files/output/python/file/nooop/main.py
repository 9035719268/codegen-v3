import matplotlib.pyplot as plt
import numpy as np


def readBytes(fileName: str) -> bytes:
    with open(fileName, "rb") as file:
        allBytes: bytes = file.read()
    return allBytes


def analyzeSyntaxAndReturnLinesList(allBytes: bytes) -> list:
    allLines: list = []
    words: list = []
    symbols: list = []
    isWord: bool = False

    tab: int = 9
    newLine: int = 10
    carriageReturn: int = 13
    space: int = 32

    for symbol in allBytes:
        if symbol is newLine:
            words.append(symbols)
            symbols = []
            allLines.append(words)
            words = []
            isWord = False
        elif isWord is True and symbol is tab:
            words.append(symbols)
            symbols = []
            isWord = False
        elif symbol is not space and symbol is not tab and symbol is not carriageReturn:
            isWord = True
            symbols.append(symbol)
    return allLines


def __getMeasurements(allLines: list, requiredSatelliteNumber: int, requiredSatelliteNumberSize: int,
                      requiredSatelliteNumber2: int = None) -> list:
    measurements: list = []
    numbersInLine: int = 21
    for line in allLines:
        try:
            satelliteNumber: int = line[0][0]
            satelliteNumberSize: int = len(line[0])
            if requiredSatelliteNumber2 is not None:
                satelliteNumber2: int = line[0][1]
                if (satelliteNumber is requiredSatelliteNumber and
                        satelliteNumber2 is requiredSatelliteNumber2 and
                        satelliteNumberSize is requiredSatelliteNumberSize):
                    lineOfNumbers: list = []
                    for number in range(1, numbersInLine + 1):
                        numeric: float = __getNumeric(line, number)
                        lineOfNumbers.append(numeric)
                    measurements.append(lineOfNumbers)
            else:
                if (satelliteNumber is requiredSatelliteNumber and
                        satelliteNumberSize is requiredSatelliteNumberSize):
                    lineOfNumbers: list = []
                    for number in range(1, numbersInLine + 1):
                        numeric: float = __getNumeric(line, number)
                        lineOfNumbers.append(numeric)
                    measurements.append(lineOfNumbers)
        except Exception:
            pass
    return measurements


def __getNumeric(line: list, number: int) -> float:
    numberBuilder: str = ""
    numberLength: int = len(line[number])
    for digit in range(numberLength):
        symbol: chr = chr(line[number][digit])
        numberBuilder += symbol
    numeric: float = float(numberBuilder)
    return numeric


def __toAsciiCode(number: int) -> int:
    return number + 48


def getTroposphericDelays(allLines: list, amountOfObservations: int, requiredSatelliteNumber) -> list:
    if requiredSatelliteNumber < 10:
        satelliteNumberInAscii: int = __toAsciiCode(requiredSatelliteNumber)
        measurements: list = __getMeasurements(allLines, satelliteNumberInAscii, 1)
    else:
        satelliteNumberFirstDigit = int(str(requiredSatelliteNumber)[0])
        satelliteNumberSecondDigit = int(str(requiredSatelliteNumber)[1])

        satelliteNumberFirstDigitInAscii: int = __toAsciiCode(satelliteNumberFirstDigit)
        satelliteNumberSecondDigitInAscii: int = __toAsciiCode(satelliteNumberSecondDigit)

        measurements: list = __getMeasurements(allLines, satelliteNumberFirstDigitInAscii, 2,
                                               satelliteNumberSecondDigitInAscii)

    delays: list = []
    for observation in range(amountOfObservations):
        md: float = measurements[observation][5]
        td: float = measurements[observation][6]
        mw: float = measurements[observation][7]
        tw: float = measurements[observation][8]
        delay: float = md * td + mw * tw
        delays.append(delay)
    return delays


def getAnglesInHalfCircles(allLines: list, amountOfObservations: int, requiredSatelliteNumber: int):
    if requiredSatelliteNumber < 10:
        satelliteNumberInAscii: int = __toAsciiCode(requiredSatelliteNumber)
        measurements: list = __getMeasurements(allLines, satelliteNumberInAscii, 1)
    else:
        satelliteNumberFirstDigit = int(str(requiredSatelliteNumber)[0])
        satelliteNumberSecondDigit = int(str(requiredSatelliteNumber)[1])

        satelliteNumberFirstDigitInAscii: int = __toAsciiCode(satelliteNumberFirstDigit)
        satelliteNumberSecondDigitInAscii: int = __toAsciiCode(satelliteNumberSecondDigit)

        measurements: list = __getMeasurements(allLines, satelliteNumberFirstDigitInAscii, 2,
                                               satelliteNumberSecondDigitInAscii)

    halfCircle: float = 180
    halfCircles: list = []
    for observation in range(amountOfObservations):
        angleInDegrees: float = measurements[observation][14]
        angleInHalfCircles: float = angleInDegrees / halfCircle
        halfCircles.append(angleInHalfCircles)
    return halfCircles


def printInfo(satellite1Number: int, satellite2Number: int, satellite3Number: int,
              satellite1TroposphericDelays: list, satellite2TroposphericDelays: list,
              satellite3TroposphericDelays: list, satellite1ElevationAngles: list, satellite2ElevationAngles: list,
              satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    print("Спутник #" + str(satellite1Number) + "\t\t\t\t\t\t\t" +
          "Спутник #" + str(satellite2Number) + "\t\t\t\t\t\t\t" +
          "Спутник #" + str(satellite3Number))
    for observation in range(amountOfObservations):
        satellite1TroposphericDelay: float = satellite1TroposphericDelays[observation]
        satellite1ElevationAngle: float = satellite1ElevationAngles[observation]
        satellite2TroposphericDelay: float = satellite2TroposphericDelays[observation]
        satellite2ElevationAngle: float = satellite2ElevationAngles[observation]
        satellite3TroposphericDelay: float = satellite3TroposphericDelays[observation]
        satellite3ElevationAngle: float = satellite3ElevationAngles[observation]
        print(str(round(satellite1TroposphericDelay, 10)) + "\t" + str(round(satellite1ElevationAngle, 10)) + "\t\t" +
              str(round(satellite2TroposphericDelay, 10)) + "\t" + str(round(satellite2ElevationAngle, 10)) + "\t\t" +
              str(round(satellite3TroposphericDelay, 10)) + "\t" + str(round(satellite3ElevationAngle, 10)))


def drawTroposphericDelays(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                           satellite1TroposphericDelays: list, satellite2TroposphericDelays: list,
                           satellite3TroposphericDelays: list, amountOfObservations: int) -> None:
    observations = np.arange(0, amountOfObservations)
    plt.plot(observations, satellite1TroposphericDelays, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2TroposphericDelays, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3TroposphericDelays, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Тропосферная задержка сигнала, метры")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def drawElevationAngles(satellite1Number: int, satellite2Number: int, satellite3Number: int,
                        satellite1ElevationAngles: list, satellite2ElevationAngles: list,
                        satellite3ElevationAngles: list, amountOfObservations: int) -> None:
    observations = np.arange(0, amountOfObservations)
    plt.plot(observations, satellite1ElevationAngles, 'o-', label="Спутник #" + str(satellite1Number))
    plt.plot(observations, satellite2ElevationAngles, 'o-', label="Спутник #" + str(satellite2Number))
    plt.plot(observations, satellite3ElevationAngles, 'o-', label="Спутник #" + str(satellite3Number))
    plt.xlabel("Время")
    plt.ylabel("Угол возвышения спутника, полуциклы")
    plt.legend()
    plt.grid(linestyle='-', linewidth=0.5)
    plt.show()


def main():
    amountOfObservations: int = 360

    satellite1Number: int = %s
    satellite2Number: int = %s
    satellite3Number: int = %s

    fileName: str = "../resources/file.dat"
    allBytes: bytes = readBytes(fileName)
    allLines: list = analyzeSyntaxAndReturnLinesList(allBytes)

    satellite1Delays: list = getTroposphericDelays(allLines, amountOfObservations, satellite1Number)
    satellite2Delays: list = getTroposphericDelays(allLines, amountOfObservations, satellite2Number)
    satellite3Delays: list = getTroposphericDelays(allLines, amountOfObservations, satellite3Number)

    satellite1ElevationAngles: list = getAnglesInHalfCircles(allLines, amountOfObservations, satellite1Number)
    satellite2ElevationAngles: list = getAnglesInHalfCircles(allLines, amountOfObservations, satellite2Number)
    satellite3ElevationAngles: list = getAnglesInHalfCircles(allLines, amountOfObservations, satellite3Number)

    printInfo(satellite1Number, satellite2Number, satellite3Number,
              satellite1Delays, satellite2Delays, satellite3Delays,
              satellite1ElevationAngles, satellite2ElevationAngles, satellite3ElevationAngles,
              amountOfObservations)

    drawTroposphericDelays(satellite1Number, satellite2Number, satellite3Number,
                           satellite1Delays, satellite2Delays, satellite3Delays,
                           amountOfObservations)

    drawElevationAngles(satellite1Number, satellite2Number, satellite3Number,
                        satellite1ElevationAngles, satellite2ElevationAngles, satellite3ElevationAngles,
                        amountOfObservations)


if __name__ == "__main__":
    main()
