package com.gvozdev.codegen.autumn2020;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.repo.OutputFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Date;

@Component
public class OutputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(OutputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/output/";

    @Autowired
    private OutputFileRepository repository;

    @PostConstruct
    @Transactional
    public void init() {
        saveOutputFile(1L, "var1", "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(2L, "var1", "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(3L, "var1", "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(4L, "var1", "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(5L, "var1", "java", "file", "withoop", "Main.java");
        saveOutputFile(6L, "var1", "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(7L, "var1", "java", "manual", "withoop", "Main.java");
        saveOutputFile(8L, "var1", "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(9L, "var1", "python", "file", "nooop", "main.py");
        saveOutputFile(10L, "var1", "python", "file", "withoop", "main.py");
        saveOutputFile(11L, "var1", "python", "manual", "nooop", "main.py");
        saveOutputFile(12L, "var1", "python", "manual", "withoop", "main.py");
        saveOutputFile(13L, "var2", "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(14L, "var2", "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(15L, "var2", "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(16L, "var2", "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(17L, "var2", "java", "file", "withoop", "Main.java");
        saveOutputFile(18L, "var2", "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(19L, "var2", "java", "manual", "withoop", "Main.java");
        saveOutputFile(20L, "var2", "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(21L, "var2", "python", "file", "nooop", "main.py");
        saveOutputFile(22L, "var2", "python", "file", "withoop", "main.py");
        saveOutputFile(23L, "var2", "python", "manual", "nooop", "main.py");
        saveOutputFile(24L, "var2", "python", "manual", "withoop", "main.py");
        saveOutputFile(25L, "var3", "cpp", "file", "nooop", "main.cpp");
        saveOutputFile(26L, "var3", "cpp", "file", "withoop", "main.cpp");
        saveOutputFile(27L, "var3", "cpp", "manual", "nooop", "main.cpp");
        saveOutputFile(28L, "var3", "cpp", "manual", "withoop", "main.cpp");
        saveOutputFile(29L, "var3", "java", "file", "withoop", "Main.java");
        saveOutputFile(30L, "var3", "java", "file", "withoop", "GraphDrawer.java");
        saveOutputFile(31L, "var3", "java", "manual", "withoop", "Main.java");
        saveOutputFile(32L, "var3", "java", "manual", "withoop", "GraphDrawer.java");
        saveOutputFile(33L, "var3", "python", "file", "nooop", "main.py");
        saveOutputFile(34L, "var3", "python", "file", "withoop", "main.py");
        saveOutputFile(35L, "var3", "python", "manual", "nooop", "main.py");
        saveOutputFile(36L, "var3", "python", "manual", "withoop", "main.py");
		
        LOGGER.info("Все выходные файлы для УИРС осени 2020 загружены");
    }

    private void saveOutputFile(Long id, String var, String lang, String file, String oop, String name) {
        try {
            String filePath = PATH + "/" + var + "/" + lang + "/" + file + "/" + oop + "/" + name;
            File codeListing = new File(filePath);
            byte[] fileBytes = Files.readAllBytes(codeListing.toPath());
            OutputFile outputFile = new OutputFile(id, var, lang, file, oop, name, fileBytes, new Date());
            repository.save(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
