package com.gvozdev.codegen.autumn2020.init;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.repo.InputFileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Component
public class InputFilesDatabaseInitializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(InputFilesDatabaseInitializer.class);
    private static final String PATH = "src/main/resources/files/input/";

    @Autowired
    private InputFileRepository repository;

    @PostConstruct
    @Transactional
    public void init() {
        saveInputFile(1L, "BOGI_1-6.dat");
        saveInputFile(2L, "brdc0010.18n");
        saveInputFile(3L, "bshm_1-10.dat");
        saveInputFile(4L, "HUEG_1-6.dat");
        saveInputFile(5L, "hueg_1-10.dat");
        saveInputFile(6L, "igrg0010.18i");
        saveInputFile(7L, "igsg0010.18i");
        saveInputFile(8L, "leij_1-10.dat");
        saveInputFile(9L, "ONSA_1-6.dat");
        saveInputFile(10L, "onsa_1-10.dat");
        saveInputFile(11L, "POTS_6hours.dat");
        saveInputFile(12L, "spt0_1-10.dat");
        saveInputFile(13L, "svtl_1-10.dat");
        saveInputFile(14L, "tit2_1-10.dat");
        saveInputFile(15L, "TITZ_6hours.dat");
        saveInputFile(16L, "vis0_1-10.dat");
        saveInputFile(17L, "warn_1-10.dat");
        saveInputFile(18L, "WARN_6hours.dat");
        saveInputFile(19L, "zeck_1-10.dat");
        saveInputFile(20L, "resources.rar");
        LOGGER.info("Все входные файлы для УИРС осени 2020 загружены");
    }

    private void saveInputFile(Long id, String fileName) {
        try {
            File file = new File(PATH + fileName);
            byte[] fileBytes = Files.readAllBytes(file.toPath());
            InputFile inputFile = new InputFile(id, fileName, fileBytes);
            repository.save(inputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
