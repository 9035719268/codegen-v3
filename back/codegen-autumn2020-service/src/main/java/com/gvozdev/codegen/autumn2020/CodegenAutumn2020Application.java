package com.gvozdev.codegen.autumn2020;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@EnableEurekaClient
public class CodegenAutumn2020Application {
    public static void main(String[] args) {
        run(CodegenAutumn2020Application.class, args);
    }
}
