package com.gvozdev.codegen.autumn2020.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createDocket() {
        return new Docket(SWAGGER_2)
            .apiInfo(getApiInfo())
            .select()
            .paths(PathSelectors.ant("/api/**"))
            .apis(RequestHandlerSelectors.basePackage("com.gvozdev.codegen.autumn2020"))
            .build();
    }

    private static ApiInfo getApiInfo() {
        return new ApiInfoBuilder()
            .title("CodeGen v3 API")
            .description("Autumn 2020 service")
            .contact(getContact())
            .version("1.0")
            .build();
    }

    private static Contact getContact() {
        String name = "Alexander Gvozdev";
        String gitlabProfileLink = "https://gitlab.com/9035719268/codegen-v3";
        String email = "9035719268a@gmail.com";

        return new Contact(name, gitlabProfileLink, email);
    }
}
