package com.gvozdev.codegen.autumn2020.util.var3.filereader;

import com.gvozdev.codegen.autumn2020.util.number.OneDigitNumber;
import com.gvozdev.codegen.autumn2020.util.number.TwoDigitNumber;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.nCopies;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class EphemerisFileReader {
    private final List<List<List<Byte>>> allLines;
    private final EphemerisFileReaderUtil ephemerisFileReaderUtil;
    private final int startOfObservationsLineNumber;
    private final int amountOfObservations;

    public EphemerisFileReader(
        byte[] fileBytes, EphemerisFileReaderUtil ephemerisFileReaderUtil, IonoFileReaderUtil ionoFileReaderUtil,
        int startOfObservationsLineNumber, int amountOfObservations
    ) {
        allLines = ionoFileReaderUtil.extractLinesOfData(fileBytes);
        this.ephemerisFileReaderUtil = ephemerisFileReaderUtil;
        this.startOfObservationsLineNumber = startOfObservationsLineNumber;
        this.amountOfObservations = amountOfObservations;
    }

    public List<Double> getAlphaCoefficients() {
        return extractIonCoefficients(3);
    }

    public List<Double> getBetaCoefficients() {
        return extractIonCoefficients(4);
    }

    public List<Double> getGpsTime(OneDigitNumber requiredSatelliteNumber) {
        List<Double> gpsTime = new ArrayList<>(nCopies(amountOfObservations, 0.0));

        range(startOfObservationsLineNumber, allLines.size())
            .filter(observation -> ephemerisFileReaderUtil.isOneDigitSatelliteNumberValid(allLines, observation))
            .filter(observation -> ephemerisFileReaderUtil.areSatelliteNumbersEqual(requiredSatelliteNumber, observation, allLines))
            .filter(observation -> ephemerisFileReaderUtil.isHourValueEven(allLines, observation))
            .forEach(observation -> {
                List<Byte> hoursNumberInfo = allLines.get(observation).get(4);
                int hourValue = ephemerisFileReaderUtil.getHourValue(hoursNumberInfo);
                double numericGpsTime = ephemerisFileReaderUtil.getNumericGpsTime(allLines, observation);

                int hour = hourValue / 2;
                gpsTime.set(hour, numericGpsTime);
            });

        return gpsTime;
    }

    public List<Double> getGpsTime(TwoDigitNumber requiredSatelliteNumber) {
        List<Double> gpsTime = new ArrayList<>(nCopies(amountOfObservations, 0.0));

        range(startOfObservationsLineNumber, allLines.size())
            .filter(observation -> ephemerisFileReaderUtil.isTwoDigitSatelliteNumberValid(allLines, observation))
            .filter(observation -> ephemerisFileReaderUtil.areSatelliteNumbersEqual(requiredSatelliteNumber, observation, allLines))
            .filter(observation -> ephemerisFileReaderUtil.isHourValueEven(allLines, observation))
            .forEach(observation -> {
                List<Byte> hoursNumberInfo = allLines.get(observation).get(4);
                int hourValue = ephemerisFileReaderUtil.getHourValue(hoursNumberInfo);
                int hour = hourValue / 2;

                double numericGpsTime = ephemerisFileReaderUtil.getNumericGpsTime(allLines, observation);

                gpsTime.set(hour, numericGpsTime);
            });
        return gpsTime;
    }

    private List<Double> extractIonCoefficients(int lineNumber) {
        List<List<Byte>> line = allLines.get(lineNumber);
        int amountOfCoefficients = 4;

        return range(0, amountOfCoefficients)
            .mapToObj(coefficient -> ephemerisFileReaderUtil.getNumericIonCoefficient(line, coefficient))
            .collect(toList());
    }
}
