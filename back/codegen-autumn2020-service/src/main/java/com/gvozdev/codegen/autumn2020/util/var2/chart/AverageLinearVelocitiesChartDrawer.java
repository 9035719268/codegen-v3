package com.gvozdev.codegen.autumn2020.util.var2.chart;

import com.gvozdev.codegen.autumn2020.domain.var2.LinearVelocities;
import com.gvozdev.codegen.autumn2020.domain.var2.Satellite;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTitleAnnotation;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.List;

import static java.awt.Color.WHITE;
import static java.util.stream.IntStream.range;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import static org.jfree.chart.ui.RectangleAnchor.RIGHT;

public class AverageLinearVelocitiesChartDrawer extends TemplateChartDrawer {
    public AverageLinearVelocitiesChartDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        super(satellite1, satellite2, satellite3, amountOfObservations);
    }

    @Override
    XYSeriesCollection getMeasurementsSeriesCollection() {
        LinearVelocities linearVelocitiesForSatellite1 = new LinearVelocities(amountOfObservations);
        java.util.List<Double> satellite1Velocities = linearVelocitiesForSatellite1.getAverageLinearVelocities();

        LinearVelocities linearVelocitiesForSatellite2 = new LinearVelocities(amountOfObservations);
        java.util.List<Double> satellite2Velocities = linearVelocitiesForSatellite2.getAverageLinearVelocities();

        LinearVelocities linearVelocitiesForSatellite3 = new LinearVelocities(amountOfObservations);
        List<Double> satellite3Velocities = linearVelocitiesForSatellite3.getAverageLinearVelocities();

        XYSeries satellite1VelocitiesSeries = new XYSeries("Спутник #" + satellite1.getNumber());
        XYSeries satellite2VelocitiesSeries = new XYSeries("Спутник #" + satellite2.getNumber());
        XYSeries satellite3VelocitiesSeries = new XYSeries("Спутник #" + satellite3.getNumber());

        range(0, 3).forEach(observation -> {
            double linearVelocityForSatellite1 = satellite1Velocities.get(observation);
            satellite1VelocitiesSeries.add(observation, linearVelocityForSatellite1);

            double linearVelocityForSatellite2 = satellite2Velocities.get(observation);
            satellite2VelocitiesSeries.add(observation, linearVelocityForSatellite2);

            double linearVelocityForSatellite3 = satellite3Velocities.get(observation);
            satellite3VelocitiesSeries.add(observation, linearVelocityForSatellite3);
        });

        XYSeriesCollection averageLinearVelocitiesSeriesCollection = new XYSeriesCollection();
        averageLinearVelocitiesSeriesCollection.addSeries(satellite1VelocitiesSeries);
        averageLinearVelocitiesSeriesCollection.addSeries(satellite2VelocitiesSeries);
        averageLinearVelocitiesSeriesCollection.addSeries(satellite3VelocitiesSeries);

        return averageLinearVelocitiesSeriesCollection;
    }

    @Override
    JFreeChart createChart(XYSeriesCollection observationsSeriesCollection) {
        return createXYLineChart(
            "",
            "Время",
            "Средняя линейная скорость, рад/c",
            observationsSeriesCollection,
            VERTICAL,
            true,
            true,
            false
        );
    }

    @Override
    void configureLegend(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        LegendTitle legend = measurementsChart.getLegend();
        legend.setItemFont(LEGEND_FONT);
        legend.setBackgroundPaint(WHITE);
        legend.setFrame(new BlockBorder(WHITE));
        XYTitleAnnotation titleAnnotation = new XYTitleAnnotation(0.98, 0.05, legend, RIGHT);

        titleAnnotation.setMaxWidth(0.48);
        plot.addAnnotation(titleAnnotation);

        measurementsChart.removeLegend();
    }
}
