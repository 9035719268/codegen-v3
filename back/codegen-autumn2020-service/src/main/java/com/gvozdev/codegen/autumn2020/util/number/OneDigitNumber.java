package com.gvozdev.codegen.autumn2020.util.number;

import static java.util.Objects.hash;

public class OneDigitNumber {
    private static final int SIZE = 1;

    private final char firstDigit;

    public OneDigitNumber(char firstDigit) {
        this.firstDigit = firstDigit;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        OneDigitNumber otherNumber = (OneDigitNumber) other;

        return firstDigit == otherNumber.getFirstDigit();
    }

    @Override
    public int hashCode() {
        return hash(firstDigit, SIZE);
    }

    private char getFirstDigit() {
        return firstDigit;
    }
}
