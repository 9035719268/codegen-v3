package com.gvozdev.codegen.autumn2020.util.var1.filereader;

import com.gvozdev.codegen.autumn2020.util.index.ComponentIndexes;
import com.gvozdev.codegen.autumn2020.util.number.OneDigitNumber;
import com.gvozdev.codegen.autumn2020.util.number.TwoDigitNumber;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static java.util.stream.Stream.concat;

public class FileReader {
    private final List<List<List<Byte>>> allLines;
    private final ComponentIndexes componentIndexes;
    private final FileReaderUtil fileReaderUtil;
    private final int amountOfObservations;

    public FileReader(byte[] fileBytes, ComponentIndexes componentIndexes, FileReaderUtil fileReaderUtil, int amountOfObservations) {
        allLines = fileReaderUtil.extractLinesOfData(fileBytes);
        this.componentIndexes = componentIndexes;
        this.fileReaderUtil = fileReaderUtil;
        this.amountOfObservations = amountOfObservations;
    }

    public List<Integer> getSatelliteNumbers() {
        List<Integer> oneDigitSatelliteNumbers = fileReaderUtil.getOneDigitSatelliteNumbers(allLines);
        List<Integer> twoDigitSatelliteNumbers = fileReaderUtil.getTwoDigitSatelliteNumbers(allLines);

        return concat(oneDigitSatelliteNumbers.stream(), twoDigitSatelliteNumbers.stream())
            .collect(toList());
    }

    public List<List<Double>> getPseudoRanges(OneDigitNumber requiredSatelliteNumber) {
        int p1Index = componentIndexes.getP1Index();
        int p2Index = componentIndexes.getP2Index();
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        List<Double> p1List = getPseudoRangesList(measurements, p1Index);
        List<Double> p2List = getPseudoRangesList(measurements, p2Index);

        return asList(p1List, p2List);
    }

    public List<List<Double>> getPseudoRanges(TwoDigitNumber requiredSatelliteNumber) {
        int p1Index = componentIndexes.getP1Index();
        int p2Index = componentIndexes.getP2Index();
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        List<Double> p1List = getPseudoRangesList(measurements, p1Index);
        List<Double> p2List = getPseudoRangesList(measurements, p2Index);

        return asList(p1List, p2List);
    }

    private List<Double> getPseudoRangesList(List<List<Double>> measurements, int componentIndex) {
        return range(0, amountOfObservations)
            .mapToObj(observation -> measurements.get(observation).get(componentIndex))
            .collect(toList());
    }
}
