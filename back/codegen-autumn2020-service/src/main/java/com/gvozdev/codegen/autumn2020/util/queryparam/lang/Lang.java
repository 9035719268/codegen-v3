package com.gvozdev.codegen.autumn2020.util.queryparam.lang;

public enum Lang {
    CPP, JAVA, PYTHON;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
