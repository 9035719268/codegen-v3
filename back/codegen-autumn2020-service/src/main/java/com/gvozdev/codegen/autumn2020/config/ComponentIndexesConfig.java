package com.gvozdev.codegen.autumn2020.config;

import com.gvozdev.codegen.autumn2020.util.index.ComponentIndexes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ComponentIndexesConfig {

    @Bean
    public ComponentIndexes componentIndexes() {
        int p1Index = 1;
        int p2Index = 2;
        int elevationIndex = 14;

        return new ComponentIndexes(p1Index, p2Index, elevationIndex);
    }
}
