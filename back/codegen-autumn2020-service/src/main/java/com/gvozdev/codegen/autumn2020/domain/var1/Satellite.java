package com.gvozdev.codegen.autumn2020.domain.var1;

import java.util.List;

public class Satellite {
    private final int number;
    private final List<Double> p1;
    private final List<Double> p2;

    public Satellite(int number, List<Double> p1, List<Double> p2) {
        this.number = number;
        this.p1 = p1;
        this.p2 = p2;
    }

    public int getNumber() {
        return number;
    }

    public List<Double> getP1() {
        return p1;
    }

    public List<Double> getP2() {
        return p2;
    }
}
