package com.gvozdev.codegen.autumn2020.util.queryparam.file;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.valueOf;

@Component
public class UrlFileQueryParamToFileEnumConverter implements Converter<String, File> {

    @Override
    public File convert(String source) {
        return valueOf(source.toUpperCase());
    }
}
