package com.gvozdev.codegen.autumn2020.util.var2.filereader;

import com.gvozdev.codegen.autumn2020.util.common.CommonUtil;
import com.gvozdev.codegen.autumn2020.util.number.OneDigitNumber;
import com.gvozdev.codegen.autumn2020.util.number.TwoDigitNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static java.lang.Double.parseDouble;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

@Component("Var2FileReaderUtil")
public class FileReaderUtil {

    @Autowired
    private CommonUtil commonUtil;

    public List<Integer> getOneDigitSatelliteNumbers(List<List<List<Byte>>> allLines) {
        return range(1, 10)
            .filter(satelliteNumber -> isAmountOfMeasurementsFromOneDigitSatelliteNumberMoreOrEqualToAmountOfObservations(allLines, satelliteNumber))
            .boxed()
            .collect(toList());
    }

    public List<Integer> getTwoDigitSatelliteNumbers(List<List<List<Byte>>> allLines) {
        return range(10, 41)
            .filter(satelliteNumber -> isAmountOfMeasurementsFromTwoDigitSatelliteNumberMoreOrEqualToAmountOfObservations(allLines, satelliteNumber))
            .boxed()
            .collect(toList());
    }

    public List<List<Double>> getMeasurements(List<List<List<Byte>>> allLines, OneDigitNumber requiredSatelliteNumber) {
        return allLines.stream()
            .filter(FileReaderUtil::isOneDigitSatelliteNumberValid)
            .filter(line -> areSatelliteNumbersEqual(line, requiredSatelliteNumber))
            .map(FileReaderUtil::getLineOfNumbers)
            .collect(toList());
    }

    public List<List<Double>> getMeasurements(List<List<List<Byte>>> allLines, TwoDigitNumber requiredSatelliteNumber) {
        return allLines.stream()
            .filter(FileReaderUtil::isTwoDigitSatelliteNumberValid)
            .filter(line -> areSatelliteNumbersEqual(line, requiredSatelliteNumber))
            .map(FileReaderUtil::getLineOfNumbers)
            .collect(toList());
    }

    public List<List<List<Byte>>> extractLinesOfData(byte[] fileBytes) {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte tab = 9;
        byte newLine = 10;
        byte carriageReturn = 13;
        byte space = 32;

        for (byte symbol : fileBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == tab)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if ((symbol != space) && (symbol != tab) && (symbol != carriageReturn)) {
                isWord = true;
                symbols.add(symbol);
            }
        }

        return allLines;
    }

    private static boolean isOneDigitSatelliteNumberValid(List<List<Byte>> line) {
        try {
            boolean satelliteNumberFirstDigitExists = line.get(0).get(0) != null;
            boolean satelliteNumberSizeEqualsOne = line.get(0).size() == 1;

            return satelliteNumberFirstDigitExists && satelliteNumberSizeEqualsOne;
        } catch (RuntimeException ignored) {
            return false;
        }
    }

    private static boolean isTwoDigitSatelliteNumberValid(List<List<Byte>> line) {
        try {
            boolean satelliteNumberFirstDigitExists = line.get(0).get(0) != null;
            boolean satelliteNumberSecondDigitExists = line.get(0).get(1) != null;
            boolean satelliteNumberSizeEqualsTwo = line.get(0).size() == 2;

            return satelliteNumberFirstDigitExists && satelliteNumberSecondDigitExists && satelliteNumberSizeEqualsTwo;
        } catch (RuntimeException ignored) {
            return false;
        }
    }

    private static boolean areSatelliteNumbersEqual(List<List<Byte>> line, OneDigitNumber requiredSatelliteNumber) {
        char receivedFirstDigit = (char) (byte) line.get(0).get(0);
        OneDigitNumber receivedSatelliteNumber = new OneDigitNumber(receivedFirstDigit);

        return requiredSatelliteNumber.equals(receivedSatelliteNumber);
    }

    private static boolean areSatelliteNumbersEqual(List<List<Byte>> line, TwoDigitNumber requiredSatelliteNumber) {
        char receivedFirstDigit = (char) (byte) line.get(0).get(0);
        char receivedSecondDigit = (char) (byte) line.get(0).get(1);

        TwoDigitNumber receivedSatelliteNumber = new TwoDigitNumber(receivedFirstDigit, receivedSecondDigit);

        return requiredSatelliteNumber.equals(receivedSatelliteNumber);
    }

    private static List<Double> getLineOfNumbers(List<List<Byte>> line) {
        int numbersInLine = 21;

        return range(1, numbersInLine + 1)
            .mapToObj(number -> getNumeric(line, number))
            .collect(toList());
    }

    private static double getNumeric(List<List<Byte>> line, int number) {
        String numeric = line.get(number).stream()
            .map(digit -> (char) digit.byteValue())
            .map(Object::toString)
            .collect(joining());

        return parseDouble(numeric);
    }

    private boolean isAmountOfMeasurementsFromOneDigitSatelliteNumberMoreOrEqualToAmountOfObservations(List<List<List<Byte>>> allLines, int number) {
        char firstDigit = commonUtil.toChar(number);

        OneDigitNumber requiredSatelliteNumber = new OneDigitNumber(firstDigit);
        List<List<Double>> measurements = getMeasurements(allLines, requiredSatelliteNumber);

        return measurements.size() >= AMOUNT_OF_OBSERVATIONS;
    }

    private boolean isAmountOfMeasurementsFromTwoDigitSatelliteNumberMoreOrEqualToAmountOfObservations(List<List<List<Byte>>> allLines, int number) {
        char firstDigit = commonUtil.toChar(commonUtil.getFirstDigit(number));
        char secondDigit = commonUtil.toChar(commonUtil.getSecondDigit(number));

        TwoDigitNumber requiredSatelliteNumber = new TwoDigitNumber(firstDigit, secondDigit);

        List<List<Double>> measurements = getMeasurements(allLines, requiredSatelliteNumber);

        return measurements.size() >= AMOUNT_OF_OBSERVATIONS;
    }
}
