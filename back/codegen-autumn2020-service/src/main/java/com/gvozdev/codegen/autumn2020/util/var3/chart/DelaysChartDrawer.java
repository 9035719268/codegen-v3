package com.gvozdev.codegen.autumn2020.util.var3.chart;

import com.gvozdev.codegen.autumn2020.domain.var3.IonosphericDelay;
import com.gvozdev.codegen.autumn2020.domain.var3.KlobucharModelDelay;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTitleAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import static java.awt.Color.*;
import static java.awt.Font.BOLD;
import static java.awt.Font.PLAIN;
import static java.util.stream.IntStream.range;
import static javax.imageio.ImageIO.write;
import static org.jfree.chart.ChartFactory.createXYLineChart;
import static org.jfree.chart.plot.PlotOrientation.VERTICAL;
import static org.jfree.chart.ui.RectangleAnchor.RIGHT;

public class DelaysChartDrawer {
    private static final Font LEGEND_FONT = new Font("Serif", BOLD, 25);
    private static final Font AXIS_LABEL_FONT = new Font("Serif", BOLD, 20);
    private static final Font AXIS_TICK_FONT = new Font("Serif", PLAIN, 15);

    private final List<IonosphericDelay> forecastDelays;
    private final List<IonosphericDelay> preciseDelays;
    private final List<KlobucharModelDelay> klobucharModelDelays;
    private final int amountOfObservations;

    public DelaysChartDrawer(
        List<IonosphericDelay> forecastDelays, List<IonosphericDelay> preciseDelays, List<KlobucharModelDelay> klobucharModelDelays, int amountOfObservations
    ) {
        this.forecastDelays = forecastDelays;
        this.preciseDelays = preciseDelays;
        this.klobucharModelDelays = klobucharModelDelays;
        this.amountOfObservations = amountOfObservations;
    }

    public byte[] getChartInBytes() {
        XYSeriesCollection observationsSeriesCollection = getMeasurementsSeriesCollection();

        JFreeChart measurementsChart = createChart(observationsSeriesCollection);

        configureChartLines(measurementsChart);
        configureLegend(measurementsChart);
        configureAxes(measurementsChart);
        configureAxesRanges(measurementsChart);

        BufferedImage bufferedImage = measurementsChart.createBufferedImage(2000, 1000);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            write(bufferedImage, "png", byteArrayOutputStream);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return byteArrayOutputStream.toByteArray();
    }

    private XYSeriesCollection getMeasurementsSeriesCollection() {
        XYSeries forecastDelaysSeries = new XYSeries("igrg");
        XYSeries preciseDelaysSeries = new XYSeries("igsg");
        XYSeries klobucharModelDelaysSeries = new XYSeries("klobuchar");

        range(0, amountOfObservations).forEach(observation -> {
            double forecastDelay = forecastDelays.get(observation).getDelayInMeters();
            forecastDelaysSeries.add(observation, forecastDelay);

            double preciseDelay = preciseDelays.get(observation).getDelayInMeters();
            preciseDelaysSeries.add(observation, preciseDelay);

            double klobucharModelDelay = klobucharModelDelays.get(observation).getDelayInMeters();
            klobucharModelDelaysSeries.add(observation, klobucharModelDelay);
        });

        XYSeriesCollection delaysSeriesCollection = new XYSeriesCollection();
        delaysSeriesCollection.addSeries(forecastDelaysSeries);
        delaysSeriesCollection.addSeries(preciseDelaysSeries);
        delaysSeriesCollection.addSeries(klobucharModelDelaysSeries);

        return delaysSeriesCollection;
    }

    private static JFreeChart createChart(XYDataset observationsSeriesCollection) {
        return createXYLineChart(
            "",
            "Время",
            "Ионосферная поправка, метр",
            observationsSeriesCollection,
            VERTICAL,
            true,
            true,
            false
        );
    }

    private static void configureChartLines(JFreeChart measurementsChart) {
        int satellite1Index = 0;
        int satellite2Index = 1;
        int satellite3Index = 2;

        XYPlot plot = measurementsChart.getXYPlot();
        XYItemRenderer plotRenderer = plot.getRenderer();

        plotRenderer.setSeriesPaint(satellite1Index, BLUE);
        plotRenderer.setSeriesStroke(satellite1Index, new BasicStroke(2f));

        plotRenderer.setSeriesPaint(satellite2Index, YELLOW);
        plotRenderer.setSeriesStroke(satellite2Index, new BasicStroke(2f));

        plotRenderer.setSeriesPaint(satellite3Index, RED);
        plotRenderer.setSeriesStroke(satellite3Index, new BasicStroke(2f));
    }

    private static void configureLegend(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        LegendTitle legend = measurementsChart.getLegend();
        legend.setItemFont(LEGEND_FONT);
        legend.setBackgroundPaint(WHITE);
        legend.setFrame(new BlockBorder(WHITE));
        XYTitleAnnotation titleAnnotation = new XYTitleAnnotation(0.98, 0.02, legend, RIGHT);

        titleAnnotation.setMaxWidth(0.48);
        plot.addAnnotation(titleAnnotation);

        measurementsChart.removeLegend();
    }

    private static void configureAxes(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        plot.getDomainAxis().setLabelFont(AXIS_LABEL_FONT);
        plot.getDomainAxis().setTickLabelFont(AXIS_TICK_FONT);
        plot.getRangeAxis().setLabelFont(AXIS_LABEL_FONT);
        plot.getRangeAxis().setTickLabelFont(AXIS_TICK_FONT);
    }

    private static void configureAxesRanges(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        NumberAxis range = (NumberAxis) plot.getDomainAxis();
        range.setTickUnit(new NumberTickUnit(50));
    }
}