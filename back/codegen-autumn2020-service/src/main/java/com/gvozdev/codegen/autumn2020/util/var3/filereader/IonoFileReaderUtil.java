package com.gvozdev.codegen.autumn2020.util.var3.filereader;

import com.gvozdev.codegen.autumn2020.util.number.ThreeDigitNumber;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.*;
import static java.lang.Integer.parseInt;
import static java.lang.Math.abs;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

@Component
public class IonoFileReaderUtil {

    public List<Integer> getTecArray(int longitude, List<List<List<Integer>>> allTecForLatitude) {
        int row = abs((FIRST_LONGITUDE - longitude) / (TEC_VALUES_PER_LINE * DELTA_LONGITUDE));
        int position = (abs((FIRST_LONGITUDE - longitude) / DELTA_LONGITUDE)) - (row * TEC_VALUES_PER_LINE);

        return range(0, VAR3_AMOUNT_OF_OBSERVATIONS)
            .mapToObj(observation -> allTecForLatitude.get(observation).get(row).get(position))
            .collect(toList());
    }

    public boolean isLatitudeValid(List<List<List<Byte>>> allLines, int lineNumber) {
        try {
            boolean latitudeFirstDigitExists = allLines.get(lineNumber).get(0).get(0) != null;
            boolean latitudeSecondDigitExists = allLines.get(lineNumber).get(0).get(1) != null;
            boolean latitudeThirdDigitExists = allLines.get(lineNumber).get(0).get(2) != null;

            return latitudeFirstDigitExists && latitudeSecondDigitExists && latitudeThirdDigitExists;
        } catch (RuntimeException ignored) {
            return false;
        }
    }

    public boolean areLatitudesEqual(List<List<List<Byte>>> allLines, int lineNumber, ThreeDigitNumber requiredLatitude) {
        char receivedFirstDigit = (char) (byte) allLines.get(lineNumber).get(0).get(0);
        char receivedSecondDigit = (char) (byte) allLines.get(lineNumber).get(0).get(1);
        char receivedThirdDigit = (char) (byte) allLines.get(lineNumber).get(0).get(2);

        ThreeDigitNumber receivedLatitude = new ThreeDigitNumber(receivedFirstDigit, receivedSecondDigit, receivedThirdDigit);

        return requiredLatitude.equals(receivedLatitude);
    }

    public List<List<Integer>> getTecPerLatitude(List<List<List<Byte>>> allLines, int lineNumber) {
        int linesWithTecPerLatitude = 5;

        return range(1, linesWithTecPerLatitude + 1)
            .mapToObj(lineWithTec -> getLineOfTec(allLines, lineNumber, lineWithTec))
            .collect(toList());
    }

    public List<List<List<Byte>>> extractLinesOfData(byte[] fileBytes) {
        List<List<List<Byte>>> allLines = new ArrayList<>();
        List<List<Byte>> words = new ArrayList<>();
        List<Byte> symbols = new ArrayList<>();
        boolean isWord = false;

        byte newLine = 10;
        byte space = 32;

        for (byte symbol : fileBytes) {
            if (symbol == newLine) {
                words.add(symbols);
                symbols = new ArrayList<>();
                allLines.add(words);
                words = new ArrayList<>();
                isWord = false;
            } else if ((isWord) && (symbol == space)) {
                words.add(symbols);
                symbols = new ArrayList<>();
                isWord = false;
            } else if (symbol != space) {
                isWord = true;
                symbols.add(symbol);
            }
        }
        return allLines;
    }

    private static List<Integer> getLineOfTec(List<List<List<Byte>>> allLines, int line, int lineWithTec) {
        int numbersInRow = allLines.get(line + lineWithTec).size();

        return range(0, numbersInRow)
            .mapToObj(number -> getNumericTec(allLines, line, lineWithTec, number))
            .collect(toList());
    }

    private static int getNumericTec(List<List<List<Byte>>> allLines, int line, int lineWithTec, int number) {
        String numeric = allLines.get(line + lineWithTec).get(number).stream()
            .map(digit -> (char) digit.byteValue())
            .map(Objects::toString)
            .collect(joining());

        return parseInt(numeric);
    }
}
