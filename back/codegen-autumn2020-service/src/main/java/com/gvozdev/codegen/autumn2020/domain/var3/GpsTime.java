package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

public class GpsTime {
    private final List<Double> gpsTime;

    public GpsTime(List<Double> gpsTime) {
        this.gpsTime = gpsTime;
    }

    public List<Double> getGpsTime() {
        return gpsTime;
    }
}
