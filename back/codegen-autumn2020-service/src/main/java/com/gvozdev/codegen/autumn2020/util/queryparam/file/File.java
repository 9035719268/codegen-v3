package com.gvozdev.codegen.autumn2020.util.queryparam.file;

public enum File {
    FILE, MANUAL;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
