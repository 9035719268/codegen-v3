package com.gvozdev.codegen.autumn2020.util.var1.fileservice;

import com.gvozdev.codegen.autumn2020.domain.var1.Satellite;
import com.gvozdev.codegen.autumn2020.util.common.CommonUtil;
import com.gvozdev.codegen.autumn2020.util.number.OneDigitNumber;
import com.gvozdev.codegen.autumn2020.util.number.TwoDigitNumber;
import com.gvozdev.codegen.autumn2020.util.var1.filereader.FileReader;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class FileService {
    private final FileReader fileReader;
    private final CommonUtil commonUtil;

    public FileService(FileReader fileReader, CommonUtil commonUtil) {
        this.fileReader = fileReader;
        this.commonUtil = commonUtil;
    }

    public List<Satellite> getSatellites() {
        return range(0, 3)
            .mapToObj(this::createSatellite)
            .collect(toList());
    }

    private Satellite createSatellite(int satelliteIndex) {
        List<Integer> satelliteNumbers = extractSatelliteNumbers();
        int satelliteNumber = satelliteNumbers.get(satelliteIndex + 1);

        List<List<List<Double>>> pseudoRanges = extractPseudoRanges(satelliteNumbers);
        List<Double> p1 = pseudoRanges.get(satelliteIndex).get(0);
        List<Double> p2 = pseudoRanges.get(satelliteIndex).get(1);

        return new Satellite(satelliteNumber, p1, p2);
    }

    private List<Integer> extractSatelliteNumbers() {
        return fileReader.getSatelliteNumbers();
    }

    private List<List<List<Double>>> extractPseudoRanges(List<Integer> satelliteNumbers) {
        return satelliteNumbers.stream()
            .map(this::extractPseudoRangesForSatellite)
            .collect(toList());
    }

    private List<List<Double>> extractPseudoRangesForSatellite(int satelliteNumber) {
        char satelliteNumberFirstDigit = commonUtil.toChar(commonUtil.getFirstDigit(satelliteNumber));

        if (commonUtil.isDoubleDigit(satelliteNumber)) {
            char satelliteNumberSecondDigit = commonUtil.toChar(commonUtil.getSecondDigit(satelliteNumber));

            TwoDigitNumber requiredSatelliteNumber = new TwoDigitNumber(satelliteNumberFirstDigit, satelliteNumberSecondDigit);

            return new ArrayList<>(fileReader.getPseudoRanges(requiredSatelliteNumber));
        } else {
            OneDigitNumber requiredSatelliteNumber = new OneDigitNumber(satelliteNumberFirstDigit);

            return new ArrayList<>(fileReader.getPseudoRanges(requiredSatelliteNumber));
        }
    }
}
