package com.gvozdev.codegen.autumn2020.domain.var2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Math.sqrt;
import static java.lang.StrictMath.pow;
import static java.util.stream.IntStream.range;

public class LinearVelocities {
    private final int amountOfObservations;

    public LinearVelocities(int amountOfObservations) {
        this.amountOfObservations = amountOfObservations;
    }

    public List<Double> getAverageLinearVelocities() {
        List<Double> averageVelocities = new ArrayList<>();
        List<Double> angularVelocities = getLinearVelocities();
        int observationsPerHour = 120;

        double firstHourSum = getVelocitySumPerHour(angularVelocities, 0, 120);
        double secondHourSum = getVelocitySumPerHour(angularVelocities, 120, 240);
        double thirdHourSum = getVelocitySumPerHour(angularVelocities, 240, 360);

        double firstHourAverage = firstHourSum / observationsPerHour;
        double secondHourAverage = secondHourSum / observationsPerHour;
        double thirdHourAverage = thirdHourSum / observationsPerHour;

        averageVelocities.add(firstHourAverage);
        averageVelocities.add(secondHourAverage);
        averageVelocities.add(thirdHourAverage);

        return averageVelocities;
    }

    public List<Double> getLinearVelocities() {
        double gravitational = 6.67 * pow(10, -11);
        double earthMass = 5.972E24;
        double earthRadius = 6_371_000;
        double flightHeight = 20_000;

        return range(0, amountOfObservations)
            .mapToObj(observation -> sqrt(gravitational * earthMass / (earthRadius + flightHeight)))
            .collect(Collectors.toList());
    }

    private static double getVelocitySumPerHour(List<Double> velocities, int start, int end) {
        return range(start, end)
            .mapToDouble(velocities::get)
            .sum();
    }
}