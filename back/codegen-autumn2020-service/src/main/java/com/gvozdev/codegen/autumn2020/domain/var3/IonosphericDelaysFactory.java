package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class IonosphericDelaysFactory {
    private final WeightMatrix weightMatrix;
    private final int amountOfObservations;

    public IonosphericDelaysFactory(WeightMatrix weightMatrix, int amountOfObservations) {
        this.weightMatrix = weightMatrix;
        this.amountOfObservations = amountOfObservations;
    }

    public List<IonosphericDelay> getIonosphericDelays(List<Tec> tecList) {
        return range(0, amountOfObservations)
            .mapToObj(observation -> {
                int a1Tec = tecList.get(0).getTec().get(observation);
                int a2Tec = tecList.get(1).getTec().get(observation);
                int a3Tec = tecList.get(2).getTec().get(observation);
                int a4Tec = tecList.get(3).getTec().get(observation);

                return asList(a1Tec, a2Tec, a3Tec, a4Tec);
            })
            .map(Tec::new)
            .map(tec -> new IonosphericDelay(weightMatrix, tec))
            .collect(toList());
    }
}