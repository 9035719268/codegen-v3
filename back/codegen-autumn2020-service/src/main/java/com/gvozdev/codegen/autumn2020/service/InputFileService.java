package com.gvozdev.codegen.autumn2020.service;

import com.gvozdev.codegen.autumn2020.entity.InputFile;

public interface InputFileService {
    InputFile findByName(String name);
}
