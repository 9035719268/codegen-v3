package com.gvozdev.codegen.autumn2020.util.number;

import static java.util.Objects.hash;

public class ThreeDigitNumber {
    private static final int SIZE = 3;

    private final char firstDigit;
    private final char secondDigit;
    private final char thirdDigit;

    public ThreeDigitNumber(char firstDigit, char secondDigit, char thirdDigit) {
        this.firstDigit = firstDigit;
        this.secondDigit = secondDigit;
        this.thirdDigit = thirdDigit;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        ThreeDigitNumber otherNumber = (ThreeDigitNumber) other;

        return firstDigit == otherNumber.getFirstDigit() && secondDigit == otherNumber.getSecondDigit() && thirdDigit == otherNumber.getThirdDigit();
    }

    @Override
    public int hashCode() {
        return hash(firstDigit, secondDigit, thirdDigit, SIZE);
    }

    private char getFirstDigit() {
        return firstDigit;
    }

    private char getSecondDigit() {
        return secondDigit;
    }

    private char getThirdDigit() {
        return thirdDigit;
    }
}
