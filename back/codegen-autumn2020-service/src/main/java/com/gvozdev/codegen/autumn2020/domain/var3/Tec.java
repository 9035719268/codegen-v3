package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

public class Tec {
    private final List<Integer> tec;

    public Tec(List<Integer> tec) {
        this.tec = tec;
    }

    public List<Integer> getTec() {
        return tec;
    }
}