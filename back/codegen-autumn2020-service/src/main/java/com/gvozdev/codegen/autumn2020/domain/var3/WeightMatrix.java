package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.ArrayList;
import java.util.List;

public class WeightMatrix {
    private final List<Double> weights;

    public WeightMatrix(AxisGeo axisGeo) {
        double xpp = axisGeo.getXpp();
        double ypp = axisGeo.getYpp();

        weights = new ArrayList<>(4);
        weights.add(xpp * ypp);
        weights.add((1 - xpp) * ypp);
        weights.add((1 - xpp) * (1 - ypp));
        weights.add(xpp * (1 - ypp));
    }

    public List<Double> getWeights() {
        return weights;
    }
}