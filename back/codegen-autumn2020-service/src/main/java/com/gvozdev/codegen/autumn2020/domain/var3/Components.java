package com.gvozdev.codegen.autumn2020.domain.var3;

import com.gvozdev.codegen.autumn2020.util.var3.interpolation.InterpolationCoordinates;

import java.util.List;

public class Components {
    private final InterpolationCoordinates interpolationCoordinates;
    private final GpsTime gpsTime;
    private final IonCoefficients alpha;
    private final IonCoefficients beta;
    private final List<Tec> forecastValues;
    private final List<Tec> preciseValues;

    public Components(
        InterpolationCoordinates interpolationCoordinates, GpsTime gpsTime,
        IonCoefficients alpha, IonCoefficients beta,
        List<Tec> forecastValues, List<Tec> preciseValues
    ) {
        this.interpolationCoordinates = interpolationCoordinates;
        this.gpsTime = gpsTime;
        this.alpha = alpha;
        this.beta = beta;
        this.forecastValues = forecastValues;
        this.preciseValues = preciseValues;
    }

    public InterpolationCoordinates getInterpolationCoordinates() {
        return interpolationCoordinates;
    }

    public GpsTime getGpsTime() {
        return gpsTime;
    }

    public IonCoefficients getAlpha() {
        return alpha;
    }

    public IonCoefficients getBeta() {
        return beta;
    }

    public List<Tec> getForecastValues() {
        return forecastValues;
    }

    public List<Tec> getPreciseValues() {
        return preciseValues;
    }
}
