package com.gvozdev.codegen.autumn2020.util.var2.filereader;

import com.gvozdev.codegen.autumn2020.util.index.ComponentIndexes;
import com.gvozdev.codegen.autumn2020.util.number.OneDigitNumber;
import com.gvozdev.codegen.autumn2020.util.number.TwoDigitNumber;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static java.util.stream.Stream.concat;

public class FileReader {
    private final List<List<List<Byte>>> allLines;
    private final ComponentIndexes componentIndexes;
    private final FileReaderUtil fileReaderUtil;
    private final int amountOfObservations;

    public FileReader(byte[] fileBytes, ComponentIndexes componentIndexes, FileReaderUtil fileReaderUtil, int amountOfObservations) {
        allLines = fileReaderUtil.extractLinesOfData(fileBytes);
        this.componentIndexes = componentIndexes;
        this.fileReaderUtil = fileReaderUtil;
        this.amountOfObservations = amountOfObservations;
    }

    public List<Integer> getSatelliteNumbers() {
        List<Integer> oneDigitSatelliteNumbers = fileReaderUtil.getOneDigitSatelliteNumbers(allLines);
        List<Integer> twoDigitSatelliteNumbers = fileReaderUtil.getTwoDigitSatelliteNumbers(allLines);

        return concat(oneDigitSatelliteNumbers.stream(), twoDigitSatelliteNumbers.stream())
            .collect(toList());
    }

    public List<Double> getElevationAngles(OneDigitNumber requiredSatelliteNumber) {
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        return getElevationAnglesList(measurements);
    }

    public List<Double> getElevationAngles(TwoDigitNumber requiredSatelliteNumber) {
        List<List<Double>> measurements = fileReaderUtil.getMeasurements(allLines, requiredSatelliteNumber);

        return getElevationAnglesList(measurements);
    }

    private List<Double> getElevationAnglesList(List<List<Double>> measurements) {
        int elevationIndex = componentIndexes.getElevationIndex();

        return range(0, amountOfObservations)
            .mapToObj(observation -> measurements.get(observation).get(elevationIndex))
            .collect(toList());
    }
}
