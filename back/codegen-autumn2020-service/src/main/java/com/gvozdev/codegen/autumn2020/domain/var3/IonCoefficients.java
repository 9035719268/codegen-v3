package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

public class IonCoefficients {
    private final List<Double> ionCoefficients;

    public IonCoefficients(List<Double> ionCoefficients) {
        this.ionCoefficients = ionCoefficients;
    }

    public List<Double> getIonCoefficients() {
        return ionCoefficients;
    }
}
