package com.gvozdev.codegen.autumn2020.util.queryparam.var;

public enum Var {
    VAR1, VAR2, VAR3;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
