package com.gvozdev.codegen.autumn2020.restadapter.task.var3;

import com.gvozdev.codegen.autumn2020.domain.var3.Components;
import com.gvozdev.codegen.autumn2020.domain.var3.IonosphericDelay;
import com.gvozdev.codegen.autumn2020.domain.var3.KlobucharModelDelay;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileService;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import com.gvozdev.codegen.autumn2020.util.queryparam.file.File;
import com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang;
import com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop;
import com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.codegen.autumn2020.util.queryparam.var.Var;
import com.gvozdev.codegen.autumn2020.util.var3.chart.DelaysChartDrawer;
import com.gvozdev.codegen.autumn2020.util.var3.controller.ControllerUtil;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.EphemerisFileReader;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.EphemerisFileReaderUtil;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.IonoFileReader;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.IonoFileReaderUtil;
import com.gvozdev.codegen.autumn2020.util.var3.fileservice.FileService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.*;
import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.FILE;
import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.MANUAL;
import static com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName.*;
import static com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName.CHARTS;
import static com.gvozdev.codegen.autumn2020.util.queryparam.var.Var.VAR3;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.ContentDisposition.attachment;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@Controller("Var3Controller")
@RequestMapping("/api/tasks/year/2020/autumn/var3")
@CrossOrigin("http://localhost:3000")
public class PageController {
    private static final Var VAR = VAR3;

    private final InputFileService inputFileService;
    private final OutputFileService outputFileService;
    private final ControllerUtil controllerUtil;
    private final EphemerisFileReaderUtil ephemerisFileReaderUtil;
    private final IonoFileReaderUtil ionoFileReaderUtil;

    public PageController(
        InputFileService inputFileService, OutputFileService outputFileService,
        ControllerUtil controllerUtil, EphemerisFileReaderUtil ephemerisFileReaderUtil, IonoFileReaderUtil ionoFileReaderUtil
    ) {
        this.inputFileService = inputFileService;
        this.outputFileService = outputFileService;
        this.controllerUtil = controllerUtil;
        this.ephemerisFileReaderUtil = ephemerisFileReaderUtil;
        this.ionoFileReaderUtil = ionoFileReaderUtil;
    }

    @GetMapping("/exercise-info")
    public ResponseEntity<Components> getComponents(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        if (controllerUtil.areInputParametersInvalid(latitude, longitude, satelliteNumber)) {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        InputFile ephemerisFile = inputFileService.findByName(BRDC.getFileName());
        byte[] ephemerisFileBytes = ephemerisFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            ephemerisFileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        InputFile forecastFile = inputFileService.findByName(IGRG.getFileName());
        byte[] forecastFileBytes = forecastFile.getFileBytes();
        IonoFileReader forecastIonoFileReader = new IonoFileReader(forecastFileBytes, ionoFileReaderUtil);

        InputFile preciseFile = inputFileService.findByName(IGSG.getFileName());
        byte[] preciseFileBytes = preciseFile.getFileBytes();
        IonoFileReader preciseIonoFileReader = new IonoFileReader(preciseFileBytes, ionoFileReaderUtil);

        FileService fileService = new FileService(
            ephemerisFileReader, forecastIonoFileReader, preciseIonoFileReader, satelliteNumber, FORECAST_TEC_LIST_FIRST_LINE, PRECISE_TEC_LIST_FIRST_LINE
        );

        String smallerLatitude = controllerUtil.getSmallerLatitudeForInterpolation(latitude);
        String biggerLatitude = controllerUtil.getBiggerLatitudeForInterpolation(latitude);

        String smallerLongitude = controllerUtil.getSmallerLongitudeForInterpolation(longitude);
        String biggerLongitude = controllerUtil.getBiggerLongitudeForInterpolation(longitude);

        Components components = fileService.getComponents(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);

        return new ResponseEntity<>(components, OK);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{outputFileName}")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") Lang lang,
        @PathVariable("file") File file,
        @PathVariable("oop") Oop oop,
        @PathVariable("outputFileName") OutputFileName outputFileName,
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), lang.toString(), file.toString(), oop.toString(), outputFileName.getFileName()
        );

        if (controllerUtil.areInputParametersInvalid(latitude, longitude, satelliteNumber)) {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        InputFile ephemerisFile = inputFileService.findByName("brdc0010.18n");
        byte[] ephemerisFileBytes = ephemerisFile.getFileBytes();

        InputFile forecastFile = inputFileService.findByName("igrg0010.18i");
        byte[] forecastFileBytes = forecastFile.getFileBytes();

        InputFile preciseFile = inputFileService.findByName("igsg0010.18i");
        byte[] preciseFileBytes = preciseFile.getFileBytes();

        byte[] outputFileBytes = outputFile.getFileBytes();

        String listing;

        if (file.equals(FILE)) {
            listing = controllerUtil.getFilledListingForFile(outputFileBytes, satelliteNumber, latitude, longitude);
        } else if (file.equals(MANUAL)) {
            listing =
                controllerUtil.getFilledListingForManual(ephemerisFileBytes, forecastFileBytes, preciseFileBytes, outputFileBytes, satelliteNumber, latitude,
                    longitude);
        } else {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        byte[] listingBytes = listing.getBytes(UTF_8);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_PDF);
        responseHeaders.setContentDisposition(attachment().filename(outputFileName.getFileName()).build());

        return new ResponseEntity<>(listingBytes, responseHeaders, OK);
    }

    @GetMapping("/download/charts")
    public ResponseEntity<byte[]> downloadCharts(
        @RequestParam("lat") String latitude,
        @RequestParam("lon") String longitude,
        @RequestParam("satnum") String satelliteNumber
    ) {
        if (controllerUtil.areInputParametersInvalid(latitude, longitude, satelliteNumber)) {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        InputFile ephemerisFile = inputFileService.findByName(BRDC.getFileName());
        byte[] ephemerisFileBytes = ephemerisFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            ephemerisFileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        InputFile forecastFile = inputFileService.findByName(IGRG.getFileName());
        byte[] forecastFileBytes = forecastFile.getFileBytes();
        IonoFileReader forecastIonoFileReader = new IonoFileReader(forecastFileBytes, ionoFileReaderUtil);

        InputFile preciseFile = inputFileService.findByName(IGSG.getFileName());
        byte[] preciseFileBytes = preciseFile.getFileBytes();
        IonoFileReader preciseIonoFileReader = new IonoFileReader(preciseFileBytes, ionoFileReaderUtil);

        FileService fileService = new FileService(
            ephemerisFileReader, forecastIonoFileReader, preciseIonoFileReader, satelliteNumber, FORECAST_TEC_LIST_FIRST_LINE, PRECISE_TEC_LIST_FIRST_LINE
        );

        String smallerLatitude = controllerUtil.getSmallerLatitudeForInterpolation(latitude);
        String biggerLatitude = controllerUtil.getBiggerLatitudeForInterpolation(latitude);

        String smallerLongitude = controllerUtil.getSmallerLongitudeForInterpolation(longitude);
        String biggerLongitude = controllerUtil.getBiggerLongitudeForInterpolation(longitude);

        Components components = fileService.getComponents(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);

        List<IonosphericDelay> forecastDelays = controllerUtil.getForecastDelays(
            components, latitude, longitude, smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude
        );

        List<IonosphericDelay> preciseDelays = controllerUtil.getPreciseDelays(
            components, latitude, longitude, smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude
        );

        List<KlobucharModelDelay> klobucharModelDelays = controllerUtil.getKlobucharModels(components, latitude, longitude);

        DelaysChartDrawer delaysChartDrawer = new DelaysChartDrawer(forecastDelays, preciseDelays, klobucharModelDelays, VAR3_AMOUNT_OF_OBSERVATIONS);
        byte[] delaysChartInBytes = delaysChartDrawer.getChartInBytes();

        ByteArrayOutputStream byteArrayOutputStream = controllerUtil.getChartsArchiveInBytes(delaysChartInBytes);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(CHARTS.getFileName()).build());

        return new ResponseEntity<>(byteArrayOutputStream.toByteArray(), responseHeaders, OK);
    }
}