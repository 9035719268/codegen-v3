package com.gvozdev.codegen.autumn2020.util.var3.controller;

import com.gvozdev.codegen.autumn2020.domain.var3.*;
import com.gvozdev.codegen.autumn2020.util.common.CommonUtil;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.EphemerisFileReader;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.EphemerisFileReaderUtil;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.IonoFileReader;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.IonoFileReaderUtil;
import com.gvozdev.codegen.autumn2020.util.var3.fileservice.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.*;
import static java.lang.Double.parseDouble;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;

@Component("Var3ControllerUtil")
public class ControllerUtil {

    @Autowired
    private CommonUtil commonUtil;

    @Autowired
    private EphemerisFileReaderUtil ephemerisFileReaderUtil;

    @Autowired
    private IonoFileReaderUtil ionoFileReaderUtil;

    public boolean areInputParametersInvalid(String latitude, String longitude, String satelliteNumber) {
        return isLatitudeInvalid(latitude) || isLongitudeInvalid(longitude) || isSatelliteNumberInvalid(satelliteNumber);
    }

    public String getSmallerLatitudeForInterpolation(String latitude) {
        double lat = parseDouble(latitude);

        return String.valueOf(commonUtil.roundDownToNumber(lat, 2.5));
    }

    public String getBiggerLatitudeForInterpolation(String latitude) {
        double offset = 2.5;
        double lat = parseDouble(latitude);
        double roundedNumber = commonUtil.roundUpToNumber(lat, offset);

        if (roundedNumber == lat) {
            return String.valueOf(roundedNumber + offset);
        }

        return String.valueOf(roundedNumber);
    }

    public String getSmallerLongitudeForInterpolation(String longitude) {
        double lon = parseDouble(longitude);

        return String.valueOf((int) commonUtil.roundDownToNumber(lon, 5.0));
    }

    public String getBiggerLongitudeForInterpolation(String longitude) {
        double offset = 5.0;
        double lon = parseDouble(longitude);
        int roundedNumber = (int) commonUtil.roundUpToNumber(lon, offset);

        if (roundedNumber == lon) {
            return String.valueOf(roundedNumber + offset);
        }

        return String.valueOf(roundedNumber);
    }

    public String getFilledListingForFile(byte[] outputFileBytes, String satelliteNumber, String latitude, String longitude) {
        String smallerLatitude = getSmallerLatitudeForInterpolation(latitude);
        String biggerLatitude = getBiggerLatitudeForInterpolation(latitude);

        String smallerLongitude = getSmallerLongitudeForInterpolation(longitude);
        String biggerLongitude = getBiggerLongitudeForInterpolation(longitude);

        String draftListing = new String(outputFileBytes);

        return String.format(
            draftListing,
            satelliteNumber, latitude, longitude, smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude
        );
    }

    public String getFilledListingForManual(
        byte[] ephemerisFileBytes, byte[] forecastFileBytes, byte[] preciseFileBytes, byte[] outputFileBytes,
        String satelliteNumber, String latitude, String longitude
    ) {
        String smallerLatitude = getSmallerLatitudeForInterpolation(latitude);
        String biggerLatitude = getBiggerLatitudeForInterpolation(latitude);

        String smallerLongitude = getSmallerLongitudeForInterpolation(longitude);
        String biggerLongitude = getBiggerLongitudeForInterpolation(longitude);

        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            ephemerisFileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );
        IonoFileReader forecastIonoFileReader = new IonoFileReader(forecastFileBytes, ionoFileReaderUtil);
        IonoFileReader preciseIonoFileReader = new IonoFileReader(preciseFileBytes, ionoFileReaderUtil);

        FileService fileService = new FileService(
            ephemerisFileReader, forecastIonoFileReader, preciseIonoFileReader, satelliteNumber, FORECAST_TEC_LIST_FIRST_LINE, PRECISE_TEC_LIST_FIRST_LINE
        );
        Components components = fileService.getComponents(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);

        List<Double> alpha = components.getAlpha().getIonCoefficients();
        String alphaString = parseNumericMeasurements(alpha);

        List<Double> beta = components.getBeta().getIonCoefficients();
        String betaString = parseNumericMeasurements(beta);

        List<Double> gpsTime = components.getGpsTime().getGpsTime();
        String gpsTimeString = parseNumericMeasurements(gpsTime);

        List<Integer> forecastA1 = components.getForecastValues().get(0).getTec();
        String forecastA1String = parseNumericMeasurements(forecastA1);

        List<Integer> forecastA2 = components.getForecastValues().get(1).getTec();
        String forecastA2String = parseNumericMeasurements(forecastA2);

        List<Integer> forecastA3 = components.getForecastValues().get(2).getTec();
        String forecastA3String = parseNumericMeasurements(forecastA3);

        List<Integer> forecastA4 = components.getForecastValues().get(3).getTec();
        String forecastA4String = parseNumericMeasurements(forecastA4);

        List<Integer> preciseA1 = components.getPreciseValues().get(0).getTec();
        String preciseA1String = parseNumericMeasurements(preciseA1);

        List<Integer> preciseA2 = components.getPreciseValues().get(1).getTec();
        String preciseA2String = parseNumericMeasurements(preciseA2);

        List<Integer> preciseA3 = components.getPreciseValues().get(2).getTec();
        String preciseA3String = parseNumericMeasurements(preciseA3);

        List<Integer> preciseA4 = components.getPreciseValues().get(3).getTec();
        String preciseA4String = parseNumericMeasurements(preciseA4);

        String draftListing = new String(outputFileBytes);

        return String.format(
            draftListing,
            latitude, longitude, smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude, alphaString, betaString, gpsTimeString,
            forecastA1String, forecastA2String, forecastA3String, forecastA4String, preciseA1String, preciseA2String, preciseA3String, preciseA4String
        );
    }

    public List<IonosphericDelay> getForecastDelays(
        Components components,
        String latitude, String longitude,
        String smallerLatitude, String biggerLatitude,
        String smallerLongitude, String biggerLongitude
    ) {
        UserGeo userGeo = new UserGeo(latitude, longitude);
        IgpGeo igpGeo = new IgpGeo(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);
        AxisGeo axisGeo = new AxisGeo(userGeo, igpGeo);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        List<Tec> forecastTecValues = components.getForecastValues();
        IonosphericDelaysFactory ionosphericDelaysFactory = new IonosphericDelaysFactory(weightMatrix, VAR3_AMOUNT_OF_OBSERVATIONS);

        return ionosphericDelaysFactory.getIonosphericDelays(forecastTecValues);
    }

    public List<IonosphericDelay> getPreciseDelays(
        Components components,
        String latitude, String longitude,
        String smallerLatitude, String biggerLatitude,
        String smallerLongitude, String biggerLongitude
    ) {
        UserGeo userGeo = new UserGeo(latitude, longitude);
        IgpGeo igpGeo = new IgpGeo(smallerLatitude, biggerLatitude, smallerLongitude, biggerLongitude);
        AxisGeo axisGeo = new AxisGeo(userGeo, igpGeo);
        WeightMatrix weightMatrix = new WeightMatrix(axisGeo);

        List<Tec> preciseTecValues = components.getPreciseValues();
        IonosphericDelaysFactory ionosphericDelaysFactory = new IonosphericDelaysFactory(weightMatrix, VAR3_AMOUNT_OF_OBSERVATIONS);

        return ionosphericDelaysFactory.getIonosphericDelays(preciseTecValues);
    }

    public List<KlobucharModelDelay> getKlobucharModels(Components components, String latitude, String longitude) {
        GpsTime gpsTime = components.getGpsTime();
        IonCoefficients alpha = components.getAlpha();
        IonCoefficients beta = components.getBeta();
        KlobucharDelaysFactory klobucharDelaysFactory = new KlobucharDelaysFactory(latitude, longitude, gpsTime, alpha, beta, VAR3_AMOUNT_OF_OBSERVATIONS);

        return klobucharDelaysFactory.getKlobucharModels();
    }

    public ByteArrayOutputStream getChartsArchiveInBytes(byte[] delaysChartInBytes) {
        String delaysChartName = "delaysChart.dat";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);

        ZipEntry delaysEntry = new ZipEntry(delaysChartName);
        delaysEntry.setSize(delaysChartInBytes.length);

        try {
            zipOutputStream.putNextEntry(delaysEntry);
            zipOutputStream.write(delaysChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return byteArrayOutputStream;
    }

    private static boolean isLatitudeInvalid(String latitude) {
        boolean latitudeDoesNotContainDot = !latitude.contains(".");
        boolean numberIsNotParsable = !isNumberParsable(latitude);

        return latitudeDoesNotContainDot || numberIsNotParsable;
    }

    private static boolean isLongitudeInvalid(String longitude) {
        boolean longitudeContainsDot = longitude.contains(".");
        boolean numberIsNotParsable = !isNumberParsable(longitude);

        return longitudeContainsDot || numberIsNotParsable;
    }

    private static boolean isSatelliteNumberInvalid(String satelliteNumber) {
        boolean numberIsParsable = isNumberParsable(satelliteNumber);

        if (numberIsParsable) {
            double satelliteNumberValue = parseDouble(satelliteNumber);
            return satelliteNumberValue < 1 || satelliteNumberValue > 40;
        }

        return true;
    }

    private static boolean isNumberParsable(String number) {
        try {
            parseDouble(number);

            return true;
        } catch (NumberFormatException numberFormatException) {
            return false;
        }
    }

    private static String parseNumericMeasurements(List<? extends Number> measurements) {
        return range(0, measurements.size())
            .mapToObj(observation -> handleMeasurement(observation, measurements))
            .collect(joining());
    }

    private static String handleMeasurement(int observation, List<? extends Number> measurements) {
        if (isElementLast(observation, measurements)) {
            return measurements.get(observation) + "";
        } else {
            return measurements.get(observation) + " ";
        }
    }

    private static boolean isElementLast(int observation, List<? extends Number> measurements) {
        return observation == measurements.size() - 1;
    }
}
