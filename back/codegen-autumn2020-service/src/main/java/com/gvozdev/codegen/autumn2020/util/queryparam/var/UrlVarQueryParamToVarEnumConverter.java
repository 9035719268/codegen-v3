package com.gvozdev.codegen.autumn2020.util.queryparam.var;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import static com.gvozdev.codegen.autumn2020.util.queryparam.var.Var.valueOf;

@Component
public class UrlVarQueryParamToVarEnumConverter implements Converter<String, Var> {

    @Override
    public Var convert(String source) {
        return valueOf(source.toUpperCase());
    }
}