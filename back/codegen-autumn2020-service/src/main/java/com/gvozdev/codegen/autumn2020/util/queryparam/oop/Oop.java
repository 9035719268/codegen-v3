package com.gvozdev.codegen.autumn2020.util.queryparam.oop;

public enum Oop {
    WITHOOP, NOOOP;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
