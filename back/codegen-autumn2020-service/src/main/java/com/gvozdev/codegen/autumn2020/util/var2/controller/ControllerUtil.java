package com.gvozdev.codegen.autumn2020.util.var2.controller;

import com.gvozdev.codegen.autumn2020.domain.var2.Satellite;
import com.gvozdev.codegen.autumn2020.util.common.CommonUtil;
import com.gvozdev.codegen.autumn2020.util.index.ComponentIndexes;
import com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang;
import com.gvozdev.codegen.autumn2020.util.var2.filereader.FileReader;
import com.gvozdev.codegen.autumn2020.util.var2.filereader.FileReaderUtil;
import com.gvozdev.codegen.autumn2020.util.var2.fileservice.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang.JAVA;
import static java.lang.String.format;
import static java.util.stream.Collectors.joining;
import static java.util.stream.IntStream.range;

@Component("Var2ControllerUtil")
public class ControllerUtil {

    @Autowired
    private ComponentIndexes componentIndexes;

    @Autowired
    private FileReaderUtil fileReaderUtil;

    @Autowired
    private CommonUtil commonUtil;

    public String getFilledListingForFile(byte[] inputFileBytes, byte[] outputFileBytes) {
        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);
        List<Satellite> satellites = fileService.getSatellites();

        String draftListing = new String(outputFileBytes);

        int satellite1Number = satellites.get(0).getNumber();
        int satellite2Number = satellites.get(1).getNumber();
        int satellite3Number = satellites.get(2).getNumber();

        return format(
            draftListing,
            satellite1Number, satellite2Number, satellite3Number
        );
    }

    public String getFilledListingForManual(byte[] inputFileBytes, byte[] outputFileBytes, Lang lang) {
        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);
        List<Satellite> satellites = fileService.getSatellites();

        String draftListing = new String(outputFileBytes);

        int satellite1Number = satellites.get(0).getNumber();
        int satellite2Number = satellites.get(1).getNumber();
        int satellite3Number = satellites.get(2).getNumber();

        List<Double> satellite1Elevation = satellites.get(0).getElevation();
        List<Double> satellite2Elevation = satellites.get(1).getElevation();
        List<Double> satellite3Elevation = satellites.get(2).getElevation();

        String satellite1ElevationWithLineBreaks = getMeasurementsWithLineBreaks(satellite1Elevation, lang);
        String satellite2ElevationWithLineBreaks = getMeasurementsWithLineBreaks(satellite2Elevation, lang);
        String satellite3ElevationWithLineBreaks = getMeasurementsWithLineBreaks(satellite3Elevation, lang);

        return format(
            draftListing,
            satellite1Number, satellite2Number, satellite3Number,
            satellite1ElevationWithLineBreaks, satellite2ElevationWithLineBreaks, satellite3ElevationWithLineBreaks
        );
    }

    public ByteArrayOutputStream getChartsArchiveInBytes(
        byte[] angularVelocitiesChartInBytes, byte[] averageAngularVelocitiesChartInBytes,
        byte[] linearVelocitiesChartInBytes, byte[] averageLinearVelocitiesChartInBytes
    ) {
        String angularVelocitiesChartName = "angularVelocitiesChart.dat";
        String averageAngularVelocitiesChartName = "averageAngularVelocitiesChart.dat";
        String linearVelocitiesChartName = "linearVelocitiesChart.dat";
        String averageLinearVelocitiesChartName = "averageLinearVelocitiesChart.dat";

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);

        ZipEntry angularVelocitiesEntry = new ZipEntry(angularVelocitiesChartName);
        angularVelocitiesEntry.setSize(angularVelocitiesChartInBytes.length);

        ZipEntry averageAngularVelocitiesEntry = new ZipEntry(averageAngularVelocitiesChartName);
        averageAngularVelocitiesEntry.setSize(averageAngularVelocitiesChartInBytes.length);

        ZipEntry linearVelocitiesEntry = new ZipEntry(linearVelocitiesChartName);
        linearVelocitiesEntry.setSize(linearVelocitiesChartInBytes.length);

        ZipEntry averageLinearVelocitiesEntry = new ZipEntry(averageLinearVelocitiesChartName);
        averageLinearVelocitiesEntry.setSize(averageLinearVelocitiesChartInBytes.length);

        try {
            zipOutputStream.putNextEntry(angularVelocitiesEntry);
            zipOutputStream.write(angularVelocitiesChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.putNextEntry(averageAngularVelocitiesEntry);
            zipOutputStream.write(averageAngularVelocitiesChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.putNextEntry(linearVelocitiesEntry);
            zipOutputStream.write(linearVelocitiesChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.putNextEntry(averageLinearVelocitiesEntry);
            zipOutputStream.write(averageLinearVelocitiesChartInBytes);
            zipOutputStream.closeEntry();

            zipOutputStream.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return byteArrayOutputStream;
    }

    private static String getMeasurementsWithLineBreaks(List<Double> measurements, Lang lang) {
        return range(0, AMOUNT_OF_OBSERVATIONS)
            .mapToObj(observation -> handleLineBreaks(observation, lang, measurements))
            .collect(joining());
    }

    private static String handleLineBreaks(int observation, Lang lang, List<Double> measurements) {
        if (isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsJava(observation, lang)) {
            return measurements.get(observation) + "";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsJava(observation, lang)) {
            return measurements.get(observation) + "\n\t\t\t";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsNotJava(observation, lang)) {
            return measurements.get(observation) + "";
        } else if (isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsNotJava(observation, lang)) {
            return measurements.get(observation) + "\n\t\t";
        } else {
            return measurements.get(observation) + " ";
        }
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean isLast = (observation == (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && isLast && lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation != (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && notLast && lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsLastAndLangIsNotJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean isLast = (observation == (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && isLast && !lang.equals(JAVA);
    }

    private static boolean isObservationNumberDivisibleBy10AndObservationNumberIsNotLastAndLangIsNotJava(int observation, Lang lang) {
        boolean divisibleBy10 = (observation + 1) % 10 == 0;
        boolean notLast = (observation != (AMOUNT_OF_OBSERVATIONS - 1));

        return divisibleBy10 && notLast && !lang.equals(JAVA);
    }
}
