package com.gvozdev.codegen.autumn2020.restadapter.resource;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName.RESOURCES;
import static org.springframework.http.ContentDisposition.attachment;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;

@Controller("ResourcesController")
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/tasks/year/2020/autumn")
public class PageController {

    @Autowired
    private InputFileService inputFileService;

    @GetMapping("/download/resources")
    public ResponseEntity<byte[]> downloadResources() {
        String inputFileName = RESOURCES.getFileName();
        InputFile inputFile = inputFileService.findByName(inputFileName);

        byte[] fileBytes = inputFile.getFileBytes();

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(inputFileName).build());

        return new ResponseEntity<>(fileBytes, responseHeaders, OK);
    }
}
