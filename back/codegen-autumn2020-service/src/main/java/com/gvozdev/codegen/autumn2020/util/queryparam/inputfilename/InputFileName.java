package com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename;

import java.util.stream.Stream;

public enum InputFileName {
    BOGI_6("BOGI_1-6.dat"),
    BRDC("brdc0010.18n"),
    BSHM_10("bshm_1-10.dat"),
    HUEG_6("HUEG_1-6.dat"),
    HUEG_10("hueg_1-10.dat"),
    IGRG("igrg0010.18i"),
    IGSG("igsg0010.18i"),
    LEIJ_10("leij_1-10.dat"),
    ONSA_6("ONSA_1-6.dat"),
    ONSA_10("onsa_1-10.dat"),
    POTS_6("POTS_6hours.dat"),
    SPT_10("spt0_1-10.dat"),
    SVTL_10("svtl_1-10.dat"),
    TIT_10("tit2_1-10.dat"),
    TIT_6("TITZ_6hours.dat"),
    VIS_10("vis0_1-10.dat"),
    WARN_10("warn_1-10.dat"),
    WARN_6("WARN_6hours.dat"),
    ZECK_10("zeck_1-10.dat"),
    RESOURCES("resources.rar");

    private final String fileName;

    InputFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public static InputFileName getFileNameFromString(String otherName) {
        return Stream.of(values())
            .filter(inputFileName -> inputFileName.getFileName().equalsIgnoreCase(otherName))
            .findFirst()
            .orElseThrow();
    }
}
