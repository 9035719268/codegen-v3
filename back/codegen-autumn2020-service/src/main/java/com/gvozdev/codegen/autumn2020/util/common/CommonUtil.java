package com.gvozdev.codegen.autumn2020.util.common;

import org.springframework.stereotype.Component;

import static java.lang.Integer.parseInt;
import static java.lang.Math.abs;
import static java.lang.Math.round;

@Component
public class CommonUtil {

    public char toChar(int number) {
        return (char) (number + 48);
    }

    public boolean isDoubleDigit(int number) {
        return Integer.toString(abs(number)).trim().length() == 2;
    }

    public int getFirstDigit(int number) {
        return parseInt(Integer.toString(number).substring(0, 1));
    }

    public int getSecondDigit(int number) {
        return parseInt(Integer.toString(number).substring(1, 2));
    }

    public double roundUpToNumber(double number, double offset) {
        double rounded = round(number / offset) * offset;

        if (rounded < number) {
            return rounded + offset;
        }

        return rounded;
    }

    public double roundDownToNumber(double number, double offset) {
        double rounded = round(number / offset) * offset;

        if (rounded > number) {
            return rounded - offset;
        }

        return rounded;
    }

    public boolean isEven(int number) {
        return number % 2 == 0;
    }
}
