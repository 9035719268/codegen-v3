package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.StrictMath.*;
import static java.util.stream.IntStream.range;

public class KlobucharModelDelay {
    private final double latpp;
    private final double lonpp;
    private final double elevationAngle;
    private final double azimuth;
    private final double gpsTime;
    private final IonCoefficients alpha;
    private final IonCoefficients beta;

    public KlobucharModelDelay(double latpp, double lonpp, double gpsTime, IonCoefficients alpha, IonCoefficients beta) {
        double halfCircle = 180;
        this.latpp = latpp;
        this.lonpp = lonpp;
        this.gpsTime = gpsTime;
        this.alpha = alpha;
        this.beta = beta;
        elevationAngle = 90 / halfCircle;
        azimuth = 0;
    }

    public double getDelayInMeters() {
        double speedOfLight = 2.99792458 * 1E8;
        double delayInSeconds = getKlobucharDelayInSeconds();

        return delayInSeconds * speedOfLight;
    }

    private double getEarthCenteredAngle() {
        return 0.0137 / (elevationAngle + 0.11) - 0.022;
    }

    private double getIppLatitude() {
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = latpp + earthCenteredAngle * cos(azimuth);

        if (ippLatitude > 0.416) {
            return 0.416;
        } else if (ippLatitude < -0.416) {
            return -0.416;
        }

        return ippLatitude;
    }

    private double getIppLongtitude() {
        double earthCenteredAngle = getEarthCenteredAngle();
        double ippLatitude = getIppLatitude();

        return lonpp + (earthCenteredAngle * sin(azimuth) / (cos(ippLatitude)));
    }

    private double getIppGeomagneticLatitude() {
        double ippLatitude = getIppLatitude();
        double ippLongtitude = getIppLongtitude();

        return ippLatitude + 0.064 * cos(ippLongtitude - 1.617);
    }

    private double getIppLocalTime() {
        double secondsInOneDay = 86_400;
        double secondsInTwelveHours = 43_200;
        double ippLongtitude = getIppLongtitude();
        double ippLocalTime = secondsInTwelveHours * ippLongtitude + gpsTime;

        while (ippLocalTime > secondsInOneDay) {
            ippLocalTime -= secondsInOneDay;
        }

        while (ippLocalTime < 0) {
            ippLocalTime += secondsInOneDay;
        }

        return ippLocalTime;
    }

    private double getIonosphericDelayAmplitude() {
        double amplitude;
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        List<Double> alphaCoefficients = alpha.getIonCoefficients();

        amplitude = range(0, 4)
            .mapToDouble(number -> alphaCoefficients.get(number) * pow(ippGeomagneticLatitude, number))
            .sum();

        if (amplitude < 0) {
            return 0;
        }

        return amplitude;
    }

    private double getIonosphericDelayPeriod() {
        double ippGeomagneticLatitude = getIppGeomagneticLatitude();
        List<Double> betaCoefficients = beta.getIonCoefficients();

        double period = range(0, 4)
            .mapToDouble(number -> betaCoefficients.get(number) * pow(ippGeomagneticLatitude, number))
            .sum();

        if (period < 72_000) {
            return 72_000;
        }

        return period;
    }

    private double getIonosphericDelayPhase() {
        double ippLocalTime = getIppLocalTime();
        double ionosphericDelayPeriod = getIonosphericDelayPeriod();
        return 2 * PI * (ippLocalTime - 50_400) / ionosphericDelayPeriod;
    }

    private double getSlantFactor() {
        return 1 + 16 * pow((0.53 - elevationAngle), 3);
    }

    private double getKlobucharDelayInSeconds() {
        double ionosphericDelayPhase = getIonosphericDelayPhase();
        double ionosphericDelayAmplitude = getIonosphericDelayAmplitude();
        double slantFactor = getSlantFactor();

        if (abs(ionosphericDelayPhase) > 1.57) {
            return 5E-9 * slantFactor;
        } else {
            return (5E-9 + ionosphericDelayAmplitude * (1 - pow(ionosphericDelayPhase, 2) / 2 + pow(ionosphericDelayPhase, 4) / 24)) * slantFactor;
        }
    }
}