package com.gvozdev.codegen.autumn2020.util.var3.interpolation;

public class InterpolationCoordinates {
    private final String smallerLatitude;
    private final String biggerLatitude;
    private final String smallerLongitude;
    private final String biggerLongitude;

    public InterpolationCoordinates(String smallerLatitude, String biggerLatitude, String smallerLongitude, String biggerLongitude) {
        this.smallerLatitude = smallerLatitude;
        this.biggerLatitude = biggerLatitude;
        this.smallerLongitude = smallerLongitude;
        this.biggerLongitude = biggerLongitude;
    }

    public String getSmallerLatitude() {
        return smallerLatitude;
    }

    public String getBiggerLatitude() {
        return biggerLatitude;
    }

    public String getSmallerLongitude() {
        return smallerLongitude;
    }

    public String getBiggerLongitude() {
        return biggerLongitude;
    }
}
