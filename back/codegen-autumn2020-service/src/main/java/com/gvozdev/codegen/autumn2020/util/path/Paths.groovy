package com.gvozdev.codegen.autumn2020.util.path

import static com.gvozdev.codegen.autumn2020.util.config.PortConfig.TEST_SERVER_PORT

class Paths {
	public static final String VAR1_EXERCISE_INFO_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var1/exercise-info?inputfilename={inputFileName}"

	public static final String VAR1_DOWNLOAD_LISTING_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var1/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}"

	public static final String VAR1_DOWNLOAD_CHARTS_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var1/download/charts/{inputFileName}"

	public static final String VAR2_EXERCISE_INFO_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var2/exercise-info?inputfilename={inputFileName}"

	public static final String VAR2_DOWNLOAD_LISTING_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var2/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}"

	public static final String VAR2_DOWNLOAD_CHARTS_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var2/download/charts/{inputFileName}"

	public static final String VAR3_EXERCISE_INFO_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var3/exercise-info?lat={latitude}&lon={longitude}&satnum={satelliteNumber}"

	public static final String VAR3_DOWNLOAD_LISTING_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var3/download/listing/{lang}/{file}/{oop}/{outputFileName}?lat={latitude}&lon={longitude}&satnum={satelliteNumber}"

	public static final String VAR3_DOWNLOAD_CHARTS_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/var3/download/charts?lat={latitude}&lon={longitude}&satnum={satelliteNumber}"

	public static final String DOWNLOAD_RESOURCES_PATH = "http://localhost:$TEST_SERVER_PORT/api/tasks/year/2020/autumn/download/resources"

	public static final String SWAGGER_UI_PATH = "http://localhost:$TEST_SERVER_PORT/api/year/2020/autumn/swagger-ui"

	public static final String ACTUATOR_PATH = "http://localhost:$TEST_SERVER_PORT/api/year/2020/autumn/actuator"
}