package com.gvozdev.codegen.autumn2020.domain.var1;

import java.util.List;

import static java.lang.StrictMath.pow;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class IonosphericDelays {
    private final Satellite satellite;
    private final int amountOfObservations;

    public IonosphericDelays(Satellite satellite, int amountOfObservations) {
        this.satellite = satellite;
        this.amountOfObservations = amountOfObservations;
    }

    public List<Double> getIonosphericDelays() {
        double speedOfLight = 2.99792458 * 1E8;
        double k = getK();

        List<Double> pseudoRanges1 = satellite.getP1();
        List<Double> pseudoRanges2 = satellite.getP2();

        return range(0, amountOfObservations)
            .mapToObj(observation -> {
                double p1 = pseudoRanges1.get(observation);
                double p2 = pseudoRanges2.get(observation);
                double delay = (p1 - p2) / (speedOfLight * (1 - k));

                return delay * speedOfLight;
            })
            .collect(toList());
    }

    private static double getK() {
        double f1 = 1_575_420_000;
        double f2 = 1_227_600_000;

        return pow(f1, 2) / pow(f2, 2);
    }
}
