package com.gvozdev.codegen.autumn2020.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity(name = "InputFile")
@Table(name = "input_file")
public class InputFile implements Serializable {
    private static final long serialVersionUID = 2912358133554059591L;

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "file_bytes", nullable = false)
    private byte[] fileBytes;

    public InputFile() {
    }

    public InputFile(Long id, String name, byte[] fileBytes) {
        this.id = id;
        this.name = name;
        this.fileBytes = fileBytes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }
}
