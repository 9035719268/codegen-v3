package com.gvozdev.codegen.autumn2020.domain.var3;

import java.util.List;

import static java.lang.Double.parseDouble;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class KlobucharDelaysFactory {
    private final double latitude;
    private final double longitude;
    private final GpsTime gpsTime;
    private final IonCoefficients alpha;
    private final IonCoefficients beta;
    private final int amountOfObservations;

    public KlobucharDelaysFactory(String latitude, String longitude, GpsTime gpsTime, IonCoefficients alpha, IonCoefficients beta, int amountOfObservations) {
        this.latitude = parseDouble(latitude);
        this.longitude = parseDouble(longitude);
        this.gpsTime = gpsTime;
        this.alpha = alpha;
        this.beta = beta;
        this.amountOfObservations = amountOfObservations;
    }

    public List<KlobucharModelDelay> getKlobucharModels() {
        List<Double> gpsTimeArray = gpsTime.getGpsTime();

        return range(0, amountOfObservations)
            .mapToObj(observation -> {
                double gpsTimeValue = gpsTimeArray.get(observation);
                return new KlobucharModelDelay(latitude, longitude, gpsTimeValue, alpha, beta);
            })
            .collect(toList());
    }
}