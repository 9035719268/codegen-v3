package com.gvozdev.codegen.autumn2020.util.var3.filereader;

import com.gvozdev.codegen.autumn2020.util.number.ThreeDigitNumber;

import java.util.List;

import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public class IonoFileReader {
    private final List<List<List<Byte>>> allLines;
    private final IonoFileReaderUtil ionoFileReaderUtil;

    public IonoFileReader(byte[] fileBytes, IonoFileReaderUtil ionoFileReaderUtil) {
        allLines = ionoFileReaderUtil.extractLinesOfData(fileBytes);
        this.ionoFileReaderUtil = ionoFileReaderUtil;
    }

    public List<Integer> getTec(ThreeDigitNumber requiredLatitude, String requiredLongitude, int firstLine) {
        List<List<List<Integer>>> allTecForLatitude = range(firstLine, allLines.size())
            .filter(lineNumber -> ionoFileReaderUtil.isLatitudeValid(allLines, lineNumber))
            .filter(lineNumber -> ionoFileReaderUtil.areLatitudesEqual(allLines, lineNumber, requiredLatitude))
            .mapToObj(lineNumber -> ionoFileReaderUtil.getTecPerLatitude(allLines, lineNumber))
            .collect(toList());

        int longitude = parseInt(requiredLongitude);

        return ionoFileReaderUtil.getTecArray(longitude, allTecForLatitude);
    }
}
