package com.gvozdev.codegen.autumn2020.util.var2.chart;

import com.gvozdev.codegen.autumn2020.domain.var2.Satellite;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeriesCollection;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static java.awt.Color.*;
import static java.awt.Font.BOLD;
import static javax.imageio.ImageIO.write;

public abstract class TemplateChartDrawer {
    private static final Font AXIS_LABEL_FONT = new Font("Serif", BOLD, 20);
    private static final Font AXIS_TICK_FONT = new Font("Serif", Font.PLAIN, 15);

    protected static final Font LEGEND_FONT = new Font("Serif", BOLD, 25);

    protected final Satellite satellite1;
    protected final Satellite satellite2;
    protected final Satellite satellite3;
    protected final int amountOfObservations;

    protected TemplateChartDrawer(Satellite satellite1, Satellite satellite2, Satellite satellite3, int amountOfObservations) {
        this.satellite1 = satellite1;
        this.satellite2 = satellite2;
        this.satellite3 = satellite3;
        this.amountOfObservations = amountOfObservations;
    }

    public byte[] getChartInBytes() {
        XYSeriesCollection observationsSeriesCollection = getMeasurementsSeriesCollection();

        JFreeChart measurementsChart = createChart(observationsSeriesCollection);

        configureChartLines(measurementsChart);
        configureLegend(measurementsChart);
        configureAxes(measurementsChart);
        configureAxesRanges(measurementsChart);

        BufferedImage bufferedImage = measurementsChart.createBufferedImage(2000, 1000);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        try {
            write(bufferedImage, "png", byteArrayOutputStream);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return byteArrayOutputStream.toByteArray();
    }

    abstract XYSeriesCollection getMeasurementsSeriesCollection();

    private static void configureChartLines(JFreeChart measurementsChart) {
        int satellite1Index = 0;
        int satellite2Index = 1;
        int satellite3Index = 2;

        XYPlot plot = measurementsChart.getXYPlot();
        XYItemRenderer plotRenderer = plot.getRenderer();

        plotRenderer.setSeriesPaint(satellite1Index, BLUE);
        plotRenderer.setSeriesStroke(satellite1Index, new BasicStroke(2f));

        plotRenderer.setSeriesPaint(satellite2Index, YELLOW);
        plotRenderer.setSeriesStroke(satellite2Index, new BasicStroke(2f));

        plotRenderer.setSeriesPaint(satellite3Index, RED);
        plotRenderer.setSeriesStroke(satellite3Index, new BasicStroke(2f));
    }

    private static void configureAxes(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        plot.getDomainAxis().setLabelFont(AXIS_LABEL_FONT);
        plot.getDomainAxis().setTickLabelFont(AXIS_TICK_FONT);
        plot.getRangeAxis().setLabelFont(AXIS_LABEL_FONT);
        plot.getRangeAxis().setTickLabelFont(AXIS_TICK_FONT);
    }

    private static void configureAxesRanges(JFreeChart measurementsChart) {
        XYPlot plot = measurementsChart.getXYPlot();

        NumberAxis range = (NumberAxis) plot.getDomainAxis();
        range.setTickUnit(new NumberTickUnit(50));
    }

    abstract JFreeChart createChart(XYSeriesCollection observationsSeriesCollection);

    abstract void configureLegend(JFreeChart measurementsChart);
}