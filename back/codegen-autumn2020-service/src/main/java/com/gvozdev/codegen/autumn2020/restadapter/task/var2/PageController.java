package com.gvozdev.codegen.autumn2020.restadapter.task.var2;

import com.gvozdev.codegen.autumn2020.domain.var2.Satellite;
import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileService;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import com.gvozdev.codegen.autumn2020.util.common.CommonUtil;
import com.gvozdev.codegen.autumn2020.util.index.ComponentIndexes;
import com.gvozdev.codegen.autumn2020.util.queryparam.file.File;
import com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName;
import com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang;
import com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop;
import com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.codegen.autumn2020.util.queryparam.var.Var;
import com.gvozdev.codegen.autumn2020.util.var2.chart.*;
import com.gvozdev.codegen.autumn2020.util.var2.controller.ControllerUtil;
import com.gvozdev.codegen.autumn2020.util.var2.filereader.FileReader;
import com.gvozdev.codegen.autumn2020.util.var2.filereader.FileReaderUtil;
import com.gvozdev.codegen.autumn2020.util.var2.fileservice.FileService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayOutputStream;
import java.util.List;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.FILE;
import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.MANUAL;
import static com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName.CHARTS;
import static com.gvozdev.codegen.autumn2020.util.queryparam.var.Var.VAR2;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.ContentDisposition.attachment;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.http.MediaType.APPLICATION_PDF;

@Controller("Var2Controller")
@RequestMapping("/api/tasks/year/2020/autumn/var2")
@CrossOrigin("http://localhost:3000")
public class PageController {
    private static final Var VAR = VAR2;

    private final InputFileService inputFileService;
    private final OutputFileService outputFileService;
    private final ComponentIndexes componentIndexes;
    private final ControllerUtil controllerUtil;
    private final FileReaderUtil fileReaderUtil;
    private final CommonUtil commonUtil;

    public PageController(
        InputFileService inputFileService, OutputFileService outputFileService, ComponentIndexes componentIndexes,
        ControllerUtil controllerUtil, FileReaderUtil fileReaderUtil, CommonUtil commonUtil
    ) {
        this.inputFileService = inputFileService;
        this.outputFileService = outputFileService;
        this.componentIndexes = componentIndexes;
        this.controllerUtil = controllerUtil;
        this.fileReaderUtil = fileReaderUtil;
        this.commonUtil = commonUtil;
    }

    @GetMapping("/exercise-info")
    public ResponseEntity<List<Satellite>> getSatellites(@RequestParam("inputfilename") InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        return new ResponseEntity<>(satellites, OK);
    }

    @GetMapping("/download/listing/{lang}/{file}/{oop}/{inputFileName}/{outputFileName}")
    public ResponseEntity<byte[]> downloadListing(
        @PathVariable("lang") Lang lang,
        @PathVariable("file") File file,
        @PathVariable("oop") Oop oop,
        @PathVariable("inputFileName") InputFileName inputFileName,
        @PathVariable("outputFileName") OutputFileName outputFileName
    ) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), lang.toString(), file.toString(), oop.toString(), outputFileName.getFileName()
        );

        byte[] inputFileBytes = inputFile.getFileBytes();
        byte[] outputFileBytes = outputFile.getFileBytes();

        String listing;

        if (file.equals(FILE)) {
            listing = controllerUtil.getFilledListingForFile(inputFileBytes, outputFileBytes);
        } else if (file.equals(MANUAL)) {
            listing = controllerUtil.getFilledListingForManual(inputFileBytes, outputFileBytes, lang);
        } else {
            return new ResponseEntity<>(BAD_REQUEST);
        }

        byte[] listingBytes = listing.getBytes(UTF_8);

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_PDF);
        responseHeaders.setContentDisposition(attachment().filename(outputFileName.getFileName()).build());

        return new ResponseEntity<>(listingBytes, responseHeaders, OK);
    }

    @GetMapping("/download/charts/{inputFileName}")
    public ResponseEntity<byte[]> downloadCharts(@PathVariable("inputFileName") InputFileName inputFileName) {
        InputFile inputFile = inputFileService.findByName(inputFileName.getFileName());

        byte[] inputFileBytes = inputFile.getFileBytes();

        FileReader fileReader = new FileReader(inputFileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);
        FileService fileService = new FileService(fileReader, commonUtil);

        List<Satellite> satellites = fileService.getSatellites();

        Satellite satellite1 = satellites.get(0);
        Satellite satellite2 = satellites.get(1);
        Satellite satellite3 = satellites.get(2);

        TemplateChartDrawer angularVelocitiesDrawer = new AngularVelocitiesChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] angularVelocitiesChartInBytes = angularVelocitiesDrawer.getChartInBytes();

        TemplateChartDrawer averageAngularVelocitiesDrawer =
            new AverageAngularVelocitiesChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] averageAngularVelocitiesChartInBytes = averageAngularVelocitiesDrawer.getChartInBytes();

        TemplateChartDrawer linearVelocitiesDrawer = new LinearVelocitiesChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] linearVelocitiesChartInBytes = linearVelocitiesDrawer.getChartInBytes();

        TemplateChartDrawer averageLinearVelocitiesDrawer = new AverageLinearVelocitiesChartDrawer(satellite1, satellite2, satellite3, AMOUNT_OF_OBSERVATIONS);
        byte[] averageLinearVelocitiesChartInBytes = averageLinearVelocitiesDrawer.getChartInBytes();

        ByteArrayOutputStream byteArrayOutputStream = controllerUtil.getChartsArchiveInBytes(
            angularVelocitiesChartInBytes, averageAngularVelocitiesChartInBytes, linearVelocitiesChartInBytes, averageLinearVelocitiesChartInBytes
        );

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(APPLICATION_OCTET_STREAM);
        responseHeaders.setContentDisposition(attachment().filename(CHARTS.getFileName()).build());

        return new ResponseEntity<>(byteArrayOutputStream.toByteArray(), responseHeaders, OK);
    }
}