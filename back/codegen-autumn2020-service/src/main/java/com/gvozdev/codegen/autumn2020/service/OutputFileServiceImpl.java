package com.gvozdev.codegen.autumn2020.service;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.repo.OutputFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OutputFileServiceImpl implements OutputFileService {

    @Autowired
    private OutputFileRepository outputFileRepository;

    @Override
    public OutputFile findByParameters(String var, String lang, String file, String oop, String name) {
        return outputFileRepository.findFileByParameters(var, lang, file, oop, name);
    }
}
