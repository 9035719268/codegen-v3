package com.gvozdev.codegen.autumn2020.service;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.repo.InputFileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InputFileServiceImpl implements InputFileService {

    @Autowired
    private InputFileRepository inputFileRepository;

    @Override
    public InputFile findByName(String name) {
        return inputFileRepository.findByName(name);
    }
}
