package com.gvozdev.codegen.autumn2020.unittests;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import com.gvozdev.codegen.autumn2020.util.number.ThreeDigitNumber;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.IonoFileReader;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.IonoFileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.FORECAST_TEC_LIST_FIRST_LINE;
import static com.gvozdev.codegen.autumn2020.util.constant.Constants.PRECISE_TEC_LIST_FIRST_LINE;
import static com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName.IGRG;
import static com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName.IGSG;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class TecValuesTests {
    private static final String FORECAST_FILE_NAME = IGRG.getFileName();
    private static final String PRECISE_FILE_NAME = IGSG.getFileName();

    @Autowired
    private InputFileServiceImpl inputFileService;

    @Autowired
    private IonoFileReaderUtil ionoFileReaderUtil;

    @Test
    void shouldCheckTecValuesFromLatitude0Longitude0ForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('0', '.', '0');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(100, 71, 59, 61, 142, 238, 308, 311, 295, 258, 184, 177);
        List<Integer> actualTecValues = forecastFileReader.getTec(requiredLatitudeNumber, "0", FORECAST_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitude0Longitude0PreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('0', '.', '0');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(98, 73, 59, 59, 140, 238, 315, 323, 299, 257, 179, 174);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "0", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitude0LongitudePositiveForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('0', '.', '0');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(73, 60, 51, 130, 198, 280, 366, 332, 277, 193, 163, 122);
        List<Integer> actualTecValues = forecastFileReader.getTec(requiredLatitudeNumber, "20", FORECAST_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitude0LongitudePositivePreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('0', '.', '0');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(64, 50, 45, 123, 193, 282, 363, 329, 268, 186, 156, 117);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "20", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitude0LongitudeNegativeForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('0', '.', '0');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(193, 117, 96, 78, 68, 55, 51, 114, 188, 267, 336, 306);
        List<Integer> actualTecValues = forecastFileReader.getTec(
            requiredLatitudeNumber, "-110", FORECAST_TEC_LIST_FIRST_LINE
        );

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitude0LongitudeNegativePreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('0', '.', '0');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(215, 113, 90, 78, 74, 62, 49, 114, 201, 269, 322, 306);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "-110", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudePositiveLongitude0ForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('5', '0', '.');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(45, 39, 31, 27, 49, 88, 74, 78, 51, 38, 33, 39);
        List<Integer> actualTecValues = forecastFileReader.getTec(requiredLatitudeNumber, "0", FORECAST_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudePositiveLongitude0PreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('5', '0', '.');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(42, 39, 30, 28, 50, 87, 71, 80, 49, 34, 33, 37);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "0", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudePositiveLongitudePositiveForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('6', '0', '.');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(22, 27, 18, 23, 53, 74, 58, 42, 23, 17, 24, 17);
        List<Integer> actualTecValues = forecastFileReader.getTec(requiredLatitudeNumber, "35", FORECAST_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudePositiveLongitudePositivePreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('6', '0', '.');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(24, 28, 17, 21, 51, 74, 59, 41, 19, 14, 14, 14);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "35", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudePositiveLongitudeNegativeForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('8', '0', '.');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(29, 21, 20, 18, 16, 17, 23, 26, 20, 21, 19, 17);
        List<Integer> actualTecValues = forecastFileReader.getTec(requiredLatitudeNumber, "-75", FORECAST_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudePositiveLongitudeNegativePreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('8', '0', '.');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(28, 21, 17, 11, 12, 13, 25, 23, 19, 18, 21, 16);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "-75", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudeNegativeLongitude0ForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('-', '2', '0');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(84, 67, 54, 80, 144, 209, 262, 287, 225, 179, 122, 94);
        List<Integer> actualTecValues = forecastFileReader.getTec(requiredLatitudeNumber, "0", FORECAST_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudeNegativeLongitude0PreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('-', '2', '0');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(68, 57, 44, 73, 144, 204, 256, 296, 225, 175, 113, 84);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "0", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudeNegativeLongitudePositiveForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('-', '5', '.');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(56, 51, 104, 170, 232, 280, 325, 239, 166, 136, 140, 116);
        List<Integer> actualTecValues = forecastFileReader.getTec(requiredLatitudeNumber, "45", FORECAST_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudeNegativeLongitudePositivePreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('-', '5', '.');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(58, 52, 100, 175, 227, 281, 331, 258, 164, 134, 125, 110);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "45", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudeNegativeLongitudeNegativeForecastFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('-', '8', '5');
        InputFile inputFile = inputFileService.findByName(FORECAST_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader forecastFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(146, 122, 102, 93, 92, 76, 56, 57, 68, 84, 94, 109);
        List<Integer> actualTecValues = forecastFileReader.getTec(
            requiredLatitudeNumber, "-160", FORECAST_TEC_LIST_FIRST_LINE
        );

        assertEquals(expectedTecValues, actualTecValues);
    }

    @Test
    void shouldCheckTecValuesFromLatitudeNegativeLongitudeNegativePreciseFile() {
        ThreeDigitNumber requiredLatitudeNumber = new ThreeDigitNumber('-', '8', '5');
        InputFile inputFile = inputFileService.findByName(PRECISE_FILE_NAME);
        byte[] fileBytes = inputFile.getFileBytes();
        IonoFileReader preciseFileReader = new IonoFileReader(fileBytes, ionoFileReaderUtil);

        List<Integer> expectedTecValues = asList(137, 114, 102, 88, 87, 69, 51, 55, 65, 76, 86, 102);
        List<Integer> actualTecValues = preciseFileReader.getTec(requiredLatitudeNumber, "-160", PRECISE_TEC_LIST_FIRST_LINE);

        assertEquals(expectedTecValues, actualTecValues);
    }
}
