package com.gvozdev.codegen.autumn2020.filestests.outputfiles.var3.python.manual.withoop;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import com.gvozdev.codegen.autumn2020.util.queryparam.file.File;
import com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang;
import com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop;
import com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName;
import com.gvozdev.codegen.autumn2020.util.queryparam.var.Var;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.MANUAL;
import static com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang.PYTHON;
import static com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName.PYTHON_MAIN;
import static com.gvozdev.codegen.autumn2020.util.queryparam.var.Var.VAR3;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class FilesLengthTests {
    private static final Var VAR = VAR3;
    private static final Lang LANG = PYTHON;
    private static final File FILE = MANUAL;
    private static final Oop OOP = WITHOOP;
    private static final OutputFileName OUTPUT_FILE_NAME = PYTHON_MAIN;

    @Autowired
    private OutputFileService outputFileService;

    @Test
    void shouldCheckMainLengthInBytes() {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), LANG.toString(), FILE.toString(), OOP.toString(), OUTPUT_FILE_NAME.getFileName()
        );

        byte[] fileBytes = outputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(13626, bytesInFile);
    }
}