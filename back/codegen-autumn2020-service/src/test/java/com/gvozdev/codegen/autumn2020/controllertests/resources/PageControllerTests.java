package com.gvozdev.codegen.autumn2020.controllertests.resources;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static com.gvozdev.codegen.autumn2020.util.path.Paths.DOWNLOAD_RESOURCES_PATH;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class PageControllerTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void shouldCheckVar1DownloadResourcesController() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(DOWNLOAD_RESOURCES_PATH, String.class);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(14267904, responseBodyLength);
    }

    @Test
    void shouldCheckVar2DownloadResourcesController() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(DOWNLOAD_RESOURCES_PATH, String.class);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(14267904, responseBodyLength);
    }

    @Test
    void shouldCheckVar3DownloadResourcesController() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(DOWNLOAD_RESOURCES_PATH, String.class);

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(14267904, responseBodyLength);
    }
}
