package com.gvozdev.codegen.autumn2020.controllertests.tasks.var3;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static com.gvozdev.codegen.autumn2020.util.path.Paths.*;
import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.FILE;
import static com.gvozdev.codegen.autumn2020.util.queryparam.file.File.MANUAL;
import static com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang.*;
import static com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop.NOOOP;
import static com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName.*;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.countMatches;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class PageControllerTests {
    private static final String LATITUDE = "55.0";
    private static final String LONGITUDE = "162";
    private static final String SATELLITE_NUMBER = "7";

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void shouldCheckExerciseInfo() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_EXERCISE_INFO_PATH,
            String.class,
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );
        String responseBody = response.getBody();

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();
        int gpsTimeListsAmount = countMatches(responseBody, "gpsTime");
        int alphaListsAmount = countMatches(responseBody, "alpha");
        int betaListsAmount = countMatches(responseBody, "beta");
        int forecastListsAmount = countMatches(responseBody, "forecast");
        int preciseListsAmount = countMatches(responseBody, "precise");

        assertEquals(200, responseStatusCode);
        assertEquals(790, responseBodyLength);
        assertEquals(2, gpsTimeListsAmount);
        assertEquals(1, alphaListsAmount);
        assertEquals(1, betaListsAmount);
        assertEquals(1, forecastListsAmount);
        assertEquals(1, preciseListsAmount);
    }

    @Test
    void shouldCheckExerciseInfoWrongLatitudeValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_EXERCISE_INFO_PATH,
            String.class,
            "wrong_latitude_value", LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckExerciseInfoWrongLongitudeValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_EXERCISE_INFO_PATH,
            String.class,
            LATITUDE, "wrong_longitude_value", SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckExerciseInfoWrongSatelliteNumberValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_EXERCISE_INFO_PATH,
            String.class,
            LATITUDE, LONGITUDE, "wrong_satellite_number"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingCppFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, WITHOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(26823, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingCppFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, NOOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(19475, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingCppManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, WITHOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(14069, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingCppManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, NOOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(9196, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueCppNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, NOOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, "wrong_file_value", WITHOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueCppNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, "wrong_file_value", NOOOP, CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppFile() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, "wrong_oop_value", CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueCppManual() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, "wrong_oop_value", CPP_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, WITHOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, FILE, NOOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, WITHOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameCppManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            CPP, MANUAL, NOOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, JAVA_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(27402, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingJavaFileWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, JAVA_GRAPH_DRAWER.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(5139, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, JAVA_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(13233, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingJavaManualWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, JAVA_GRAPH_DRAWER.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(4465, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, JAVA_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValueJavaWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, JAVA_GRAPH_DRAWER.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, "wrong_file_value", WITHOOP, JAVA_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValueJavaWithoopGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, "wrong_file_value", WITHOOP, JAVA_GRAPH_DRAWER.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, "wrong_oop_value", JAVA_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaFileGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, "wrong_oop_value", JAVA_GRAPH_DRAWER.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualMain() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, "wrong_oop_value", JAVA_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValueJavaManualGraphDrawer() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, "wrong_oop_value", JAVA_GRAPH_DRAWER.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, FILE, WITHOOP, "wrong_output_file_name", LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNameJavaManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            JAVA, MANUAL, WITHOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingPythonFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(26317, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingPythonFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, NOOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(20265, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingPythonManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, WITHOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(14064, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingPythonManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, NOOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(10277, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, WITHOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLangValuePythonNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            "wrong_lang_value", FILE, NOOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, "wrong_file_value", WITHOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongFileValuePythonNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, "wrong_file_value", NOOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonFile() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, "wrong_oop_value", PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOopValuePythonManual() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, "wrong_oop_value", PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonFileNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, NOOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualWithoop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, WITHOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongOutputFileNamePythonManualNooop() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, MANUAL, NOOOP, "wrong_output_file_name",
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLatitudeValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName(), "wrong_latitude_value",
            LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongLongitudeValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, "wrong_longitude_value", SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadListingWrongSatelliteNumberValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_LISTING_PATH,
            String.class,
            PYTHON, FILE, WITHOOP, PYTHON_MAIN.getFileName(),
            LATITUDE, LONGITUDE, "wrong_satellite_number_value"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadCharts() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_CHARTS_PATH,
            String.class,
            LATITUDE, LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();
        int responseBodyLength = requireNonNull(response.getBody()).length();

        assertEquals(200, responseStatusCode);
        assertEquals(71612, responseBodyLength);
    }

    @Test
    void shouldCheckDownloadChartsWrongLatitudeValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_CHARTS_PATH,
            String.class,
            "wrong_latitude_value", LONGITUDE, SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadChartsWrongLongitudeValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_CHARTS_PATH,
            String.class,
            LATITUDE, "wrong_longitude_value", SATELLITE_NUMBER
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }

    @Test
    void shouldCheckDownloadChartsWrongSatelliteNumberValue() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(
            VAR3_DOWNLOAD_CHARTS_PATH,
            String.class,
            LATITUDE, LONGITUDE, "wrong_satellite_number"
        );

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(400, responseStatusCode);
    }
}
