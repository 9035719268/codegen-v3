package com.gvozdev.codegen.autumn2020.filestests.inputfiles;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class FilesLengthTests {

    @Autowired
    private InputFileServiceImpl inputFileService;

    @Test
    void shouldCheckBogi6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(BOGI_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(975441, bytesInFile);
    }

    @Test
    void shouldCheckBrdc0010LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(BRDC.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(398728, bytesInFile);
    }

    @Test
    void shouldCheckBshm10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(BSHM_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2727464, bytesInFile);
    }

    @Test
    void shouldCheckHueg6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(HUEG_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(1640852, bytesInFile);
    }

    @Test
    void shouldCheckHueg10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(HUEG_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2746623, bytesInFile);
    }

    @Test
    void shouldCheckIgrg0010LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(IGRG.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(862947, bytesInFile);
    }

    @Test
    void shouldCheckIgsg0010LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(IGSG.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(870643, bytesInFile);
    }

    @Test
    void shouldCheckLeij10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(LEIJ_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2819423, bytesInFile);
    }

    @Test
    void shouldCheckOnsa6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(ONSA_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(1682271, bytesInFile);
    }

    @Test
    void shouldCheckOnsa10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(ONSA_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2958038, bytesInFile);
    }

    @Test
    void shouldCheckPots6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(POTS_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(1690371, bytesInFile);
    }

    @Test
    void shouldCheckSpt10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(SPT_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(3253827, bytesInFile);
    }

    @Test
    void shouldCheckSvtl10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(SVTL_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(3026325, bytesInFile);
    }

    @Test
    void shouldCheckTit10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(TIT_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2807553, bytesInFile);
    }

    @Test
    void shouldCheckTit6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(TIT_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(1589836, bytesInFile);
    }

    @Test
    void shouldCheckVis10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(VIS_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2869674, bytesInFile);
    }

    @Test
    void shouldCheckWarn6LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(WARN_6.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(1654061, bytesInFile);
    }

    @Test
    void shouldCheckWarn10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(WARN_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2868401, bytesInFile);
    }

    @Test
    void shouldCheckZeck10LengthInBytes() {
        InputFile inputFile = inputFileService.findByName(ZECK_10.getFileName());

        byte[] fileBytes = inputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(2802665, bytesInFile);
    }
}
