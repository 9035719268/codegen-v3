package com.gvozdev.codegen.autumn2020.controllertests.actuator;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static com.gvozdev.codegen.autumn2020.util.path.Paths.ACTUATOR_PATH;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@TestPropertySource("classpath:test-application.properties")
class PageControllerTests {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void shouldCheckActuator() {
        ResponseEntity<String> response = testRestTemplate.getForEntity(ACTUATOR_PATH, String.class);

        int responseStatusCode = response.getStatusCodeValue();

        assertEquals(200, responseStatusCode);
    }
}
