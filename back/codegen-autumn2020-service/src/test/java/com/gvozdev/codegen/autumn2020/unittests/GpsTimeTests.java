package com.gvozdev.codegen.autumn2020.unittests;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import com.gvozdev.codegen.autumn2020.util.number.OneDigitNumber;
import com.gvozdev.codegen.autumn2020.util.number.TwoDigitNumber;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.EphemerisFileReader;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.EphemerisFileReaderUtil;
import com.gvozdev.codegen.autumn2020.util.var3.filereader.IonoFileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.START_OF_OBSERVATIONS_LINE_NUMBER;
import static com.gvozdev.codegen.autumn2020.util.constant.Constants.VAR3_AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName.BRDC;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class GpsTimeTests {

    @Autowired
    private InputFileServiceImpl inputFileService;

    @Autowired
    private EphemerisFileReaderUtil ephemerisFileReaderUtil;

    @Autowired
    private IonoFileReaderUtil ionoFileReaderUtil;

    @Test
    void shouldCheckSingleDigitNumberSatellite() {
        OneDigitNumber requiredSatelliteNumber = new OneDigitNumber('7');
        InputFile inputFile = inputFileService.findByName(BRDC.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            fileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        List<Double> expectedGpsTime = asList(80496.0, 90270.0, 93630.0, 100830.0, 108030.0, 0.0, 124230.0, 129630.0, 136800.0, 144000.0, 151200.0, 158400.0);
        List<Double> actualGpsTime = ephemerisFileReader.getGpsTime(requiredSatelliteNumber);

        assertEquals(expectedGpsTime, actualGpsTime);
    }

    @Test
    void shouldCheckSoubleDigitSatelliteNumber() {
        TwoDigitNumber requiredSatelliteNumber = new TwoDigitNumber('1', '4');
        InputFile inputFile = inputFileService.findByName(BRDC.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        EphemerisFileReader ephemerisFileReader = new EphemerisFileReader(
            fileBytes, ephemerisFileReaderUtil, ionoFileReaderUtil, START_OF_OBSERVATIONS_LINE_NUMBER, VAR3_AMOUNT_OF_OBSERVATIONS
        );

        List<Double> expectedGpsTime = asList(86400.0, 86430.0, 93600.0, 100800.0, 108000.0, 0.0, 122400.0, 129600.0, 143460.0, 146160.0, 151230.0, 158430.0);
        List<Double> actualGpsTime = ephemerisFileReader.getGpsTime(requiredSatelliteNumber);

        assertEquals(expectedGpsTime, actualGpsTime);
    }
}
