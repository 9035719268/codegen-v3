package com.gvozdev.codegen.autumn2020.unittests;

import com.gvozdev.codegen.autumn2020.entity.InputFile;
import com.gvozdev.codegen.autumn2020.service.InputFileServiceImpl;
import com.gvozdev.codegen.autumn2020.util.index.ComponentIndexes;
import com.gvozdev.codegen.autumn2020.util.var1.filereader.FileReader;
import com.gvozdev.codegen.autumn2020.util.var1.filereader.FileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static com.gvozdev.codegen.autumn2020.util.constant.Constants.AMOUNT_OF_OBSERVATIONS;
import static com.gvozdev.codegen.autumn2020.util.queryparam.inputfilename.InputFileName.BSHM_10;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SatelliteNumbersTests {

    @Autowired
    private InputFileServiceImpl inputFileService;

    @Autowired
    private ComponentIndexes componentIndexes;

    @Autowired
    private FileReaderUtil fileReaderUtil;

    @Test
    void shouldCheckSatelliteNumbers() {
        InputFile inputFile = inputFileService.findByName(BSHM_10.getFileName());
        byte[] fileBytes = inputFile.getFileBytes();
        FileReader fileReader = new FileReader(fileBytes, componentIndexes, fileReaderUtil, AMOUNT_OF_OBSERVATIONS);

        List<Integer> expectedSatelliteNumbers = asList(2, 5, 6, 12, 15, 16, 18, 20, 21, 24, 25, 26, 29, 31);
        List<Integer> actualSatelliteNumbers = fileReader.getSatelliteNumbers();

        assertEquals(expectedSatelliteNumbers, actualSatelliteNumbers);
    }
}