package com.gvozdev.codegen.autumn2020.filestests.outputfiles.var3.java.file.withoop;

import com.gvozdev.codegen.autumn2020.entity.OutputFile;
import com.gvozdev.codegen.autumn2020.service.OutputFileService;
import com.gvozdev.codegen.autumn2020.util.queryparam.file.File;
import com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang;
import com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop;
import com.gvozdev.codegen.autumn2020.util.queryparam.var.Var;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.gvozdev.codegen.autumn2020.util.queryparam.lang.Lang.JAVA;
import static com.gvozdev.codegen.autumn2020.util.queryparam.oop.Oop.WITHOOP;
import static com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName.JAVA_GRAPH_DRAWER;
import static com.gvozdev.codegen.autumn2020.util.queryparam.outputfilename.OutputFileName.JAVA_MAIN;
import static com.gvozdev.codegen.autumn2020.util.queryparam.var.Var.VAR3;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class FilesLengthTests {
    private static final Var VAR = VAR3;
    private static final Lang LANG = JAVA;
    private static final File FILE = File.FILE;
    private static final Oop OOP = WITHOOP;

    @Autowired
    private OutputFileService outputFileService;

    @Test
    void shouldCheckMainLengthInBytes() {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), LANG.toString(), FILE.toString(), OOP.toString(), JAVA_MAIN.getFileName()
        );

        byte[] fileBytes = outputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(27398, bytesInFile);
    }

    @Test
    void shouldCheckGraphDrawerLengthInBytes() {
        OutputFile outputFile = outputFileService.findByParameters(
            VAR.toString(), LANG.toString(), FILE.toString(), OOP.toString(), JAVA_GRAPH_DRAWER.getFileName()
        );

        byte[] fileBytes = outputFile.getFileBytes();
        int bytesInFile = fileBytes.length;

        assertEquals(5131, bytesInFile);
    }
}