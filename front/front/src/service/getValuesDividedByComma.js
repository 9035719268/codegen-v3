const getValuesDividedByComma = oldValues => {
  const isValueLast = (value, values) => values.indexOf(value) === values.length - 1;

  let dividedValues = null;

  if (typeof oldValues !== "undefined") {
    return oldValues.map(value => {
      if (isValueLast(value, oldValues)) {
        return value;
      } else {
        return value + ", ";
      }
    });
  }

  return dividedValues;
}

export default getValuesDividedByComma;