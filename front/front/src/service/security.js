import axios from "axios";
import jwtDecode from "jwt-decode";
import {loginPath, refreshTokenPath} from "../config/paths/security";

export const login = (userName, password) => {
  const url = loginPath;

  const params = new URLSearchParams();
  params.append("username", userName);
  params.append("password", password);

  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };

  return axios.post(url, params, config);
}

export const logout = () => {
  localStorage.setItem("access_token", null);
  localStorage.setItem("refresh_token", null);
}

export const getNewAccessToken = () => {
  const refreshToken = localStorage.getItem("refresh_token");

  return axios.get(
    refreshTokenPath,
    {
      headers: {
        "Authorization": refreshToken
      }
    }
  );
}

export const checkUserAccess = rolesAllowed => {
  if (rolesAllowed === ALL_USERS) {
    return true;
  } else {
    const currentUserRoles = getCurrentUserRoles();

    return rolesAllowed.some(role => currentUserRoles.includes(role));
  }
}

export const getCurrentUser = () => {
  const decodedJWT = getDecodedJWT();

  return decodedJWT.sub;
}

export const getCurrentUserRoles = () => {
  const decodedJWT = getDecodedJWT();

  return decodedJWT.roles;
}

export const isTokenExpired = () => {
  const decodedJWT = getDecodedJWT();
  const tokenExpirationTime = decodedJWT.exp;
  const timeNow = Math.floor(Date.now() / 1000);

  return timeNow > tokenExpirationTime;
}

export const getDecodedJWT = () => {
  const accessTokenWithPrefix = localStorage.getItem("access_token");
  const accessToken = accessTokenWithPrefix.substring("Bearer ".length);

  return jwtDecode(accessToken);
}

export const ALL_USERS = ["ROLE_ADMIN", "ROLE_A20201", "ROLE_A20202", "ROLE_A20203", "ROLE_S2021"];

export const AUTUMN_2020_USERS = ["ROLE_ADMIN", "ROLE_A20201", "ROLE_A20202", "ROLE_A20203"];

export const AUTUMN_2020_VAR1_USERS = ["ROLE_ADMIN", "ROLE_A20201"];

export const AUTUMN_2020_VAR2_USERS = ["ROLE_ADMIN", "ROLE_A20202"];

export const AUTUMN_2020_VAR3_USERS = ["ROLE_ADMIN", "ROLE_A20203"];

export const SPRING_2021_USERS = ["ROLE_ADMIN", "ROLE_S2021"];