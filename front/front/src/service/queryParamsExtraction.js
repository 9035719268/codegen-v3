export const extractVarNumber = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("var");
}

export const extractLang = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("lang");
}

export const extractFile = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("file") === "true" ? "file" : "manual";
}

export const extractOop = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("oop") === "true" ? "withoop" : "nooop";
}

export const extractInputFileName = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("inputfilename");
}

export const extractLat = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("lat");
}

export const extractLon = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("lon");
}

export const extractSatelliteNumber = routerSearch => {
  const searchParams = new URLSearchParams(routerSearch);
  return searchParams.get("satnum");
}