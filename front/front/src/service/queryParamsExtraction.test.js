import {
  extractVarNumber, extractLang, extractFile, extractOop, extractInputFileName, extractLat, extractLon, extractSatelliteNumber
} from "./queryParamsExtraction"

it("shouldGetVarNumber", () => {
  const varNumber = extractVarNumber("?var=3");

  expect(varNumber).toBe("3");
});

it("shouldGetLang", () => {
  const lang = extractLang("?lang=cpp");

  expect(lang).toBe("cpp");
});

it("shouldGetFile", () => {
  const file = extractFile("?file=true");

  expect(file).toBe("file");
});

it("shouldGetManual", () => {
  const file = extractFile("?file=false");

  expect(file).toBe("manual");
});

it("shouldGetWithOop", () => {
  const oop = extractOop("?oop=true");

  expect(oop).toBe("withoop");
});

it("shouldGetNoOop", () => {
  const oop = extractOop("?oop=false");

  expect(oop).toBe("nooop");
});

it("shouldGetInputFileName", () => {
  const inputFileName = extractInputFileName("?inputfilename=POTS_6hours.dat");

  expect(inputFileName).toBe("POTS_6hours.dat");
});

it("shouldGetLat", () => {
  const lat = extractLat("?lat=145");

  expect(lat).toBe("145");
});

it("shouldGetLon", () => {
  const lon = extractLon("?lon=60");

  expect(lon).toBe("60");
});

it("shouldGetSatelliteNumber", () => {
  const satelliteNumber = extractSatelliteNumber("?satnum=7");

  expect(satelliteNumber).toBe("7");
});

it("shouldGetAllParams", () => {
  const routerSearch = "?var=3&lang=python&file=false&oop=true&inputfilename=POTS_6hours.dat&lat=130&lon=50&satnum=5";

  const varNumber = extractVarNumber(routerSearch);
  const lang = extractLang(routerSearch);
  const file = extractFile(routerSearch);
  const oop = extractOop(routerSearch);
  const inputFileName = extractInputFileName(routerSearch);
  const lat = extractLat(routerSearch);
  const lon = extractLon(routerSearch);
  const satelliteNumber = extractSatelliteNumber(routerSearch);

  expect(varNumber).toBe("3");
  expect(lang).toBe("python");
  expect(file).toBe("manual");
  expect(oop).toBe("withoop")
  expect(inputFileName).toBe("POTS_6hours.dat");
  expect(lat).toBe("130");
  expect(lon).toBe("50");
  expect(satelliteNumber).toBe("5");
})