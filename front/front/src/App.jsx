import {Switch} from "react-router-dom";
import LoginForm from "./auth/LoginForm";
import Logout from "./auth/Logout";
import AuthWrapper from "./auth/AuthWrapper";
import {ALL_USERS, AUTUMN_2020_USERS, SPRING_2021_USERS} from "./service/security";
import AccessContext from "./pages/accesscontext/AccessContext";
import NavigationPanel from "./pages/navbar/NavigationPanel";
import MainPageView from "./pages/main-page/MainPageView";
import Autumn2020Landing from "./pages/year/2020/autumn/landing/Landing";
import Autumn2020ActionsOverview from "./pages/year/2020/autumn/actions-overview/ActionsOverview";
import Autumn2020ExerciseInfoForm from "./pages/year/2020/autumn/exercise-info-form/ExerciseInfoForm";
import Autumn2020ExerciseInfoRouter from "./pages/year/2020/autumn/exercise-info/ExerciseInfoRouter";
import Spring2021Landing from "./pages/year/2021/spring/landing/Landing";
import Spring2021InputFileNameForm from "./pages/year/2021/spring/input-file-name-form/InputFileNameForm";
import Spring2021ActionsOverview from "./pages/year/2021/spring/actions-overview/ActionsOverview";
import Spring2021FetchExerciseInfo from "./pages/year/2021/spring/exercise-info/FetchExerciseInfo"

const App = () => {
  return (
    <Switch>
      <AuthWrapper path="/login" component={LoginForm} rolesAllowed={ALL_USERS} />
      <AuthWrapper path="/logout" component={Logout} rolesAllowed={ALL_USERS} />
      <div>
        <AccessContext />
        <NavigationPanel />
        <AuthWrapper path="/" component={MainPageView} rolesAllowed={ALL_USERS} />
        <AuthWrapper path="/year/2020/autumn" component={Autumn2020Landing} rolesAllowed={AUTUMN_2020_USERS} />
        <AuthWrapper path="/year/2020/autumn/actions-overview" component={Autumn2020ActionsOverview} rolesAllowed={AUTUMN_2020_USERS} />
        <AuthWrapper path="/year/2020/autumn/exercise-info-form" component={Autumn2020ExerciseInfoForm} rolesAllowed={AUTUMN_2020_USERS} />
        <AuthWrapper path="/year/2020/autumn/exercise-info" component={Autumn2020ExerciseInfoRouter} rolesAllowed={AUTUMN_2020_USERS} />
        <AuthWrapper path="/year/2021/spring" component={Spring2021Landing} rolesAllowed={SPRING_2021_USERS} />
        <AuthWrapper path="/year/2021/spring/input-file-name" component={Spring2021InputFileNameForm} rolesAllowed={SPRING_2021_USERS} />
        <AuthWrapper path="/year/2021/spring/actions-overview" component={Spring2021ActionsOverview} rolesAllowed={SPRING_2021_USERS} />
        <AuthWrapper path="/year/2021/spring/exercise-info" component={Spring2021FetchExerciseInfo} rolesAllowed={SPRING_2021_USERS} />
      </div>
    </Switch>
  );
}

export default App;