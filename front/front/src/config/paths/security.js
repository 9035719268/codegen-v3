const securityPort = 9999;

export const loginPath = `http://localhost:${securityPort}/api/login`;

export const refreshTokenPath = `http://localhost:${securityPort}/api/token/refresh`;