const frontEndPort = 3000;

export const sendLoginCredentialsPath = `http://localhost:${frontEndPort}/send-login-credentials`;

export const autumn2020MenuPath = `http://localhost:${frontEndPort}/year/2020/autumn`;

export const spring2021MenuPath = `http://localhost:${frontEndPort}/year/2021/spring`;

export const getAutumn2020Var1ExerciseInfoPagePath = inputFileName => {
  return `http://localhost:${frontEndPort}/year/2020/autumn/exercise-info?var=1&inputfilename=${inputFileName}`;
}

export const getAutumn2020Var2ExerciseInfoPagePath = inputFileName => {
  return `http://localhost:${frontEndPort}/year/2020/autumn/exercise-info?var=2&inputfilename=${inputFileName}`;
}

export const getAutumn2020Var3ExerciseInfoPagePath = (lat, lon, satelliteNumber) => {
  return `http://localhost:${frontEndPort}/year/2020/autumn/exercise-info?var=3&lat=${lat}&lon=${lon}&satnum=${satelliteNumber}`;
}

export const getSpring2021ExerciseInfoPagePath = inputFileName => {
  return `http://localhost:${frontEndPort}/year/2021/spring/exercise-info?inputfilename=${inputFileName}`;
}