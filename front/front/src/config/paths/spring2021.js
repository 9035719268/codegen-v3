const gatewayPort = 8888;

export const getSpring2021ExerciseInfoPath = inputFileName => {
  return `http://localhost:${gatewayPort}/api/year/2021/spring/exercise-info?inputfilename=${inputFileName}`;
}

export const spring2021DownloadResourcesPath = `http://localhost:${gatewayPort}/year/2021/spring/download/resources`;

export const getSpring2021DownloadChartsPath = inputFileName => {
  return `http://localhost:${gatewayPort}/api/year/2021/spring/download/chart/${inputFileName}`;
}

export const getSpring2021DownloadListingPath = (lang, file, oop, inputFileName) => {
  return `http://localhost:${gatewayPort}/api/year/2021/spring/download/listing/${lang}/${file}/${oop}/${inputFileName}/`;
}

export const spring2021SwaggerUiPath = `http://localhost:${gatewayPort}/api/year/2021/spring/swagger-ui`;

export const spring2021ActuatorPath = `http://localhost:${gatewayPort}/api/year/2021/spring/actuator`;