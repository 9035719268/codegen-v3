const gatewayPort = 8888;

export const getAutumn2020Var1ExerciseInfoPath = inputFileName => {
  return `http://localhost:${gatewayPort}/api/year/2020/autumn/var1/exercise-info?inputfilename=${inputFileName}`;
}

export const getAutumn2020Var2ExerciseInfoPath = inputFileName => {
  return `http://localhost:${gatewayPort}/api/year/2020/autumn/var2/exercise-info?inputfilename=${inputFileName}`;
}

export const getAutumn2020Var3ExerciseInfoPath = (lat, lon, satelliteNumber) => {
  return `http://localhost:${gatewayPort}/api/year/2020/autumn/var3/exercise-info?lat=${lat}&lon=${lon}&satnum=${satelliteNumber}`;
}

export const autumn2020DownloadResourcesPath = `http://localhost:${gatewayPort}/year/2020/autumn/download/resources`;

export const getAutumn2020DownloadChartsPath = (varNumber, inputFileName) => {
  return `http://localhost:${gatewayPort}/api/year/2020/autumn/var${varNumber}/download/chart/${inputFileName}`;
}

export const getAutumn2020Var1DownloadListingPath = (lang, file, oop, inputFileName) => {
  return `http://localhost:${gatewayPort}/api/year/2020/autumn/var1/download/listing/${lang}/${file}/${oop}/${inputFileName}/`;
}

export const getAutumn2020Var2DownloadListingPath = (lang, file, oop, inputFileName) => {
  return `http://localhost:${gatewayPort}/api/year/2020/autumn/var2/download/listing/${lang}/${file}/${oop}/${inputFileName}/`;
}

export const getAutumn2020Var3DownloadListingPath = (lang, file, oop) => {
  return `http://localhost:${gatewayPort}/api/year/2020/autumn/var3/download/listing/${lang}/${file}/${oop}`;
}

export const autumn2020SwaggerUiPath = `http://localhost:${gatewayPort}/api/year/2020/autumn/swagger-ui`;

export const autumn2020ActuatorPath = `http://localhost:${gatewayPort}/api/year/2020/autumn/actuator`;