export const adminAccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" +
  ".eyJzdWIiOiJhZG1pbiIsInJvbGVzIjpbIlJPTEVfQURNSU4iXSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo5OTk4L2FwaS9sb2dpbiIsImV4cCI6MTYzNDExNzAyNX0" +
  ".yPLTaJcTZ72-d5f9oPHMVRRbCweqNLBFRJ-oFFnINbo"

export const a20201AccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" +
  ".eyJzdWIiOiJhMjAyMDEiLCJyb2xlcyI6WyJST0xFX0EyMDIwMSJdLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0Ojk5OTgvYXBpL2xvZ2luIiwiZXhwIjoxNjM0MTE3MTEyfQ" +
  ".Z5HGjFFThGbGivPFoiQb4BOrdet06jPeX9SIA3bIv3g"

export const a20202AccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" +
  ".eyJzdWIiOiJhMjAyMDIiLCJyb2xlcyI6WyJST0xFX0EyMDIwMiJdLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0Ojk5OTgvYXBpL2xvZ2luIiwiZXhwIjoxNjM0MTE3MTQ2fQ" +
  ".3nYQxD83TvW0SJzzRe4lpWsovBpKBjPhPxe5OfSFsoU"

export const a20203AccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" +
  ".eyJzdWIiOiJhMjAyMDMiLCJyb2xlcyI6WyJST0xFX0EyMDIwMyJdLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0Ojk5OTgvYXBpL2xvZ2luIiwiZXhwIjoxNjM0MTE3MTkzfQ" +
  ".1KcdvDklH3OLJoAP0TFaZpXVewvkn98h9dUN1aNJwRQ"

export const s2021AccessToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" +
  ".eyJzdWIiOiJzMjAyMSIsInJvbGVzIjpbIlJPTEVfUzIwMjEiXSwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo5OTk4L2FwaS9sb2dpbiIsImV4cCI6MTYzNDExNzIyMn0" +
  ".0Oa8Ko8LP5z3XXpLcBIWrj2o-bdetWf5KIIlAMARiic"

export const adminRefreshToken = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9" +
  ".eyJzdWIiOiJhZG1pbiIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6OTk5OC9hcGkvbG9naW4iLCJleHAiOjE2MzQyMjczMjh9" +
  ".ijgYmy99aiLWq53pDl70kQsRWtc30SEaLeQnK7Vl8dg";