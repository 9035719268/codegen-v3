import {render, unmountComponentAtNode} from "react-dom";
import LoginForm from "./LoginForm";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderLoginFormNoErrorInfo", () => {
  const location = {
    state: {errorInfo: ""}
  }

  render(<LoginForm location={location}/>, container);

  expect(container.getElementsByTagName("DIV")[2].textContent).toBe("Имя пользователя");
  expect(container.getElementsByTagName("DIV")[3].textContent).toBe("Пароль");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Войти");
  expect(container.getElementsByTagName("DIV")[4].textContent).toBe("");
});

it("shouldRenderLoginFormWithErrorInfo", () => {
  const location = {
    state: {errorInfo: "> Информация об ошибке"}
  }

  render(<LoginForm location={location}/>, container);

  expect(container.getElementsByTagName("DIV")[2].textContent).toBe("Имя пользователя");
  expect(container.getElementsByTagName("DIV")[3].textContent).toBe("Пароль");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Войти");
  expect(container.getElementsByTagName("DIV")[4].textContent).toBe("> Информация об ошибке");
});