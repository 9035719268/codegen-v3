import {Route} from "react-router-dom";
import {checkUserAccess} from "../service/security";

const AuthWrapper = props => {
  const path = props.path;
  const component = props.component;
  const rolesAllowed = props.rolesAllowed;

  const hasAccess = checkUserAccess(rolesAllowed);

  if (hasAccess) {
    return <Route path={path} component={component} exact />
  } else {
    return <></>
  }
}

export default AuthWrapper;