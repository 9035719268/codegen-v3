import {checkUserAccess} from "../../../service/security";
import MainPageButton from "../button/MainPageButton";

const ButtonAuthWrapper = props => {
  const path = props.path;
  const text = props.text;
  const isServiceEnabled = props.isServiceEnabled;
  const rolesAllowed = props.rolesAllowed;
  const className = props.className;

  const hasAccess = checkUserAccess(rolesAllowed);

  if (hasAccess) {
    return <MainPageButton path={path} text={text} isServiceEnabled={isServiceEnabled} className={className} />
  } else {
    return <></>
  }
}

export default ButtonAuthWrapper;