const MainPageButton = props => {
  const path = props.path;
  const text = props.text;
  const isServiceEnabled = props.isServiceEnabled === undefined ? "" : props.isServiceEnabled;
  const className = props.className;

  return (
    <a href={path}>
      <button className={className} type="submit">{`${text} ${isServiceEnabled}`}</button>
    </a>
  );
}

export default MainPageButton;