import {autumn2020MenuPath, spring2021MenuPath} from "../../config/paths/frontend";
import {autumn2020ActuatorPath, autumn2020SwaggerUiPath} from "../../config/paths/autumn2020";
import {spring2021ActuatorPath, spring2021SwaggerUiPath} from "../../config/paths/spring2021";
import {eurekaDashboardPath} from "../../config/paths/eureka";
import {useEffect, useState} from "react";
import axios from "axios";
import ButtonAuthWrapper from "./authwrapper/ButtonAuthWrapper";
import {AUTUMN_2020_USERS, SPRING_2021_USERS} from "../../service/security";

const MainPageView = () => {
  const [isAutumn2020ServiceEnabled, setAutumn2020ServiceEnabled] = useState("[отключен]");
  const [isSpring2021ServiceEnabled, setSpring2021ServiceEnabled] = useState("[отключен]");

  useEffect(() => {
    axios.get(autumn2020ActuatorPath).then(() => setAutumn2020ServiceEnabled("[работает]"));

    axios.get(spring2021ActuatorPath).then(() => setSpring2021ServiceEnabled("[работает]"));
  })

  return (
    <div>
      <div>
        <ButtonAuthWrapper
          path={autumn2020MenuPath}
          text="Осень 2020"
          isServiceEnabled={isAutumn2020ServiceEnabled}
          rolesAllowed={AUTUMN_2020_USERS}
          className="overview"
        />
        <ButtonAuthWrapper
          path={spring2021MenuPath}
          text="Весна 2021"
          isServiceEnabled={isSpring2021ServiceEnabled}
          rolesAllowed={SPRING_2021_USERS}
          className="overview"
        />
      </div>
      <div>
        <ButtonAuthWrapper
          path={autumn2020SwaggerUiPath}
          text="Осень 2020 Swagger UI"
          isServiceEnabled={isAutumn2020ServiceEnabled}
          rolesAllowed={AUTUMN_2020_USERS}
          className="swagger-ui-button"
        />
        <ButtonAuthWrapper
          path={spring2021SwaggerUiPath}
          text="Весна 2021 Swagger UI"
          isServiceEnabled={isSpring2021ServiceEnabled}
          rolesAllowed={SPRING_2021_USERS}
          className="swagger-ui-button"
        />
      </div>
      <div>
        <ButtonAuthWrapper
          path={eurekaDashboardPath}
          text="Eureka Dashboard"
          rolesAllowed={["ROLE_ADMIN"]}
          className="eureka-dashboard-button"
        />
      </div>
    </div>
  );
}

export default MainPageView;