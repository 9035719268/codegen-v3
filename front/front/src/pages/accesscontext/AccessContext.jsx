import {useEffect, useState} from "react";
import {getNewAccessToken, isTokenExpired} from "../../service/security";
import translate from "translate";
import {Redirect} from "react-router-dom";

const AccessContext = () => {
  const accessTokenExpirationTime = 1800000;

  const [errorInfo, setErrorInfo] = useState("");

  useEffect(() => {
    const interval = setInterval(() => {
      getNewAccessToken()
        .then(response => {
          const accessToken = response.data["access_token"];

          if (accessToken) {
            localStorage.setItem("access_token", accessToken);
          } else {
            translate(response.data["error_info"], "ru")
              .then(errInfo => setErrorInfo(errInfo))
          }
        });
    }, accessTokenExpirationTime);

    return () => clearInterval(interval);
  });

  useEffect(() => {
    const tokenExpired = isTokenExpired();

    if (tokenExpired) {
      setErrorInfo("Токен устарел");
    }
  }, []);

  if (errorInfo === "") {
    return <></>
  } else {
    return <Redirect to={{pathname: "/login", state: {errorInfo: errorInfo}}} />
  }
}

export default AccessContext;