const DownloadPythonListingButton = props => {
  return (
    <a href={props.path + "main.py"} download="main.py">
      <button className="overview" type="submit">Загрузить main.py</button>
    </a>
  );
}

export default DownloadPythonListingButton;