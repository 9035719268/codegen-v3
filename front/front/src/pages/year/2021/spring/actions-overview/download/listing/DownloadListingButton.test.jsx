import {render, unmountComponentAtNode} from "react-dom";
import DownloadListingButton from "./DownloadListingButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppDownloadListingButton", () => {
  render(<DownloadListingButton lang="cpp" />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
});

it("shouldRenderJavaDownloadListingButtons", () => {
  render(<DownloadListingButton lang="java" />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
});

it("shouldRenderPythonDownloadListingButton", () => {
  render(<DownloadListingButton lang="python" />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
});
