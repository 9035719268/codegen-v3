import {render, unmountComponentAtNode} from "react-dom";
import DownloadResourcesButton from "./DownloadResourcesButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadResourcesButton", () => {
  render(<DownloadResourcesButton />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Загрузить resources.rar");
});