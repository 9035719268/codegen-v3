import {render, unmountComponentAtNode} from "react-dom";
import DownloadPythonListingButton from "./DownloadPythonListingButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderPythonDownloadListingButton", () => {
  render(<DownloadPythonListingButton />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Загрузить main.py");
});