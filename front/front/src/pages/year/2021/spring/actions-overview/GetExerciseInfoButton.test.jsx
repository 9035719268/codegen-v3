import {render, unmountComponentAtNode} from "react-dom";
import GetExerciseInfoButton from "./GetExerciseInfoButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderGetExerciseInfoButton", () => {
  render(<GetExerciseInfoButton inputFileName="arti_6hours.dat" />, container);

  expect(container.getElementsByTagName("A")[0].href).toBe("http://localhost:3000/year/2021/spring/exercise-info?inputfilename=arti_6hours.dat")
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Получить информацию по заданию");
});