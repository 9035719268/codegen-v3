import DownloadJavaListingButton from "./java/DownloadJavaListingButton";
import DownloadCppListingButton from "./cpp/DownloadCppListingButton";
import DownloadPythonListingButton from "./python/DownloadPythonListingButton";
import {getSpring2021DownloadListingPath} from "../../../../../../../config/paths/spring2021";

const DownloadListingButton = props => {
  const path = getSpring2021DownloadListingPath(props.lang, props.file, props.oop, props.inputFileName);

  if (props.lang === "cpp") {
    return <DownloadCppListingButton path={path} />
  } else if (props.lang === "java") {
    return <DownloadJavaListingButton path={path} />
  } else if (props.lang === "python") {
    return <DownloadPythonListingButton path={path} />
  }
}

export default DownloadListingButton;