import {getSpring2021ExerciseInfoPagePath} from "../../../../../config/paths/frontend";

const GetExerciseInfoButton = props => {
  const path = getSpring2021ExerciseInfoPagePath(props.inputFileName);

  return (
    <a href={path}>
      <button className="overview" type="submit">Получить информацию по заданию</button>
    </a>
  );
}

export default GetExerciseInfoButton;