import {render, unmountComponentAtNode} from "react-dom";
import DownloadJavaListingButton from "./DownloadJavaListingButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadMainButton", () => {
  render(<DownloadJavaListingButton />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Загрузить Main.java");
});

it("shouldRenderDownloadGraphDrawerButton", () => {
  render(<DownloadJavaListingButton />, container);

  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("Загрузить GraphDrawer.java");
});