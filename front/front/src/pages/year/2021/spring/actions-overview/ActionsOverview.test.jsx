import {render, unmountComponentAtNode} from "react-dom";
import ActionsOverview from "./ActionsOverview";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderValuesForCppFileNoOop", () => {
  const location = {
    search: "?lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForCppFileWithOop", () => {
  const location = {
    search: "?lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForPythonFileNoOop", () => {
  const location = {
    search: "?lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForPythonFileWithOop", () => {
  const location = {
    search: "?lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForJavaFileWithOop", () => {
  const location = {
    search: "?lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForCppManualNoOop", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForCppManualWithOop", () => {
  const location = {
    search: "?lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForPythonManualNoOop", () => {
  const location = {
    search: "?lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForPythonManualWithOop", () => {
  const location = {
    search: "?lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForJavaManualWithOop", () => {
  const location = {
    search: "?lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Весна, 2021 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});