import {render, unmountComponentAtNode} from "react-dom";
import DownloadCppListingButton from "./DownloadCppListingButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppDownloadListingButton", () => {
  render(<DownloadCppListingButton />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Загрузить main.cpp");
});