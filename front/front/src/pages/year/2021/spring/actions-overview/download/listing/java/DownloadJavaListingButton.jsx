const DownloadJavaListingButton = props => {
  return (
    <span>
      <a href={props.path + "Main.java"} download="Main.java">
        <button className="overview" type="submit">Загрузить Main.java</button>
      </a>
      <a href={props.path + "GraphDrawer.java"} download="GraphDrawer.java">
        <button className="overview" type="submit">Загрузить GraphDrawer.java</button>
      </a>
    </span>
  );
}

export default DownloadJavaListingButton;