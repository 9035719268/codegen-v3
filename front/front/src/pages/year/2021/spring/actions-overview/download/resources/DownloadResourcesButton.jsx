import {spring2021DownloadResourcesPath} from "../../../../../../../config/paths/spring2021";

const DownloadResourcesButton = () => {
  return (
    <a href={spring2021DownloadResourcesPath} download="resources.rar">
      <button className="overview" type="submit">Загрузить resources.rar</button>
    </a>
  );
}

export default DownloadResourcesButton;