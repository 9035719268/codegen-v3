const DownloadCppListingButton = props => {
  return (
    <a href={props.path + "main.cpp"} download="main.cpp">
      <button className="overview" type="submit">Загрузить main.cpp</button>
    </a>
  );
}

export default DownloadCppListingButton;