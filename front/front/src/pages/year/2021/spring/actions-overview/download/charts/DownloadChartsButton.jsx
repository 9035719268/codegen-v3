import {getSpring2021DownloadChartsPath} from "../../../../../../../config/paths/spring2021";

const DownloadChartsButton = props => {
  const path = getSpring2021DownloadChartsPath(props.inputFileName);

  return (
    <a href={path} download={"charts.rar"}>
      <button className="overview" type="submit">Загрузить charts.rar</button>
    </a>
  );
}

export default DownloadChartsButton;