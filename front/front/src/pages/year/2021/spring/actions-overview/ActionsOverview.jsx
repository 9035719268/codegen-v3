import "../../../../../index.css";
import DownloadListingButton from "./download/listing/DownloadListingButton";
import DownloadChartsButton from "./download/charts/DownloadChartsButton";
import DownloadResourcesButton from "./download/resources/DownloadResourcesButton";
import GetExerciseInfoButton from "./GetExerciseInfoButton";
import {extractFile, extractInputFileName, extractLang, extractOop} from "../../../../../service/queryParamsExtraction";

const ActionsOverview = props => {
  const routerSearch = props.location.search;
  const lang = extractLang(routerSearch);
  const file = extractFile(routerSearch);
  const oop = extractOop(routerSearch);
  const inputFileName = extractInputFileName(routerSearch);

  return (
    <div>
      <div className="ttl">
        <p>Весна, 2021 год</p>
        <span>Язык программирования: {lang.charAt(0).toUpperCase() + lang.slice(1)}, </span>
        <span>{file === "file" ? "с чтением файла, " : "без чтения файла, "}</span>
        <span>{oop === "withoop" ? "с ООП" : "без ООП"}</span>
      </div>
      <DownloadListingButton lang={lang} file={file} oop={oop} inputFileName={inputFileName} />
      <DownloadChartsButton inputFileName={inputFileName} />
      <DownloadResourcesButton />
      <GetExerciseInfoButton inputFileName={inputFileName} />
    </div>
  );
}

export default ActionsOverview;