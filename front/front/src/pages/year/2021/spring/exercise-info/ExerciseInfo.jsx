import "../../../../../index.css";
import InputFileNameView from "./components/InputFileNameView";
import SatelliteInfoView from "./components/SatelliteInfoView";
import TroposphericDelayComponentsView from "./components/TroposphericDelayComponentsView";
import ElevationAnglesView from "./components/ElevationAnglesView";

const ExerciseInfo = props => {
  const [satellite1Number, satellite1Md, satellite1Td, satellite1Mw, satellite1Tw, satellite1Elevation] = Object.values(props.satellite1Data);
  const [satellite2Number, satellite2Md, satellite2Td, satellite2Mw, satellite2Tw, satellite2Elevation] = Object.values(props.satellite2Data);
  const [satellite3Number, satellite3Md, satellite3Td, satellite3Mw, satellite3Tw, satellite3Elevation] = Object.values(props.satellite3Data);

  return (
    <div>
      <div className="ttl">Дополнительная информация</div>
      <InputFileNameView inputFileName={props.inputFileName} />
      <div className="groupval">
        <SatelliteInfoView order="первого" number={satellite1Number} />
        <SatelliteInfoView order="второго" number={satellite2Number} />
        <SatelliteInfoView order="третьего" number={satellite3Number} />
      </div>
      <TroposphericDelayComponentsView order="первого" md={satellite1Md} td={satellite1Td} mw={satellite1Mw} tw={satellite1Tw}/>
      <TroposphericDelayComponentsView order="второго" md={satellite2Md} td={satellite2Td} mw={satellite2Mw} tw={satellite2Tw}/>
      <TroposphericDelayComponentsView order="третьего" md={satellite3Md} td={satellite3Td} mw={satellite3Mw} tw={satellite3Tw}/>
      <ElevationAnglesView order="первого" elevation={satellite1Elevation}/>
      <ElevationAnglesView order="второго" elevation={satellite2Elevation}/>
      <ElevationAnglesView order="третьего" elevation={satellite3Elevation}/>
    </div>
  );
}

export default ExerciseInfo;