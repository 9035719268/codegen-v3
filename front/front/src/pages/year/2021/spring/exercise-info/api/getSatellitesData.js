import axios from "axios";
import {getSpring2021ExerciseInfoPath} from "../../../../../../config/paths/spring2021";

const getSatellitesData = inputFileName => {
  const path = getSpring2021ExerciseInfoPath(inputFileName);

  return axios.get(path).then(response => {
    const satellite1 = response.data[0];
    const satellite2 = response.data[1];
    const satellite3 = response.data[2];
    return {satellite1, satellite2, satellite3};
  });
}

export default getSatellitesData;