const InputFileNameView = props => {
  return (
    <div className="groupval">
      <div>Название файла:
        <span className="inf">{props.inputFileName}</span>
      </div>
    </div>
  );
}

export default InputFileNameView;