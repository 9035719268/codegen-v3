import getValuesDividedByComma from "../../../../../../service/getValuesDividedByComma";

const TroposphericDelayComponentsView = props => {
  let md = null;
  let td = null;
  let mw = null;
  let tw = null;

  const areAllComponentsDefined = () => props.md !== undefined && props.td !== undefined && props.mw !== undefined && props.tw !== undefined;

  if (areAllComponentsDefined()) {
    md = getValuesDividedByComma(props.md);
    td = getValuesDividedByComma(props.td);
    mw = getValuesDividedByComma(props.mw);
    tw = getValuesDividedByComma(props.tw);
  }

  return (
    <div>
      <div className="groupval">Компонент md {props.order} спутника:
        <div className="inf">{md === null ? "Загрузка..." : md}</div>
      </div>
      <div className="groupval">Компонент td {props.order} спутника:
        <div className="inf">{td === null ? "Загрузка..." : td}</div>
      </div>
      <div className="groupval">Компонент mw {props.order} спутника:
        <div className="inf">{mw === null ? "Загрузка..." : mw}</div>
      </div>
      <div className="groupval">Компонент tw {props.order} спутника:
        <div className="inf">{tw === null ? "Загрузка..." : tw}</div>
      </div>
    </div>
  );
}

export default TroposphericDelayComponentsView;