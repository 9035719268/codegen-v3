import getValuesDividedByComma from "../../../../../../service/getValuesDividedByComma";

const ElevationAnglesView = props => {
  let elevation = null;

  if (props.elevation !== undefined) {
    elevation = getValuesDividedByComma(props.elevation);
  }

  return (
    <div className="groupval">Углы возвышения {props.order} спутника:
      <div className="inf">{elevation === null ? "Загрузка..." : elevation}</div>
    </div>
  );
}

export default ElevationAnglesView;