import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";
import {SPRING_2021_USERS} from "../../../../../../../service/security";

const CppTableRow = () => {
  const path = "/year/2021/spring/input-file-name";
  const withOop = "С использованием ООП";
  const noOop = "Без использования ООП";

  return (
    <tr>
      <th scope="row">Cpp</th>
      <td>
        <TableQueryAuthWrapper path={path} lang="cpp" file="true" oop="false" text={noOop} rolesAllowed={SPRING_2021_USERS} />
        <TableQueryAuthWrapper path={path} lang="cpp" file="true" oop="true" text={withOop} rolesAllowed={SPRING_2021_USERS} />
      </td>
      <td>
        <TableQueryAuthWrapper path={path} lang="cpp" file="false" oop="false" text={noOop} rolesAllowed={SPRING_2021_USERS} />
        <TableQueryAuthWrapper path={path} lang="cpp" file="false" oop="true" text={withOop} rolesAllowed={SPRING_2021_USERS} />
      </td>
    </tr>
  );
}

export default CppTableRow;