import {render, unmountComponentAtNode} from "react-dom";
import Landing from "./Landing";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderLandingAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("С чтением файла (на 5)");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Без чтения файла (на 3/4)");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe("С использованием ООП");
});

it("shouldRenderLandingA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("С чтением файла (на 5)");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Без чтения файла (на 3/4)");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderLandingA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("С чтением файла (на 5)");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Без чтения файла (на 3/4)");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderLandingA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("С чтением файла (на 5)");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Без чтения файла (на 3/4)");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderLandingS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("С чтением файла (на 5)");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Без чтения файла (на 3/4)");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe("С использованием ООП");
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe("С использованием ООП");
});
