import {render, unmountComponentAtNode} from "react-dom";
import TableRow from "./TableRow";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
});

it("shouldRenderCppTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderCppTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderCppTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderCppTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
});

it("shouldRenderJavaTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("С использованием ООП");
});

it("shouldRenderJavaTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderJavaTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderJavaTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderJavaTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("С использованием ООП");
});

it("shouldRenderPythonTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
});

it("shouldRenderPythonTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
});
