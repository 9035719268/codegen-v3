import {render, unmountComponentAtNode} from "react-dom";
import PythonTableRow from "./PythonTableRow";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderPythonTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
});

it("shouldRenderPythonTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Без использования ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("С использованием ООП");
});