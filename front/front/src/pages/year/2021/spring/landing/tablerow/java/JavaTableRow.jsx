import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";
import {SPRING_2021_USERS} from "../../../../../../../service/security";

const JavaTableRow = () => {
  const path = "/year/2021/spring/input-file-name";
  const withOop = "С использованием ООП";

  return (
    <tr>
      <th scope="row">Java</th>
      <td>
        <TableQueryAuthWrapper path={path} lang="java" file="true" oop="true" text={withOop} rolesAllowed={SPRING_2021_USERS} />
      </td>
      <td>
        <TableQueryAuthWrapper path={path} lang="java" file="false" oop="true" text={withOop} rolesAllowed={SPRING_2021_USERS} />
      </td>
    </tr>
  );
}

export default JavaTableRow;