import {render, unmountComponentAtNode} from "react-dom";
import TableHeader from "./TableHeader";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFirstHeader", () => {
  render(<TableHeader/>, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("С чтением файла (на 5)");
});

it("shouldRenderSecondHeader", () => {
  render(<TableHeader/>, container);

  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Без чтения файла (на 3/4)");
});