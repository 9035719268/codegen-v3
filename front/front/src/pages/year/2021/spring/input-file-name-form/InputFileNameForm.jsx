import "../../../../../index.css";

const InputFileNameForm = props => {
  const routerSearch = props.location.search;
  const searchParams = new URLSearchParams(routerSearch);
  const lang = searchParams.get("lang")
  const file = searchParams.get("file");
  const oop = searchParams.get("oop");

  return (
    <form action="actions-overview" method="get">
      <input type="hidden" name="lang" value={lang} />
      <input type="hidden" name="file" value={file} />
      <input type="hidden" name="oop" value={oop} />
      <div className="ttl">Введите название файла наблюдений</div>
      <label htmlFor="inputfilename">Название файла</label>
      <input name="inputfilename" type="text" id="inputfilename" placeholder="Введите текст" required={true} />
      <button className="form-button" type="submit">Отправить</button>
    </form>
  );
}

export default InputFileNameForm;