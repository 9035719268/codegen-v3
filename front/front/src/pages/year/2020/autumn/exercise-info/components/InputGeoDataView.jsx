const InputGeoDataView = props => {
  let lat = null;
  let lon = null;
  let lat1 = null;
  let lat2 = null;
  let lon1 = null;
  let lon2 = null;
  let satelliteNumber = null;

  const areAllValuesDefined = () => {
    return props.lat !== undefined && props.lon !== undefined &&
      props.lat1 !== undefined && props.lat2 !== undefined &&
      props.lon1 !== undefined && props.lon2 !== undefined &&
      props.satelliteNumber !== undefined;
  }

  if (areAllValuesDefined()) {
    lat = props.lat;
    lon = props.lon;
    lat1 = props.lat1;
    lat2 = props.lat2;
    lon1 = props.lon1;
    lon2 = props.lon2;
    satelliteNumber = props.satelliteNumber;
  }

  return (
    <div>
      <div>Широта города:
        <span className="inf">{lat === null ? "Загрузка..." : lat}</span>
      </div>
      <div>Долгота города:
        <span className="inf">{lon === null ? "Загрузка..." : lon}</span>
      </div>
      <div>Меньшая широта точек для интерполяции:
        <span className="inf">{lat1 === null ? "Загрузка..." : lat1}</span>
      </div>
      <div>Большая широта точек для интерполяции:
        <span className="inf">{lat2 === null ? "Загрузка..." : lat2}</span>
      </div>
      <div>Меньшая долгота точек для интерполяции:
        <span className="inf">{lon1=== null ? "Загрузка..." : lon1}</span>
      </div>
      <div>Большая долгота точек для интерполяции:
        <span className="inf">{lon2 === null ? "Загрузка..." : lon2}</span>
      </div>
      <div>Номер спутника для сбора времени GPS:
        <span className="inf">{satelliteNumber === null ? "Загрузка..." : satelliteNumber}</span>
      </div>
    </div>
  );
}

export default InputGeoDataView;