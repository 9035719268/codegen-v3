import {render, unmountComponentAtNode} from "react-dom";
import ExerciseInfo from "./ExerciseInfo";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

const componentsData = {
  interpolationCoordinates: {
    biggerLatitude: "57.5",
    biggerLongitude: "165",
    smallerLatitude: "55.0",
    smallerLongitude: "160",
  },
  gpsTime: {
    gpsTime: ["1", "2", "3", "4", "5"]
  },
  alpha: ["6", "7", "8", "9", "10"],
  beta: ["11", "12", "13", "14", "15"],
  forecastValues: [{
    tec: ["16", "17", "18", "19", "20"]
  }, {
    tec: ["21", "22", "23", "24", "25"]
  }, {
    tec: ["26", "27", "28", "29", "30"]
  }, {
    tec: ["31", "32", "33", "34", "35"]
  }
  ],
  preciseValues: [{
    tec: ["36", "37", "38", "39", "40"]
  }, {
    tec: ["41", "42", "43", "44", "45"]
  }, {
    tec: ["46", "47", "48", "49", "50"]
  }, {
    tec: ["51", "52", "53", "54", "55"]
  }]
};

const inputData = {
  lat: 55,
  lon: 160,
  satelliteNumber: 7
};

it("shouldRenderExerciseInfoTitle", () => {
  render(<ExerciseInfo componentsData={componentsData} inputData={inputData} />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Дополнительная информация");
});

it("shouldRenderExerciseInfoVarNumber", () => {
  render(<ExerciseInfo componentsData={componentsData} inputData={inputData} />, container);

  expect(container.getElementsByClassName("groupval")[0].textContent).toBe("Номер варианта:3");
});

it("shouldRenderExerciseInfoInputDataLines", () => {
  render(<ExerciseInfo componentsData={componentsData} inputData={inputData} />, container);

  const inputDataLines = container.getElementsByClassName("groupval")[1];

  expect(inputDataLines.getElementsByTagName("DIV")[1].textContent).toBe("Широта города:55");
  expect(inputDataLines.getElementsByTagName("DIV")[2].textContent).toBe("Долгота города:160");
  expect(inputDataLines.getElementsByTagName("DIV")[7].textContent).toBe("Номер спутника для сбора времени GPS:7");
});

it("shouldRenderExerciseInfoGpsTime", () => {
  render(<ExerciseInfo componentsData={componentsData} inputData={inputData} />, container);

  expect(container.getElementsByClassName("groupval")[2].textContent).toBe("Массив времен GPS:1, 2, 3, 4, 5");
});

it("shouldRenderExerciseInfoIonCoefficients", () => {
  render(<ExerciseInfo componentsData={componentsData} inputData={inputData} />, container);

  const coefficientsGroup = container.getElementsByClassName("groupval")[3];

  expect(coefficientsGroup.getElementsByTagName("DIV")[0].textContent).toBe("Массив альфа-коэффициентов:6, 7, 8, 9, 10");
  expect(coefficientsGroup.getElementsByTagName("DIV")[1].textContent).toBe("Массив бета-коэффициентов:11, 12, 13, 14, 15");
});

it("shouldRenderExerciseInfoTecValues", () => {
  render(<ExerciseInfo componentsData={componentsData} inputData={inputData} />, container);

  const tecValuesGroup = container.getElementsByClassName("groupval")[4];

  expect(tecValuesGroup.getElementsByTagName("DIV")[0].textContent).toBe("Величина TEC точки A1 прогнозного файла:16, 17, 18, 19, 20");
  expect(tecValuesGroup.getElementsByTagName("DIV")[1].textContent).toBe("Величина TEC точки A2 прогнозного файла:21, 22, 23, 24, 25");
  expect(tecValuesGroup.getElementsByTagName("DIV")[2].textContent).toBe("Величина TEC точки A3 прогнозного файла:26, 27, 28, 29, 30");
  expect(tecValuesGroup.getElementsByTagName("DIV")[3].textContent).toBe("Величина TEC точки A4 прогнозного файла:31, 32, 33, 34, 35");
  expect(tecValuesGroup.getElementsByTagName("DIV")[4].textContent).toBe("Величина TEC точки A1 точного файла:36, 37, 38, 39, 40");
  expect(tecValuesGroup.getElementsByTagName("DIV")[5].textContent).toBe("Величина TEC точки A2 точного файла:41, 42, 43, 44, 45");
  expect(tecValuesGroup.getElementsByTagName("DIV")[6].textContent).toBe("Величина TEC точки A3 точного файла:46, 47, 48, 49, 50");
  expect(tecValuesGroup.getElementsByTagName("DIV")[7].textContent).toBe("Величина TEC точки A4 точного файла:51, 52, 53, 54, 55");
});