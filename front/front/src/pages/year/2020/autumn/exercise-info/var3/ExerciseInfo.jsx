import "../../../../../../index.css";
import VarNumberView from "../components/VarNumberView";
import InputGeoDataView from "../components/InputGeoDataView";
import GpsTimeView from "../components/GpsTimeView";
import IonCoefficientsView from "../components/IonCoefficientsView";
import TecValuesView from "../components/TecValuesView";

const ExerciseInfo = props => {
  const [interpolationCoordinates, gpsTime, alpha, beta, forecastTecValues, preciseTecValues] = Object.values(props.componentsData);

  const [lat, lon, satelliteNumber] = Object.values(props.inputData);

  return (
    <div>
      <div className="ttl">Дополнительная информация</div>
      <div className="groupval">
        <VarNumberView varNumber="3" />
      </div>
      <div className="groupval">
        <InputGeoDataView
          lat={lat === null ? "Загрузка..." : lat}
          lon={lon === null ? "Загрузка..." : lon}
          lat1={interpolationCoordinates.smallerLatitude === null ? "Загрузка..." : interpolationCoordinates.smallerLatitude}
          lat2={interpolationCoordinates.biggerLatitude === null ? "Загрузка..." : interpolationCoordinates.biggerLatitude}
          lon1={interpolationCoordinates.smallerLongitude === null ? "Загрузка..." : interpolationCoordinates.smallerLongitude}
          lon2={interpolationCoordinates.biggerLongitude === null ? "Загрузка..." : interpolationCoordinates.biggerLongitude}
          satelliteNumber={satelliteNumber === null ? "Загрузка..." : satelliteNumber}
        />
      </div>
      <div className="groupval">
        <GpsTimeView gpsTime={gpsTime} />
      </div>
      <div className="groupval">
        <IonCoefficientsView coefficientsName="альфа" coefficients={alpha} />
        <IonCoefficientsView coefficientsName="бета" coefficients={beta} />
      </div>
      <div className="groupval">
        <TecValuesView pointName="A1" fileType="прогнозного" tecValues={forecastTecValues[0]} />
        <TecValuesView pointName="A2" fileType="прогнозного" tecValues={forecastTecValues[1]} />
        <TecValuesView pointName="A3" fileType="прогнозного" tecValues={forecastTecValues[2]} />
        <TecValuesView pointName="A4" fileType="прогнозного" tecValues={forecastTecValues[3]} />
        <TecValuesView pointName="A1" fileType="точного" tecValues={preciseTecValues[0]} />
        <TecValuesView pointName="A2" fileType="точного" tecValues={preciseTecValues[1]} />
        <TecValuesView pointName="A3" fileType="точного" tecValues={preciseTecValues[2]} />
        <TecValuesView pointName="A4" fileType="точного" tecValues={preciseTecValues[3]} />
      </div>
    </div>
  );
}

export default ExerciseInfo;