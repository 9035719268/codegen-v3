import {render, unmountComponentAtNode} from "react-dom";
import TecValuesView from "./TecValuesView";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderTecValuesView", () => {
  const tecValues = {
    tec: ["1", "2", "3", "4", "5"]
  };

  render(<TecValuesView pointName="A1" fileType="прогнозного" tecValues={tecValues}/>, container);

  expect(container.textContent).toBe("Величина TEC точки A1 прогнозного файла:1, 2, 3, 4, 5");
});

it("shouldRenderEmptyTecValuesView", () => {
  render(<TecValuesView pointName="A1" fileType="прогнозного" />, container);

  expect(container.textContent).toBe("Величина TEC точки A1 прогнозного файла:Загрузка...");
});