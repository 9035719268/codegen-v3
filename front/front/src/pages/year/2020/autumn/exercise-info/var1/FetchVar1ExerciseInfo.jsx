import {useState, useEffect} from "react";
import getSatellitesData from "../api/var1/getSatellitesData";
import ExerciseInfo from "./ExerciseInfo";
import {extractInputFileName, extractVarNumber} from "../../../../../../service/queryParamsExtraction";

const FetchVar1ExerciseInfo = props => {
  const [satellite1Data, setSatellite1Data] = useState({});
  const [satellite2Data, setSatellite2Data] = useState({});
  const [satellite3Data, setSatellite3Data] = useState({});

  const varNumber = extractVarNumber(props.routerSearch);
  const inputFileName = extractInputFileName(props.routerSearch);

  useEffect(() => {
    getSatellitesData(inputFileName).then(satellitesData => {
      const satellite1 = satellitesData.satellite1;
      setSatellite1Data({
        number: satellite1.number,
        p1: satellite1.p1,
        p2: satellite1.p2
      });
      const satellite2 = satellitesData.satellite2;
      setSatellite2Data({
        number: satellite2.number,
        p1: satellite2.p1,
        p2: satellite2.p2
      });
      const satellite3 = satellitesData.satellite3;
      setSatellite3Data({
        number: satellite3.number,
        p1: satellite3.p1,
        p2: satellite3.p2
      });
    });
  }, [inputFileName]);

  return (
    <ExerciseInfo
      varNumber={varNumber}
      inputFileName={inputFileName}
      satellite1Data={satellite1Data}
      satellite2Data={satellite2Data}
      satellite3Data={satellite3Data}
    />
  );
}

export default FetchVar1ExerciseInfo;