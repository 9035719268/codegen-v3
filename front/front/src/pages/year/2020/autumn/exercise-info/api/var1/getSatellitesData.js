import axios from "axios";
import {getAutumn2020Var1ExerciseInfoPath} from "../../../../../../../config/paths/autumn2020";

const getSatellitesData = inputFileName => {
  const path = getAutumn2020Var1ExerciseInfoPath(inputFileName);

  return axios.get(path).then(response => {
    const satellite1 = response.data[0];
    const satellite2 = response.data[1];
    const satellite3 = response.data[2];

    return {satellite1, satellite2, satellite3}
  });
}

export default getSatellitesData;