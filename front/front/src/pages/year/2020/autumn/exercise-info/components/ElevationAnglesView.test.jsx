import {render, unmountComponentAtNode} from "react-dom";
import ElevationAnglesView from "./ElevationAnglesView";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderElevationAnglesView", () => {
  const elevation = ["1", "2", "3", "4", "5"];

  render(<ElevationAnglesView order={"первого"} elevation={elevation} />, container);

  expect(container.textContent).toBe("Углы возвышения первого спутника:1, 2, 3, 4, 5");
});

it("shouldRenderEmptyElevationAnglesView", () => {
  render(<ElevationAnglesView order={"первого"} />, container);

  expect(container.textContent).toBe("Углы возвышения первого спутника:Загрузка...");
});
