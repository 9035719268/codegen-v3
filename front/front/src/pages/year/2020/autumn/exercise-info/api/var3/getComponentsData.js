import axios from "axios";
import {getAutumn2020Var3ExerciseInfoPath} from "../../../../../../../config/paths/autumn2020";

const getComponentsData = (lat, lon, satelliteNumber) => {
  const path = getAutumn2020Var3ExerciseInfoPath(lat, lon, satelliteNumber);

  return axios.get(path).then(response => response.data);
}

export default getComponentsData