import {useState, useEffect} from "react";
import getComponentsData from "../api/var3/getComponentsData";
import ExerciseInfo from "./ExerciseInfo";
import {extractLat, extractLon, extractSatelliteNumber} from "../../../../../../service/queryParamsExtraction";

const FetchVar3ExerciseInfo = props => {
  const [componentsData, setComponentsData] = useState({
    interpolationCoordinates: {},
    gpsTime: [],
    alpha: [],
    beta: [],
    forecastTecValues: [],
    preciseTecValues: []
  });

  const [inputData, setInputData] = useState({});
  
  const lat = extractLat(props.routerSearch);
  const lon = extractLon(props.routerSearch);
  const satelliteNumber = extractSatelliteNumber(props.routerSearch);

  useEffect(() => {
    getComponentsData(lat, lon, satelliteNumber)
      .then(data => {
        setComponentsData({
          interpolationCoordinates: data.interpolationCoordinates,
          gpsTime: data.gpsTime,
          alpha: data.alpha.ionCoefficients,
          beta: data.beta.ionCoefficients,
          forecastTecValues: data.forecastTecValues,
          preciseTecValues: data.preciseTecValues
        });
        setInputData({
          lat: lat,
          lon: lon,
          satelliteNumber: satelliteNumber
        });
      });
  }, [lat, lon, satelliteNumber]);

  return <ExerciseInfo componentsData={componentsData} inputData={inputData}/>;
}

export default FetchVar3ExerciseInfo;