import {render, unmountComponentAtNode} from "react-dom";
import Var1Form from "./Var1Form";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderForm", () => {
  render(<Var1Form />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Введите название файла наблюдений");
});