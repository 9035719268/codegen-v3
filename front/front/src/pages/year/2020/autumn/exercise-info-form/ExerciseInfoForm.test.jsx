import {render, unmountComponentAtNode} from "react-dom";
import ExerciseInfoForm from "./ExerciseInfoForm";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldGiveVar1InputForm", () => {
  const location = {
    search: "?var=1"
  };

  render(<ExerciseInfoForm location={location} />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Введите название файла наблюдений");
});

it("shouldGiveVar2InputForm", () => {
  const location = {
    search: "?var=2"
  };

  render(<ExerciseInfoForm location={location} />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Введите название файла наблюдений");
});

it("shouldGiveVar3InputForm", () => {
  const location = {
    search: "?var=3"
  };

  render(<ExerciseInfoForm location={location} />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Введите координаты города и координаты точек для интерполяции");
});