import {render, unmountComponentAtNode} from "react-dom";
import Var2Form from "./Var2Form";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderForm", () => {
  render(<Var2Form />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Введите название файла наблюдений");
});