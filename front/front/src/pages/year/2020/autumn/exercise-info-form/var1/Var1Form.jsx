const Var1Form = props => {
  return (
    <form action="actions-overview" method="get">
      <input type="hidden" name="var" value="1" />
      <input type="hidden" name="lang" value={props.lang} />
      <input type="hidden" name="file" value={props.file} />
      <input type="hidden" name="oop" value={props.oop} />
      <div className="ttl">Введите название файла наблюдений</div>
      <label htmlFor="inputfilename">Название файла</label>
      <input name="inputfilename" type="text" id="inputfilename" placeholder="Введите текст" required={true} />
      <button className="form-button" type="submit">Отправить</button>
    </form>
  );
}

export default Var1Form;