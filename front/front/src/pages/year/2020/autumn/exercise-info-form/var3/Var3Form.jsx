const Var3Form = props => {

  return (
    <form action="actions-overview" method="get">
      <input type="hidden" name="var" value="3" />
      <input type="hidden" name="lang" value={props.lang} />
      <input type="hidden" name="file" value={props.file} />
      <input type="hidden" name="oop" value={props.oop} />
      <div className="ttl">Введите координаты города и координаты точек для интерполяции</div>
      <div>
        <label htmlFor="lat">Широта города</label>
        <input name="lat" type="text" id="lat" placeholder="Введите текст" required={true} />
      </div>
      <div>
        <label htmlFor="lon">Долгота города</label>
        <input name="lon" type="text" id="lon" placeholder="Введите текст" required={true} />
      </div>
      <div>
        <label htmlFor="satnum">Номер спутника для сбора времени GPS</label>
        <input name="satnum" type="text" id="satnum" placeholder="Введите текст" required={true} />
      </div>
      <button className="form-button" type="submit">Отправить</button>
    </form>
  );
}

export default Var3Form;