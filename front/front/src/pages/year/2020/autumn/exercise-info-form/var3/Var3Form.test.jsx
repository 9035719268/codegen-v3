import {render, unmountComponentAtNode} from "react-dom";
import Var3Form from "./Var3Form";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderTitle", () => {
  render(<Var3Form />, container);

  expect(container.getElementsByClassName("ttl")[0].textContent).toBe("Введите координаты города и координаты точек для интерполяции");
});

it("shouldRenderLatInput", () => {
  render(<Var3Form />, container);

  expect(container.getElementsByTagName("LABEL")[0].textContent).toBe("Широта города")
});

it("shouldRenderLonInput", () => {
  render(<Var3Form />, container);

  expect(container.getElementsByTagName("LABEL")[1].textContent).toBe("Долгота города")
});

it("shouldRenderSatelliteNumber", () => {
  render(<Var3Form />, container);

  expect(container.getElementsByTagName("LABEL")[2].textContent).toBe("Номер спутника для сбора времени GPS")
});