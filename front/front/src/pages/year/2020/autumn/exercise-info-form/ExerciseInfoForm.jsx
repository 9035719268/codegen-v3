import "../../../../../index.css";
import Var1Form from "./var1/Var1Form";
import Var2Form from "./var2/Var2Form";
import Var3Form from "./var3/Var3Form";

const ExerciseInfoForm = props => {
  const routerSearch = props.location.search;
  const searchParams = new URLSearchParams(routerSearch);
  const varNumber = searchParams.get("var");
  const lang = searchParams.get("lang");
  const file = searchParams.get("file");
  const oop = searchParams.get("oop");

  if (varNumber === "1") {
    return <Var1Form lang={lang} file={file} oop={oop} />
  } else if (varNumber === "2") {
    return <Var2Form lang={lang} file={file} oop={oop} />
  } else if (varNumber === "3") {
    return <Var3Form lang={lang} file={file} oop={oop} />
  }
}

export default ExerciseInfoForm;