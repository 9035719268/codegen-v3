import {render, unmountComponentAtNode} from "react-dom";
import DownloadChartsButton from "./DownloadChartsButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderDownloadChartsButton", () => {
  render(<DownloadChartsButton inputFileName="POTS_6hours.dat" />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить charts.rar");
});