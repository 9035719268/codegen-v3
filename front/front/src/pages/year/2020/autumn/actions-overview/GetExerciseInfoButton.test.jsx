import {render, unmountComponentAtNode} from "react-dom";
import GetExerciseInfoButton from "./GetExerciseInfoButton";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderGetExerciseInfoButton", () => {
  render(<GetExerciseInfoButton />, container);

  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderVar1GetExerciseInfoButton", () => {
  const routerSearch = "?var=1&inputfilename=POTS_6hours.dat";

  render(<GetExerciseInfoButton varNumber="1" routerSearch={routerSearch} />, container);

  expect(container.getElementsByTagName("A")[0].href).toBe("http://localhost:3000/year/2020/autumn/exercise-info?var=1&inputfilename=POTS_6hours.dat");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderVar2GetExerciseInfoButton", () => {
  const routerSearch = "?var=2&inputfilename=POTS_6hours.dat";

  render(<GetExerciseInfoButton varNumber="2" routerSearch={routerSearch} />, container);

  expect(container.getElementsByTagName("A")[0].href).toBe("http://localhost:3000/year/2020/autumn/exercise-info?var=2&inputfilename=POTS_6hours.dat");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderVar3GetExerciseInfoButton", () => {
  const routerSearch = "?var=3&lat=55.0&lon=162&satnum=7";

  render(<GetExerciseInfoButton varNumber="3" routerSearch={routerSearch} />, container);

  expect(container.getElementsByTagName("A")[0].href).toBe("http://localhost:3000/year/2020/autumn/exercise-info?var=3&lat=55.0&lon=162&satnum=7");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("Получить информацию по заданию");
});