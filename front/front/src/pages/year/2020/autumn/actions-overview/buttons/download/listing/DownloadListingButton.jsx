import DownloadCppListingButton from "./cpp/DownloadCppListingButton";
import DownloadJavaListingButton from "./java/DownloadJavaListingButton";
import DownloadPythonListingButton from "./python/DownloadPythonListingButton";
import {
  getAutumn2020Var1DownloadListingPath,
  getAutumn2020Var2DownloadListingPath,
  getAutumn2020Var3DownloadListingPath
} from "../../../../../../../../config/paths/autumn2020";
import {extractInputFileName, extractLat, extractLon, extractSatelliteNumber} from "../../../../../../../../service/queryParamsExtraction";

const DownloadListingButton = props => {
  const varNumber = props.varNumber;
  const lang = props.lang;
  const file = props.file;
  const oop = props.oop;

  let path = "";
  let requestParams = "";

  if (varNumber === "1") {
    const inputFileName = extractInputFileName(props.routerSearch);
    path = getAutumn2020Var1DownloadListingPath(lang, file, oop, inputFileName);
  } else if (varNumber === "2") {
    const inputFileName = extractInputFileName(props.routerSearch);
    path = getAutumn2020Var2DownloadListingPath(lang, file, oop, inputFileName);
  } else if (varNumber === "3") {
    path = getAutumn2020Var3DownloadListingPath(lang, file, oop);
    const lat = extractLat(props.routerSearch);
    const lon = extractLon(props.routerSearch);
    const satelliteNumber = extractSatelliteNumber(props.routerSearch);
    requestParams = "?lat=" + lat + "&lon=" + lon + "&satnum=" + satelliteNumber;
  }

  if (props.lang === "cpp") {
    return <DownloadCppListingButton path={path} requestParams={requestParams} />
  } else if (props.lang === "java") {
    return <DownloadJavaListingButton path={path} requestParams={requestParams} />
  } else if (props.lang === "python") {
    return <DownloadPythonListingButton path={path} requestParams={requestParams} />
  }
}

export default DownloadListingButton;