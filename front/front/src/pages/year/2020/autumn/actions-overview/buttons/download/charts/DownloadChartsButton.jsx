import {getAutumn2020DownloadChartsPath} from "../../../../../../../../config/paths/autumn2020";
import {extractInputFileName, extractVarNumber} from "../../../../../../../../service/queryParamsExtraction";

const DownloadChartsButton = props => {
  const varNumber = extractVarNumber(props.routerSearch);
  const inputFileName = extractInputFileName(props.routerSearch);

  const path = getAutumn2020DownloadChartsPath(varNumber, inputFileName);

  return (
    <a href={path} download={"charts.rar"}>
      <button className="overview" type="submit">Загрузить charts.rar</button>
    </a>
  );
}

export default DownloadChartsButton;