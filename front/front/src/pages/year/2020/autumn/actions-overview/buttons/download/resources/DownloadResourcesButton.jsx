import {autumn2020DownloadResourcesPath} from "../../../../../../../../config/paths/autumn2020";

const DownloadResourcesButton = () => {
  return (
    <a href={autumn2020DownloadResourcesPath} download="resources.rar">
      <button className="overview" type="submit">Загрузить resources.rar</button>
    </a>
  );
}

export default DownloadResourcesButton;