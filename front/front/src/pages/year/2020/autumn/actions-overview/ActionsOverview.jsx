import "../../../../../index.css";
import DownloadListingButton from "./buttons/download/listing/DownloadListingButton";
import DownloadResourcesButton from "./buttons/download/resources/DownloadResourcesButton";
import GetExerciseInfoButton from "./GetExerciseInfoButton";
import DownloadChartsButton from "./buttons/download/charts/DownloadChartsButton";
import {extractFile, extractLang, extractOop, extractVarNumber} from "../../../../../service/queryParamsExtraction";

const ActionsOverview = props => {
  const routerSearch = props.location.search;
  const varNumber = extractVarNumber(routerSearch);
  const lang = extractLang(routerSearch);
  const file = extractFile(routerSearch);
  const oop = extractOop(routerSearch);

  return (
    <div>
      <div className="ttl">
        <p>Осень, 2020 год</p>
        <span>{varNumber} вариант, </span>
        <span>язык программирования: {lang.charAt(0).toUpperCase() + lang.slice(1)}, </span>
        <span>{file === "file" ? "с чтением файла, " : "без чтения файла, "}</span>
        <span>{oop === "withoop" ? "с ООП" : "без ООП"}</span>
      </div>
      <DownloadListingButton varNumber={varNumber} lang={lang} file={file} oop={oop} routerSearch={routerSearch} />
      <DownloadChartsButton varNumber={varNumber} routerSearch={routerSearch} />
      <DownloadResourcesButton />
      <GetExerciseInfoButton varNumber={varNumber} routerSearch={routerSearch} />
    </div>
  );
}

export default ActionsOverview;