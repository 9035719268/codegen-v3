import {render, unmountComponentAtNode} from "react-dom";
import ActionsOverview from "./ActionsOverview";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderValuesForVar1CppManualNoOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1CppManualWithOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1CppFileNoOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1CppFileWithOop", () => {
  const location = {
    search: "?var=1&lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1PythonManualNoOop", () => {
  const location = {
    search: "?var=1&lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1PythonManualWithOop", () => {
  const location = {
    search: "?var=1&lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1PythonFileNoOop", () => {
  const location = {
    search: "?var=1&lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1PythonFileWithOop", () => {
  const location = {
    search: "?var=1&lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1JavaManualWithOop", () => {
  const location = {
    search: "?var=1&lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar1JavaFileWithOop", () => {
  const location = {
    search: "?var=1&lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("1 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2CppManualNoOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2CppManualWithOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2CppFileNoOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2CppFileWithOop", () => {
  const location = {
    search: "?var=2&lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2PythonManualNoOop", () => {
  const location = {
    search: "?var=2&lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2PythonManualWithOop", () => {
  const location = {
    search: "?var=2&lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2PythonFileNoOop", () => {
  const location = {
    search: "?var=2&lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2PythonFileWithOop", () => {
  const location = {
    search: "?var=2&lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2JavaManualWithOop", () => {
  const location = {
    search: "?var=2&lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar2JavaFileWithOop", () => {
  const location = {
    search: "?var=2&lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("2 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3CppManualNoOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3CppManualWithOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3CppFileNoOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3CppFileWithOop", () => {
  const location = {
    search: "?var=3&lang=cpp&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Cpp, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.cpp");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3PythonManualNoOop", () => {
  const location = {
    search: "?var=3&lang=python&file=false&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3PythonManualWithOop", () => {
  const location = {
    search: "?var=3&lang=python&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3PythonFileNoOop", () => {
  const location = {
    search: "?var=3&lang=python&file=true&oop=false"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("без ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3PythonFileWithOop", () => {
  const location = {
    search: "?var=3&lang=python&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Python, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить main.py");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3JavaManualWithOop", () => {
  const location = {
    search: "?var=3&lang=java&file=false&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("без чтения файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});

it("shouldRenderValuesForVar3JavaFileWithOop", () => {
  const location = {
    search: "?var=3&lang=java&file=true&oop=true"
  };

  render(<ActionsOverview location={location} />, container);

  expect(container.getElementsByTagName("P")[0].textContent).toBe("Осень, 2020 год");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("3 вариант, ");
  expect(container.getElementsByTagName("SPAN")[1].textContent).toBe("язык программирования: Java, ");
  expect(container.getElementsByTagName("SPAN")[2].textContent).toBe("с чтением файла, ");
  expect(container.getElementsByTagName("SPAN")[3].textContent).toBe("с ООП");
  expect(container.getElementsByTagName("A")[0].textContent).toBe("Загрузить Main.java");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("Загрузить GraphDrawer.java");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Загрузить charts.rar");
  expect(container.getElementsByTagName("A")[3].textContent).toBe("Загрузить resources.rar");
  expect(container.getElementsByTagName("A")[4].textContent).toBe("Получить информацию по заданию");
});