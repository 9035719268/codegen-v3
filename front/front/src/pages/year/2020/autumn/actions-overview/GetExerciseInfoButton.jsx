import {
  getAutumn2020Var1ExerciseInfoPagePath,
  getAutumn2020Var2ExerciseInfoPagePath,
  getAutumn2020Var3ExerciseInfoPagePath
} from "../../../../../config/paths/frontend";
import {extractInputFileName, extractLat, extractLon, extractSatelliteNumber} from "../../../../../service/queryParamsExtraction";

const GetExerciseInfoButton = props => {
  const varNumber = props.varNumber;

  let path = "";
  if (varNumber === "1") {
    const inputFileName = extractInputFileName(props.routerSearch);
    path = getAutumn2020Var1ExerciseInfoPagePath(inputFileName);
  } else if (varNumber === "2") {
    const inputFileName = extractInputFileName(props.routerSearch);
    path = getAutumn2020Var2ExerciseInfoPagePath(inputFileName);
  } else if (varNumber === "3") {
    const lat = extractLat(props.routerSearch);
    const lon = extractLon(props.routerSearch);
    const satelliteNumber = extractSatelliteNumber(props.routerSearch);
    path = getAutumn2020Var3ExerciseInfoPagePath(lat, lon, satelliteNumber);
  }

  return (
    <a href={path}>
      <button className="overview" type="submit">Получить информацию по заданию</button>
    </a>
  );
}

export default GetExerciseInfoButton;