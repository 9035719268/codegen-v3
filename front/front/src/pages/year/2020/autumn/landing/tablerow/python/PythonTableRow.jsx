import {AUTUMN_2020_VAR1_USERS, AUTUMN_2020_VAR2_USERS, AUTUMN_2020_VAR3_USERS} from "../../../../../../../service/security";
import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";

const PythonTableRow = () => {
  const path = "/year/2020/autumn/exercise-info-form";
  const fileWithOop = "На 5 с ООП";
  const fileNoOop = "На 5 без ООП";
  const manualWithOop = "На 3/4 с ООП";
  const manualNoOop = "На 3/4 без ООП";

  return (
    <tr>
      <th scope="row">Python</th>
      <td>
        <TableQueryAuthWrapper path={path} varNumber="1" lang="python" file="false" oop="false" text={manualNoOop} rolesAllowed={AUTUMN_2020_VAR1_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="1" lang="python" file="false" oop="true" text={manualWithOop} rolesAllowed={AUTUMN_2020_VAR1_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="1" lang="python" file="true" oop="false" text={fileNoOop} rolesAllowed={AUTUMN_2020_VAR1_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="1" lang="python" file="true" oop="true" text={fileWithOop} rolesAllowed={AUTUMN_2020_VAR1_USERS} />
      </td>
      <td>
        <TableQueryAuthWrapper path={path} varNumber="2" lang="python" file="false" oop="false" text={manualNoOop} rolesAllowed={AUTUMN_2020_VAR2_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="2" lang="python" file="false" oop="true" text={manualWithOop} rolesAllowed={AUTUMN_2020_VAR2_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="2" lang="python" file="true" oop="false" text={fileNoOop} rolesAllowed={AUTUMN_2020_VAR2_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="2" lang="python" file="true" oop="true" text={fileWithOop} rolesAllowed={AUTUMN_2020_VAR2_USERS} />
      </td>
      <td>
        <TableQueryAuthWrapper path={path} varNumber="3" lang="python" file="false" oop="false" text={manualNoOop} rolesAllowed={AUTUMN_2020_VAR3_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="3" lang="python" file="false" oop="true" text={manualWithOop} rolesAllowed={AUTUMN_2020_VAR3_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="3" lang="python" file="true" oop="false" text={fileNoOop} rolesAllowed={AUTUMN_2020_VAR3_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="3" lang="python" file="true" oop="true" text={fileWithOop} rolesAllowed={AUTUMN_2020_VAR3_USERS} />
      </td>
    </tr>
  );
}

export default PythonTableRow;