import {render, unmountComponentAtNode} from "react-dom";
import PythonTableRow from "./PythonTableRow";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderPythonTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<PythonTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});