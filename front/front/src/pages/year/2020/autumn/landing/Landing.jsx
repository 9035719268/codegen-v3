import "bootstrap/dist/css/bootstrap.min.css";
import TableHeader from "./tableheader/TableHeader";
import TableRow from "./tablerow/TableRow";

const Landing = () => {
  return (
    <div>
      <table className="table table-hover table-dark">
        <TableHeader />
        <tbody>
          <TableRow lang="cpp" />
          <TableRow lang="python" />
          <TableRow lang="java" />
        </tbody>
      </table>
    </div>
  );
}

export default Landing;