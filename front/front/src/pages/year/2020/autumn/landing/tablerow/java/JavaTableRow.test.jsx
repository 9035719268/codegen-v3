import {render, unmountComponentAtNode} from "react-dom";
import JavaTableRow from "./JavaTableRow";
import {adminAccessToken, a20201AccessToken, a20202AccessToken, a20203AccessToken, s2021AccessToken} from "../../../../../../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderJavaTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<JavaTableRow />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});