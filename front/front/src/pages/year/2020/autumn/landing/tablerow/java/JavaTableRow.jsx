import {AUTUMN_2020_VAR1_USERS, AUTUMN_2020_VAR2_USERS, AUTUMN_2020_VAR3_USERS} from "../../../../../../../service/security";
import TableQueryAuthWrapper from "../../tablequery/TableQueryAuthWrapper";

const JavaTableRow = () => {
  const path = "/year/2020/autumn/exercise-info-form";
  const fileWithOop = "На 5 с ООП";
  const manualWithOop = "На 3/4 с ООП";

  return (
    <tr>
      <th scope="row">Java</th>
      <td>
        <TableQueryAuthWrapper path={path} varNumber="1" lang="java" file="false" oop="true" text={manualWithOop} rolesAllowed={AUTUMN_2020_VAR1_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="1" lang="java" file="true" oop="true" text={fileWithOop} rolesAllowed={AUTUMN_2020_VAR1_USERS} />
      </td>
      <td>
        <TableQueryAuthWrapper path={path} varNumber="2" lang="java" file="false" oop="true" text={manualWithOop} rolesAllowed={AUTUMN_2020_VAR2_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="2" lang="java" file="true" oop="true" text={fileWithOop} rolesAllowed={AUTUMN_2020_VAR2_USERS} />
      </td>
      <td>
        <TableQueryAuthWrapper path={path} varNumber="3" lang="java" file="false" oop="true" text={manualWithOop} rolesAllowed={AUTUMN_2020_VAR3_USERS} />
        <TableQueryAuthWrapper path={path} varNumber="3" lang="java" file="true" oop="true" text={fileWithOop} rolesAllowed={AUTUMN_2020_VAR3_USERS} />
      </td>
    </tr>
  );
}

export default JavaTableRow;