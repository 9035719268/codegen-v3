import {render, unmountComponentAtNode} from "react-dom";
import TableHeader from "./TableHeader";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderFirstHeader", () => {
  render(<TableHeader />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("Вариант 1");
});

it("shouldRenderSecondHeader", () => {
  render(<TableHeader />, container);

  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Вариант 2");
});

it("shouldRenderThirdHeader", () => {
  render(<TableHeader />, container);

  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Вариант 3");
});