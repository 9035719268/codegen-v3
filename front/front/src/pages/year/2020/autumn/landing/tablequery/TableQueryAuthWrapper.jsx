import {checkUserAccess} from "../../../../../../service/security";
import TableQuery from "./TableQuery";

const TableQueryAuthWrapper = props => {
  const path = props.path;
  const varNumber = props.varNumber;
  const lang = props.lang;
  const file = props.file;
  const oop = props.oop;
  const text = props.text;
  const rolesAllowed = props.rolesAllowed;

  const hasAccess = checkUserAccess(rolesAllowed);

  if (hasAccess) {
    return <TableQuery path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} />
  } else {
    return <></>
  }
}

export default TableQueryAuthWrapper;