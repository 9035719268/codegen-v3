import {render, unmountComponentAtNode} from "react-dom";
import TableRow from "./TableRow";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderCppTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderCppTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderCppTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderCppTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderCppTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="cpp" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderJavaTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 5 с ООП");
});

it("shouldRenderJavaTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="java" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderPythonTableRowAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
});

it("shouldRenderPythonTableRowS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<TableRow lang="python" />, container);

  expect(container.getElementsByTagName("TH")[0].textContent).toBe("Python");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});
