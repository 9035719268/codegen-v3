import {render, unmountComponentAtNode} from "react-dom";
import TableQueryAuthWrapper from "./TableQueryAuthWrapper";
import {adminAccessToken, a20201AccessToken, a20202AccessToken, a20203AccessToken, s2021AccessToken} from "../../../../../../config/tokens/tokens";

let adminContainer = null;
let a20201Container = null;
let a20202Container = null;
let a20203Container = null;
let s2021Container = null;

beforeEach(() => {
  adminContainer = document.createElement("div");
  a20201Container = document.createElement("div");
  a20202Container = document.createElement("div");
  a20203Container = document.createElement("div");
  s2021Container = document.createElement("div");

  document.body.appendChild(adminContainer);
  document.body.appendChild(a20201Container);
  document.body.appendChild(a20202Container);
  document.body.appendChild(a20203Container);
  document.body.appendChild(s2021Container);
});

afterEach(() => {
  unmountComponentAtNode(adminContainer);
  unmountComponentAtNode(a20201Container);
  unmountComponentAtNode(a20202Container);
  unmountComponentAtNode(a20203Container);
  unmountComponentAtNode(s2021Container);

  adminContainer.remove();
  a20201Container.remove();
  a20202Container.remove();
  a20203Container.remove();
  s2021Container.remove();

  adminContainer = null;
  a20201Container = null;
  a20202Container = null;
  a20203Container = null;
  s2021Container = null;
});

const path = "http://localhost:8888/path";
const varNumber = "1";
const lang = "cpp";
const file = "file";
const oop = "withoop";
const text = "На 3/4 без ООП";

it("shouldRenderTableQueryAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_ADMIN"]} />, adminContainer
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20201"]} />, a20201Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20202"]} />, a20202Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20203"]} />, a20203Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_S2021"]} />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderTableQueryA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_ADMIN"]} />, adminContainer
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20201"]} />, a20201Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20202"]} />, a20202Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20203"]} />, a20203Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_S2021"]} />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderTableQueryA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_ADMIN"]} />, adminContainer
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20201"]} />, a20201Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20202"]} />, a20202Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20203"]} />, a20203Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_S2021"]} />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderTableQueryA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_ADMIN"]} />, adminContainer
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20201"]} />, a20201Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20202"]} />, a20202Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20203"]} />, a20203Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_S2021"]} />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(s2021Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});

it("shouldRenderTableQueryS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_ADMIN"]} />, adminContainer
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20201"]} />, a20201Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20202"]} />, a20202Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_A20203"]} />, a20203Container
  );
  render(
    <TableQueryAuthWrapper path={path} varNumber={varNumber} lang={lang} file={file} oop={oop} text={text} rolesAllowed={["ROLE_S2021"]} />, s2021Container
  );

  expect(adminContainer.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20201Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20202Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(a20203Container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
  expect(s2021Container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
});