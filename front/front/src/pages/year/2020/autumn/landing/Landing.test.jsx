import {render, unmountComponentAtNode} from "react-dom";
import Landing from "./Landing";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../../../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderLandingAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("Вариант 1");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Вариант 2");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Вариант 3");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[6].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[10].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[11].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[12].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[13].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[14].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[15].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[16].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[17].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[18].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[19].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[20].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[21].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[22].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[23].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[24].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[25].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[26].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[27].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[28].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[29].textContent).toBe("На 5 с ООП");
});

it("shouldRenderLandingA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("Вариант 1");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Вариант 2");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Вариант 3");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[6].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe("На 5 с ООП");
});

it("shouldRenderLandingA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("Вариант 1");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Вариант 2");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Вариант 3");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[6].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe("На 5 с ООП");
});

it("shouldRenderLandingA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("Вариант 1");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Вариант 2");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Вариант 3");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[6].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[1].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[2].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[3].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[4].textContent).toBe("На 3/4 без ООП");
  expect(container.getElementsByTagName("BUTTON")[5].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[6].textContent).toBe("На 5 без ООП");
  expect(container.getElementsByTagName("BUTTON")[7].textContent).toBe("На 5 с ООП");
  expect(container.getElementsByTagName("BUTTON")[8].textContent).toBe("На 3/4 с ООП");
  expect(container.getElementsByTagName("BUTTON")[9].textContent).toBe("На 5 с ООП");
});

it("shouldRenderLandingS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<Landing />, container);

  expect(container.getElementsByTagName("TH")[1].textContent).toBe("Вариант 1");
  expect(container.getElementsByTagName("TH")[2].textContent).toBe("Вариант 2");
  expect(container.getElementsByTagName("TH")[3].textContent).toBe("Вариант 3");
  expect(container.getElementsByTagName("TH")[4].textContent).toBe("Cpp");
  expect(container.getElementsByTagName("TH")[5].textContent).toBe("Python");
  expect(container.getElementsByTagName("TH")[6].textContent).toBe("Java");
  expect(container.getElementsByTagName("BUTTON")[0]).toBe(undefined);
});