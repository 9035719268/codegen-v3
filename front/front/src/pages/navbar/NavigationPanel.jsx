import {Container, Nav, Navbar} from "react-bootstrap";
import {getCurrentUser} from "../../service/security";

const NavigationPanel = () => {
  return (
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">Codegen</Navbar.Brand>
        <Navbar.Brand>{"Пользователь: " + getCurrentUser()}</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/">На главную</Nav.Link>
          <Nav.Link href="/logout">Выйти</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
}

export default NavigationPanel;