import {render, unmountComponentAtNode} from "react-dom";
import NavigationPanel from "./NavigationPanel";
import {a20201AccessToken, a20202AccessToken, a20203AccessToken, adminAccessToken, s2021AccessToken} from "../../config/tokens/tokens";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("shouldRenderNavigationPanelAdmin", () => {
  localStorage.setItem("access_token", adminAccessToken);

  render(<NavigationPanel />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Codegen");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Пользователь: admin");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("На главную");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Выйти");
});

it("shouldRenderNavigationPanelA20201", () => {
  localStorage.setItem("access_token", a20201AccessToken);

  render(<NavigationPanel />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Codegen");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Пользователь: a20201");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("На главную");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Выйти");
});

it("shouldRenderNavigationPanelA20202", () => {
  localStorage.setItem("access_token", a20202AccessToken);

  render(<NavigationPanel />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Codegen");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Пользователь: a20202");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("На главную");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Выйти");
});

it("shouldRenderNavigationPanelA20203", () => {
  localStorage.setItem("access_token", a20203AccessToken);

  render(<NavigationPanel />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Codegen");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Пользователь: a20203");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("На главную");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Выйти");
});

it("shouldRenderNavigationPanelS2021", () => {
  localStorage.setItem("access_token", s2021AccessToken);

  render(<NavigationPanel />, container);

  expect(container.getElementsByTagName("A")[0].textContent).toBe("Codegen");
  expect(container.getElementsByTagName("SPAN")[0].textContent).toBe("Пользователь: s2021");
  expect(container.getElementsByTagName("A")[1].textContent).toBe("На главную");
  expect(container.getElementsByTagName("A")[2].textContent).toBe("Выйти");
});