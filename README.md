# Services

Service | Link
------------ | -------------
SERVICE-REGISTRY | **http://localhost:8761**
AUTUMN-2020-SERVICE | **http://localhost:8081**
SPRING-2021-SERVICE | **http://localhost:8082**
GATEWAY-SERVICE | **http://localhost:8888**
SECURITY-SERVICE | **http://localhost:9999**
FRONT-END | **http://localhost:3000**
